import 'package:keYou/model/verification.dart';

abstract class IVerificationPhotoDao {
  Future<void> setVerificationImage(VerificationPhoto verificationImagePath);

  Future<VerificationPhoto> getVerificationImage();

  Future<void> deleteVerificationPhotos();

  Future<void> setVerificationApproved(VerificationApproved approved);

  Future<VerificationApproved> getVerificationApproved();

  Future<void> deleteVerificationApprove();
}
