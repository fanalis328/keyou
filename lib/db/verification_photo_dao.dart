import 'package:keYou/data/app.dart';
import 'verification_photo_dao_interface.dart';
import 'package:keYou/model/verification.dart';
import 'package:sembast/sembast.dart';

class VerificationPhotoDao implements IVerificationPhotoDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setVerificationImage(VerificationPhoto verificationImagePath) async {
    await store
        .record('verificationPhoto')
        .put(await db, verificationImagePath.toJson());
  }

  Future<VerificationPhoto> getVerificationImage() async {
    var verificationPhoto =
        await store.record('verificationPhoto').get(await db);
    return VerificationPhoto.fromJson(verificationPhoto);
  }

  deleteVerificationPhotos() async {
    await store.record('verificationPhoto').delete(await db);
  }

  // _________________________VerificationApproved___________________________________

  setVerificationApproved(VerificationApproved approved) async {
    store.record('verificationApproved').put(await db, approved.toJson());
  }

  Future<VerificationApproved> getVerificationApproved() async {
    var approve = await store.record('verificationApproved').get(await db);
    return VerificationApproved.fromJson(approve);
  }

  deleteVerificationApprove() async {
    await store.record('verificationApproved').delete(await db);
  }
}
