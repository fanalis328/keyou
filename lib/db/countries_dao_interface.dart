abstract class ICountriesDao {
  Future<void> setCountries(List countries);

  Future<dynamic> getCountries();

  Future<void> deleteCountries();
}
