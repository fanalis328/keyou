abstract class ISettingsDao {
  Future<void> setSettings(Map settings);

  Future<Map<String, dynamic>> getSettings();

  Future<void> deleteSettings();
}
