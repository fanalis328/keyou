import 'package:keYou/data/app.dart';
import 'user_dao_interface.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:sembast/sembast.dart';

class UserDao implements IUserDao {
  var _userStore = intMapStoreFactory.store('user');
  Future<Database> get _db async => await App.me.db.database;

  Future insertUser(ProfileData user) async {
    await _userStore.add(await _db, user.toJson());
  }

  Future updateUser(ProfileData user) async {
    var finder = Finder(filter: Filter.equals('id', user.id));
    _userStore.findKey(await _db);

    await _userStore.update(
      await _db,
      user.toJson(),
      finder: finder,
    );
  }

  Future<ProfileData> getCurrentUser(ProfileData user) async {
    var finder = Finder(filter: Filter.equals('id', user.id));
    ProfileData currentUser;

    final recordSnapshot = await _userStore.find(
      await _db,
      finder: finder,
    );

    if (recordSnapshot == null) return null;

    recordSnapshot.map((snapshot) {
      currentUser = ProfileData.fromJson(snapshot.value);
    }).toList();
    return currentUser;
  }

  Future deleteUser(ProfileData user) async {
    final finder = Finder(filter: Filter.byKey(user.id));
    await _userStore.delete(await _db, finder: finder);
  }

  Future deleteAll() async {
    await _userStore.delete(
      await _db,
    );
  }

  Future<List<ProfileData>> getAllSortedById() async {
    final finder = Finder(sortOrders: [
      SortOrder('id'),
    ]);

    final recordSnapshots = await _userStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = ProfileData.fromJson(snapshot.value);
      return user;
    }).toList();
  }
}
