abstract class ICountriesListDao {
  Future<void> setCountriesList(List countries);

  Future<dynamic> getCountriesList();

  Future<void> deleteCountriesList();
}
