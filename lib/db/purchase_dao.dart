import 'package:keYou/data/app.dart';
import 'package:keYou/db/purchase_dao_interface.dart';
import 'package:keYou/model/purchase.dart';
import 'package:sembast/sembast.dart';

class PurchaseDao implements IPurchaseDao {
  var store = StoreRef.main();
  var purchaseStore = intMapStoreFactory.store('purchaseStore');
  var denimBlackStore = intMapStoreFactory.store('denimBlackStore');
  Future<Database> get db async => await App.me.db.database;

  Future<void> setTrialPurchaseStatus(TrialPurchaseStatus purchaseStatus) async {
    print(['____SET trialPurchaseStatus ___:', purchaseStatus.toJson()]);
    await purchaseStore.add(await db, purchaseStatus.toJson());
  }

  Future<TrialPurchaseStatus> getTrialPurchaseStatus() async {
    TrialPurchaseStatus trialPurchaseStatus;
    var finder = Finder(filter: Filter.equals('owner', App.me.user.id));

    final recordSnapshot = await purchaseStore.find(
      await db,
      finder: finder,
    );

    if (recordSnapshot == null) return null;

    recordSnapshot.map((snapshot) {
      trialPurchaseStatus = TrialPurchaseStatus.fromJson(snapshot.value);
    }).toList();

    print(['____GET trialPurchaseStatus ___:', trialPurchaseStatus]);
    return trialPurchaseStatus;
  }

  Future<void> setPurchaseData(UpdatePurchaseData purchaseData) async {
    print(['____SET purchase data___:', purchaseData.toJson()]);
    await denimBlackStore.add(await db, purchaseData.toJson());
  }

  Future<UpdatePurchaseData> getPurchaseData() async {
    UpdatePurchaseData purchaseData;
  
    var finder = Finder(filter: Filter.equals('owner', App.me.user.id));
    final recordSnapshot = await denimBlackStore.find(
      await db,
      finder: finder,
    );

    if (recordSnapshot == null) return null;
    recordSnapshot.map((snapshot) {
      purchaseData = UpdatePurchaseData.fromJson(snapshot.value);
    }).toList();

    print(['____GET PURCHASE DATA ___:', purchaseData?.toJson()]);
    return purchaseData;
  }

  Future<void> updatePurchase(UpdatePurchaseData purchaseData) async {
    var finder = Finder(filter: Filter.equals('owner', purchaseData.owner));
    denimBlackStore.findKey(await db);
    await denimBlackStore.update(
      await db,
      purchaseData.toJson(),
      finder: finder,
    );
    print(['____UPDATE PURCHASE DATA ___:', purchaseData?.toJson()]);
  }
}
