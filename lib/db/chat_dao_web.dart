import 'package:keYou/data/app.dart';
import 'package:keYou/model/websocket/chat.dart';
import 'chat_dao_interface.dart';
import 'crud_db_web_localstorage.dart';

class ChatDaoWeb implements IChatDao {
  final _db = CrudLocalStorage<ChatDialogData>(
      getKey: (chat) => chat.topic,
      storeName: 'chats',
      parseDelegate: ChatDialogData.fromJson);

  Future insertChatDialog(ChatDialogData chat) async {
    final chats = await _db.getAll();
    if (chats
        .any((itm) => itm.topic == chat.topic && itm.owner == App.me.user.id))
      return;
    _db.add(chat);
  }

  Future updateChatDialog(ChatDialogData chat) async {
    _db.update(chat, chat.topic);
  }

  Future<ChatDialogData> getChatDialog(String topic) async {
    return _db.getItem(topic);
  }

  Future deleteChatDialog(String topic) async {
    _db.delete(topic);
  }

  Future deleteAllChats() {
    return _db.deleteAll();
  }

  Future<List<ChatDialogData>> getAllChatsSortedById() async {
    // TODO: add sorting
    return _db.getAll();
  }
}
