import 'package:keYou/data/app.dart';
import 'package:sembast/sembast.dart';
import 'lang_dao_interface.dart';

class LangDao implements ILangDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setLang([String lang = 'ru']) async {
    await store.record('lang').put(await db, lang);
  }

  getLang() async {
    var lang = await store.record('lang').get(await db);
    return lang.toString();
  }
}
