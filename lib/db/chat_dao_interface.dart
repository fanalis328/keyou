import 'package:keYou/model/websocket/chat.dart';

abstract class IChatDao {
  Future<void> insertChatDialog(ChatDialogData chat);

  Future<void> updateChatDialog(ChatDialogData chat);

  Future<ChatDialogData> getChatDialog(String topic);

  Future<void> deleteChatDialog(String topic);

  Future<void> deleteAllChats();

  Future<List<ChatDialogData>> getAllChatsSortedById();
}
