import 'package:keYou/data/app.dart';
import 'package:sembast/sembast.dart';
import 'cities_dao_interface.dart';

class CitiesDao implements ICitiesDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setCities(List cities) async {
    await store.record('cities').put(await db, cities);
  }

  getCities() async {
    var cities = await store.record('cities').get(await db);
    return cities;
  }

  deleteCities() async {
    await store.record('cities').delete(await db);
  }
}
