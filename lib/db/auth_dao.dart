import 'package:keYou/data/app.dart';
import 'package:sembast/sembast.dart';
import 'auth_dao_interface.dart';

class AuthDao implements IAuthDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setToken([String token]) async {
    await store.record('token').put(await db, token);
  }

  getToken() async {
    var token = await store.record('token').get(await db);
    return token.toString();
  }

  deleteToken() async {
    await store.record('token').delete(await db);
  }

  //______________________________________________________________

  setCurrentUserId([String id]) async {
    await store.record('currentUserId').put(await db, id);
  }

  getCurrentUserId() async {
    var id = await store.record('currentUserId').get(await db);
    return id.toString();
  }

  deleteCurrentUserId() async {
    await store.record('currentUserId').delete(await db);
  }
}
