abstract class IYandexAnalytics {
  //  Event names
  final String eventRegistration = "registration";
  final String eventLogin = "log_in";
  final String eventPurchase = "purchase";
  final String eventRegStep4M = "reg_step_4_M";
  final String eventRegStep4W = "reg_step_4_W";
  final String eventPurchaseSuccess = "purchase_success";
  final String eventRegStep9PhotoconfW = "reg_step_9_photoconf_W";
  final String eventRegStep10proSuccessM = "reg_step_10_pro_success_M";
  final String eventRegStep10proSuccessW = "reg_step_10_pro_success_W";
  final String eventRegSuccessM = "reg_success_M";
  final String eventRegSuccessW = "reg_success_W";

  void sendYandexAnalyticsEventWithParams(String name, [Map parameters]);

  void sendYandexAnalyticsEvent(String name);

  Future<void> initialize();

  Future<String> getLibraryVersion();
}
