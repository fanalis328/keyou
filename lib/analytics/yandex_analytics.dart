import 'package:appmetrica_sdk/appmetrica_sdk.dart';
import 'package:keYou/analytics/yandex_analytics_interface.dart';

class YandexAnalytics implements IYandexAnalytics {
  //  Event names
  final String eventRegistration = "registration";
  final String eventLogin = "log_in";
  final String eventPurchase = "purchase";
  final String eventRegStep4M = "reg_step_4_M";
  final String eventRegStep4W = "reg_step_4_W";
  final String eventPurchaseSuccess = "purchase_success";
  final String eventRegStep9PhotoconfW = "reg_step_9_photoconf_W";
  final String eventRegStep10proSuccessM = "reg_step_10_pro_success_M";
  final String eventRegStep10proSuccessW = "reg_step_10_pro_success_W";
  final String eventRegSuccessM = "reg_success_M";
  final String eventRegSuccessW = "reg_success_W";

  void sendYandexAnalyticsEventWithParams(String name, [Map parameters]) async {
    AppmetricaSdk().reportEvent(name: name, attributes: parameters);
    print('Yandex event with params succeeded');
  }

  void sendYandexAnalyticsEvent(String name) async {
    AppmetricaSdk().reportEvent(name: name);
    print('Yandex event succeeded');
  }

  @override
  // Future<void> initialize() {
  //   return AppmetricaSdk()
  //       .activate(apiKey: 'c53a0871-a1aa-4a30-94ba-8fee2e3f5825');
  // }

  Future<void> initialize() {
    // KeYou
    return AppmetricaSdk()
        .activate(apiKey: '9eaf9675-70d0-4629-8dce-f95b3fdc2f19');
  }

  @override
  Future<String> getLibraryVersion() {
    return AppmetricaSdk().getLibraryVersion();
  }
}
