import 'package:keYou/analytics/yandex_analytics_interface.dart';

class YandexAnalyticsWeb implements IYandexAnalytics {
  @override
  Future<void> initialize() {
    // TODO: implement initialize
    //throw UnimplementedError();
  }

  @override
  void sendYandexAnalyticsEvent(String name) {
    // TODO: implement sendYandexAnalyticsEvent
  }

  @override
  void sendYandexAnalyticsEventWithParams(String name, [Map parameters]) {
    // TODO: implement sendYandexAnalyticsEventWithParams
  }

  @override
  Future<String> getLibraryVersion() {
    // TODO: implement getLibraryVersion
    //throw UnimplementedError();
  }

  @override
  // TODO: implement eventLogin
  String get eventLogin => null;

  @override
  // TODO: implement eventPurchase
  String get eventPurchase => null;

  @override
  // TODO: implement eventPurchaseSuccess
  String get eventPurchaseSuccess => null;

  @override
  // TODO: implement eventRegStep10proSuccessM
  String get eventRegStep10proSuccessM => null;

  @override
  // TODO: implement eventRegStep10proSuccessW
  String get eventRegStep10proSuccessW => null;

  @override
  // TODO: implement eventRegStep4M
  String get eventRegStep4M => null;

  @override
  // TODO: implement eventRegStep4W
  String get eventRegStep4W => null;

  @override
  // TODO: implement eventRegStep9PhotoconfW
  String get eventRegStep9PhotoconfW => null;

  @override
  // TODO: implement eventRegSuccessM
  String get eventRegSuccessM => null;

  @override
  // TODO: implement eventRegSuccessW
  String get eventRegSuccessW => null;

  @override
  // TODO: implement eventRegistration
  String get eventRegistration => null;
}
