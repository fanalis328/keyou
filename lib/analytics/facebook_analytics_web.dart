import 'package:facebook_app_events/facebook_app_events.dart';

import 'facebook_analytics_interface.dart';

class FacebookAnalyticsWeb implements IFacebookAnalytics {
  @override
  void activatedAppEvent() {
    // TODO: implement activatedAppEvent
  }

  @override
  void sendFacebookAnalyticsEvent(String name) {
    // TODO: implement sendFacebookAnalyticsEvent
  }

  @override
  // TODO: implement evenInitiatedCheckout
  String get evenInitiatedCheckout => null;

  @override
  // TODO: implement eventCompletedRegistrationM
  String get eventCompletedRegistrationM => null;

  @override
  // TODO: implement eventNameSearched
  String get eventNameSearched => null;

  @override
  // TODO: implement eventSubmitApplicationW
  String get eventSubmitApplicationW => null;

  @override
  // TODO: implement eventViewedContent
  String get eventViewedContent => null;

  @override
  // TODO: implement facebookAppEvents
  FacebookAppEvents get facebookAppEvents => null;

  @override
  // TODO: implement standardEventCompletedRegistration
  String get standardEventCompletedRegistration => null;

  @override
  // TODO: implement standardEventViewedContent
  String get standardEventViewedContent => null;

  @override
  void sendFacebookAnalyticsEventWithParams(
      String name, double value, String currency) {
    // TODO: implement sendFacebookAnalyticsEventWithParams
  }
}
