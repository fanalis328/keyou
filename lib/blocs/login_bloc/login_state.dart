import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginState extends Equatable {
  LoginState([List props = const <dynamic>[]]) : super(props);
}

class InitialLoginState extends LoginState {}

class LoadingLoginState extends LoginState {}

class GetSmsCodeLoginState extends LoginState {
  final String smsCode;
  GetSmsCodeLoginState({this.smsCode});
}
