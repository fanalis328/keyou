import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegistrationEvent extends Equatable {
  RegistrationEvent([List props = const <dynamic>[]]) : super(props);
}

class GetLoadingState extends RegistrationEvent {}

class GetInitialStep extends RegistrationEvent {}

class GetSmsCode extends RegistrationEvent {
  // final String phone;

  // GetSmsCode({this.phone}) : super([phone]);
}

class GetSmsCodeStep extends RegistrationEvent {
  final String code;
  final String phone;
  final String status;
  GetSmsCodeStep({this.code, this.phone, this.status});
}

// class GetPasswordStep extends RegistrationEvent {}

// class GetRepeatPasswordStep extends RegistrationEvent {
//   final String password;

//   GetRepeatPasswordStep({this.password});
// }

class GetGenderStep extends RegistrationEvent {}

class GetSearchGenderStep extends RegistrationEvent {}

class GetNameStep extends RegistrationEvent {}

class GetBirthDayStep extends RegistrationEvent {}

class GetCountryStep extends RegistrationEvent {}

class GetCityStep extends RegistrationEvent {
  final UserLocation country;
  GetCityStep({this.country});
}

class GetCitizenshipStep extends RegistrationEvent {}

class GetPhotoStep extends RegistrationEvent {}

class GetMainPhotoEditStep extends RegistrationEvent {
  final ImageFile imageFile;
  GetMainPhotoEditStep({this.imageFile});
}

class GetPhotoVerificationStep extends RegistrationEvent {}

class GetPhotoVerificationEndStep extends RegistrationEvent {
  final ImageFile imageFile;
  GetPhotoVerificationEndStep({this.imageFile}) : super([imageFile]);
}

class GetCheckpointStep extends RegistrationEvent {}

class GetHeightStep extends RegistrationEvent {}

class GetWeightStep extends RegistrationEvent {}

class GetQuestion1Step extends RegistrationEvent {}

class GetUserProfileDescriptionStep extends RegistrationEvent {}

class GetFinalRegistrationStep extends RegistrationEvent {}
