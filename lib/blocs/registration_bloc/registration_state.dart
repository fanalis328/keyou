import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegistrationState extends Equatable {
  RegistrationState([List props = const <dynamic>[]]) : super(props);
}

class InitialRegistrationState extends RegistrationState {}

class LoadingRegistrationStepState extends RegistrationState {}

class SmsCodeRegistrationState extends RegistrationState {
  final String smsCode;
  final String phone;
  final String status;
  SmsCodeRegistrationState({this.smsCode, this.phone, this.status})
      : super([smsCode, phone, status]);
}

class PasswordRegistrationState extends RegistrationState {}

class PasswordRepeatRegistrationState extends RegistrationState {
  final String password;

  PasswordRepeatRegistrationState({this.password});
}

class GenderRegistrationState extends RegistrationState {}

class SearchGenderRegistrationState extends RegistrationState {}

class NameRegistrationState extends RegistrationState {}

class BirthDayRegistrationState extends RegistrationState {}

class CountryRegistrationState extends RegistrationState {}

class CityRegistrationState extends RegistrationState {
  final UserLocation country;
  CityRegistrationState({this.country});
}

class CitizenshipRegistrationState extends RegistrationState {}

class PhotoRegistrationState extends RegistrationState {}

class MainPhotoEditRegistrationState extends RegistrationState {
  final ImageFile imageFile;

  MainPhotoEditRegistrationState({this.imageFile});
}

class PhotoVerificationRegistrationState extends RegistrationState {}

class PhotoVerificationEndRegistrationState extends RegistrationState {
  final ImageFile imageFile;
  PhotoVerificationEndRegistrationState({this.imageFile});
}

class CheckpointRegistrationState extends RegistrationState {}

class HeightRegistrationState extends RegistrationState {}

class WeightRegistrationState extends RegistrationState {}

class QuestionRegistrationState extends RegistrationState {}

class Question1RegistrationState extends RegistrationState {}

class UserProfileDescriptionRegistrationState extends RegistrationState {}

class FinalRegistrationState extends RegistrationState {}
