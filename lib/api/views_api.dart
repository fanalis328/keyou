import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';

class ViewsApi {
  getViews(int offset, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'getViews',
      'data': {'offset': offset}
    }, callback);
  }

  addView(String userId, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'viewUser',
      'data': {'user': userId}
    }, callback);
  }
}
