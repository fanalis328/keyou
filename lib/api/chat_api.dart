import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';

class ChatApi {
  subscribeChat(String topic, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'subscribeChat',
      'data': {'topic': topic}
    }, callback);
  }

  leaveChat(String topic, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'leaveChat',
      'data': {'topic': topic}
    }, callback);
  }

  sendMessage(String topic, String content, List<String> head,
      [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'sendMessage',
      'data': {
        'topic': topic,
        'content': content,
        'head': {'attachments': head}
      }
    }, callback);
  }

  getMessages(String topic,
      [int before = 10, int since = 0, dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'getMessages',
      'data': {'topic': topic, 'before': before, 'since': since}
    }, callback);
  }

  getChatInfo(String topic, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'getChat',
      'data': {'topic': topic}
    }, callback);
  }

  getChats(String topic, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'getChats',
      'data': {'topic': topic}
    }, callback);
  }

  // CHAT EVENTS____________________________________
  typingMessage(String topic, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'typingMessage',
      'data': {'topic': topic}
    }, callback);
  }

  readMessage(String topic, int seq, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'readMessage',
      'data': {'topic': topic, 'seq': seq}
    }, callback);
  }

  receivedMessage(String topic, int seq, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'receivedMessage',
      'data': {'topic': topic, 'seq': seq}
    }, callback);
  }
}
