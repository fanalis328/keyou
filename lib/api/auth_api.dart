import 'dart:convert';
import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/model/websocket/auth.dart';
import 'package:keYou/model/websocket/validate.dart';
import 'package:keYou/utils/profile.dart';

class AuthApi {
  login(String secret, String scheme, [dynamic callback]) {
    String loginSecret;

    if (scheme == 'phone') {
      loginSecret = base64.encode(utf8.encode(secret));
    } else {
      loginSecret = secret;
    }

    App.me.sendLegacy(
      Login(
        id: socket.requestId(),
        data: LoginData(scheme: scheme, secret: loginSecret),
      ).toJson(),
      callback,
    );
  }

  validate(String phone, dynamic callback) {
    App.me.sendLegacy(
      Validate(
        id: socket.requestId(),
        data: ValidateData(phone: phone),
      ).toJson(),
      callback,
    );
  }

  logout([callback]) {
    App.me.sendLegacy(Logout(id: socket.requestId()).toJson(),
        callback == null ? ProfileUtils.logoutCallback : callback);
  }
}
