import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/model/websocket/search_filter.dart';

class SearchApi {
  searchUsers(SearchFilters filters, dynamic callback) async {
    Map filtersJson = filters.toJson();
    filtersJson.removeWhere((key, value) => value == null);
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'searchUsers',
      'data': {'filters': filtersJson}
    }, callback);
  }

  getUserById(String userId, dynamic callback) {
    String currentUserId = App.me.user.id;
    if (userId != currentUserId) {
      viewsApi.addView(userId);
    }
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'getUser',
      'data': {'user': userId}
    }, callback);
  }
}
