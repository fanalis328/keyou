import 'dart:io';
import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'image_editor_interface.dart';

class _ImageEditor {
  static Future<File> cropImage(File imageFile) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: 'Редактирование фото',
        toolbarColor: DenimColors.colorPrimary,
        toolbarWidgetColor: Colors.white,
        activeControlsWidgetColor: DenimColors.colorPrimary,
        activeWidgetColor: DenimColors.colorPrimary,
        statusBarColor: DenimColors.colorPrimary,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: false,
      ),
    );

    return croppedFile;
  }
}

class ImageEditor implements IImageEditor {
  @override
  Future<ImageFile> cropImage(ImageFile image) async {
    final file = File(image.path);
    final croped = await _ImageEditor.cropImage(file);
    return ImageFile(path: croped.path);
  }
}
