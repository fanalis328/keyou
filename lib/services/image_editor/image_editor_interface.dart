import 'package:keYou/services/image_picker/image_file_model.dart';

abstract class IImageEditor {
  Future<ImageFile> cropImage(ImageFile imageFile);
}
