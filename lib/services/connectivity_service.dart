import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';

Future<ConnectivityResult> checkConnectivity() {
  if (kIsWeb) return Future.value(ConnectivityResult.mobile);
  return Connectivity().checkConnectivity();
}
