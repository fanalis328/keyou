import 'dart:async';
import 'dart:html' as html;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'image_file_model.dart';

class _WebImagePicker {
  Future<Map<String, dynamic>> pickImage() async {
    print('pickImage');
    final Map<String, dynamic> data = {};
    final html.FileUploadInputElement input = html.FileUploadInputElement();
    // Fix for mobile safari
    html.document.body.append(input);
    input..accept = 'image/*';
    input.click();
    print("click!");
    final completer = Completer<dynamic>();
    // Fix for mobile safari
    // await input.onChange.first;
    input.addEventListener('change', (e) => completer.complete(e));
    await completer.future;
    print("change");
    if (input.files.isEmpty) return null;
    print("have files");
    final reader = html.FileReader();
    reader.readAsDataUrl(input.files[0]);
    await reader.onLoad.first;
    final encoded = reader.result as String;
    // remove data:image/*;base64 preambule
    final stripped =
        encoded.replaceFirst(RegExp(r'data:image/[^;]+;base64,'), '');
    // final imageBase64 = base64.decode(stripped);
    final imageName = input.files?.first?.name;
    final imagePath = input.files?.first?.relativePath;
    data.addAll({'name': imageName, 'data': stripped, 'path': imagePath});
    return data;
  }
}

class FlutterWebImagePicker {
  static void _registerWith(Registrar registrar) {
    final channel = MethodChannel('flutter_web_image_picker',
        const StandardMethodCodec(), registrar.messenger);
    final instance = _WebImagePicker();
    channel.setMethodCallHandler((call) async {
      switch (call.method) {
        case 'pickImage':
          return await instance.pickImage();
        default:
          throw MissingPluginException();
      }
    });
  }

  static final MethodChannel _methodChannel =
      const MethodChannel('flutter_web_image_picker');

  static Future<ImageFile> get getImage async {
    final data =
        await _methodChannel.invokeMapMethod<String, dynamic>('pickImage');
    final imageName = data['name'];
    final imageData = base64.decode(data['data']);
    return ImageFile(
        name: data['name'], path: data['path'], data: data['data']);
  }
}
