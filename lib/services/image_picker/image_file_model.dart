class ImageFile {
  final String name;
  final String data;
  final String path;
  ImageFile({
    this.name,
    this.data,
    this.path,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      // don't uncomment
      // 'data': data,
      'path': path,
    };
  }

  static ImageFile fromJson(Map<String, dynamic> map) {
    if (map == null) return null;

    return ImageFile(
      name: map['name'],
      // don't uncomment
      // data: map['data'],
      path: map['path'],
    );
  }
}
