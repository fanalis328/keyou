import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:image_picker/image_picker.dart' as mobilePicker;
import 'image_picker_interface.dart';

class ImagePicker implements IImagePicker {
  @override
  Future<ImageFile> pickImage(
      {mobilePicker.ImageSource source =
          mobilePicker.ImageSource.gallery}) async {
    final file = await mobilePicker.ImagePicker.pickImage(source: source);
    return file?.path == null ? null : ImageFile(path: file.path);
  }
}
