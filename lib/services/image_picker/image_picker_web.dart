import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:keYou/services/image_picker/web_image_picker.dart';
import 'package:image_picker/image_picker.dart' as mobilePicker;
import 'image_picker_interface.dart';

class ImagePicker implements IImagePicker {
  @override
  Future<ImageFile> pickImage(
      {mobilePicker.ImageSource source =
          mobilePicker.ImageSource.gallery}) async {
    return FlutterWebImagePicker.getImage;
  }
}
