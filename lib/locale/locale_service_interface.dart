abstract class ILocaleService {
  Future<String> getLocale();
}
