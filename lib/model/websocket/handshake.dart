class HandShake {
  int id;
  String method;
  HandShakeData data;

  HandShake({
    this.id,
    this.method = 'hello',
    this.data,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data,
    };
  }

  HandShake.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    method = json['method'];
    data = json['data'];
  }
}

class HandShakeData {
  String ver;
  String platf;
  String lang;
  String dev;
  String push;
  String ua;
  String project;
  String appver;

  HandShakeData({
    this.ver = '1.0',
    this.platf = 'android',
    this.lang = 'ru',
    this.push,
    this.dev = '1',
    this.ua = 'android',
    this.project = 'keyou',
    this.appver = '1.0.0'
  });

  HandShakeData.fromJson(Map<String, dynamic> json) {
    ver = json['ver'];
    platf = json['platf'];
    lang = json['lang'];
    dev = json['dev'];
    push = json['push'];
    ua = json['ua'];
    project = json['project'];
    appver = json['appver'];
  }

  
  Map<String, dynamic> toJson() {
    return {
      'ver': ver,
      'platf': platf,
      'lang': lang,
      'dev': dev,
      'push': push,
      'ua': ua,
      'project': project,
      'appver': appver,
    };
  }
}
