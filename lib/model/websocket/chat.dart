import 'package:keYou/data/app.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:flutter/cupertino.dart';

class ChatDialogData {
  String owner;
  String topic;
  String updated;
  String touched;
  int seq;
  int read;
  int recv;
  ShortUserProfile public;
  LastMessage lastMessage;
  bool typing;

  ChatDialogData({
    this.owner,
    this.topic,
    this.updated,
    this.touched,
    this.seq = 0,
    this.read = 0,
    this.recv = 0,
    this.public,
    this.lastMessage,
    this.typing = false,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': App.me.user.id,
      'topic': topic,
      'updated': updated,
      'touched': touched,
      'seq': seq,
      'read': read,
      'recv': recv,
      'public': public == null ? null : public?.toJson(),
      'lastMessage': lastMessage == null ? null : lastMessage.toJson(),
      'typing': typing == null ? false : typing,
    };
  }

  static ChatDialogData fromJson(Map<String, dynamic> json) {
    return ChatDialogData(
      owner: json['owner'],
      topic: json['topic'],
      updated: json['updated'],
      touched: json['touched'],
      seq: json['seq'] == null ? 0 : json['seq'],
      read: json['read'] == null ? 0 : json['read'],
      recv: json['recv'] == null ? 0 : json['recv'],
      public: json['public'] == null
          ? null
          : ShortUserProfile.fromJson(json['public']),
      lastMessage: json['lastMessage'] == null ? null : LastMessage.fromJson(json['lastMessage']),
      typing: json['typing'] == null ? false : json['typing'],
    );
  }
}

class MessageItemHead {
  final List<dynamic> attachments;

  MessageItemHead({this.attachments});

  Map<String, dynamic> toJson() {
    return {
      'attachments': attachments,
    };
  }

  static MessageItemHead fromJson(Map json) {
    return MessageItemHead(
      attachments: json == null ? null : json['attachments'],
    );
  }
}

class ChatMessageItemWidget {
  int seq;
  int type;
  dynamic content;
  bool incoming;
  String ts;
  bool read;
  String topic;
  String from;
  String imageUrl;
  bool loading;
  GestureTapCallback removeLoadingPhoto;

  ChatMessageItemWidget({
    this.seq,
    this.type,
    this.content,
    this.incoming = false,
    this.ts = '',
    this.read = true,
    this.from,
    this.topic,
    this.imageUrl = '',
    this.loading = false,
    this.removeLoadingPhoto,
  });

}

class ChatMessage {
  final String topic;
  final String from;
  final String ts;
  final int seq;
  final String content;
  final MessageItemHead head;

  ChatMessage({
    this.topic,
    this.from,
    this.ts,
    this.seq,
    this.content,
    this.head,
  });

  Map<String, dynamic> toJson() {
    return {
      'topic': topic,
      'from': from,
      'ts': ts,
      'seq': seq,
      'content': content,
      'head': head == null ? null : head.toJson(),
    };
  }

  static ChatMessage fromJson(Map<String, dynamic> json) {
    return ChatMessage(
      topic: json['topic'],
      from: json['from'],
      ts: json['ts'],
      seq: json['seq'],
      content: json['content'],
      head: json['head'] == null ? null : MessageItemHead.fromJson(json['head']),
    );
  }
}


class LastMessage {
  String content;
  int seq;
  String ts;
  String from;
  int type;

  LastMessage({this.content, this.seq, this.ts, this.from, this.type = 0});

  Map<String, dynamic> toJson() {
    return {
      'content': content,
      'seq': seq,
      'ts': ts,
      'from': from,
      'type': type,
    };
  }

  static LastMessage fromJson(Map<String, dynamic> json) {
    return LastMessage(
      content: json['content'],
      seq: json['seq'],
      ts: json['ts'],
      from: json['from'],
      type: json == null ? null : json['type']
    );
  }
}


class MessagesList {
  int count;
  List<ChatMessage> messages;

  MessagesList({
    this.count,
    this.messages,
  });

  Map<String, dynamic> toJson() {
    return {
      "count": count,
      "message": messages == null || messages.length == 0 ? [] : messages.map((message) => message.toJson()).toList(),
    };
  }

  static MessagesList fromJson(Map<String, dynamic> json) {
    return MessagesList(
      count: json['count'],
      messages: json == null || json['messages'] == null ? null : List<ChatMessage>.from(json['messages']?.map((item) => ChatMessage.fromJson(item))),
    );
  }
}