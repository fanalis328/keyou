class RegisterAccount {
  int id;
  String method;

  RegisterAccount({
    this.id,
    this.method = 'registerAccount',
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
    };
  }
}