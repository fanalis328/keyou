class Validate {
  int id;
  String method;
  ValidateData data;

  Validate({
    this.id,
    this.method = "validate",
    this.data,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data,
    };
  }
}

class  ValidateData {
  String phone;

  ValidateData({this.phone});

  Map<String, dynamic> toJson() {
    return { 'phone': phone };
  }
}