class  RegistrationRule {
  final String title;
  final String src1;
  final String src2;

  RegistrationRule({this.title, this.src1, this.src2});
}