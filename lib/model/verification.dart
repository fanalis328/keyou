class VerificationApproved {
  String owner;
  bool approved;

  VerificationApproved({
    this.owner,
    this.approved,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': owner,
      'approved': approved,
    };
  }

  static VerificationApproved fromJson(Map<String, dynamic> json) {
    return VerificationApproved(
      owner: json == null ? null : json['owner'],
      approved: json == null ? null : json['approved'],
    );
  }
}

class VerificationPhoto {
  String owner;
  String imagePath;

  VerificationPhoto({
    this.owner,
    this.imagePath,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': owner,
      'imagePath': imagePath,
    };
  }

  static VerificationPhoto fromJson(Map<String, dynamic> json) {
    return VerificationPhoto(
      owner: json == null ? null : json['owner'],
      imagePath: json == null ? null : json['imagePath'],
    );
  }
}