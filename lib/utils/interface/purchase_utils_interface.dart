import 'package:keYou/model/purchase.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

abstract class IPurchaseUtils {
  Future<void> afterProductPurchase(Map purchaseData, [callback]);

  void buyProduct(ProductDetails product);

  Future<List<PurchaseDetails>> getPastPurchases();

  Future<List<ProductDetails>> getProducts(List<String> productIdList);

  bool getPurchaseStatus(String purchaseExpiredTs);

  PurchaseDetails hasPurchased(String productID, List<PurchaseDetails> purchases);

  Future<void> onlineSearchPurchaseCallback(Map purchaseData, [dynamic callback]);

  Future<void> successPurchseDialog(context, [String title]);

  Future<void> updateCurrentPurchase();

  Future<void> checkAndUpdateCurrentDenimBlackPurchase(UpdatePurchaseData purchaseData);

  InAppPurchaseConnection iap;
}
