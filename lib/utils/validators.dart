class ValidatorUtils {
  static RegExp clearPhoneNumberExp = RegExp(r"[^+0-9]+");
  
  static bool validatePhoneNumberForLogin(String inputValue, String userPhone) {
    var formattedPhone = inputValue.replaceAll(clearPhoneNumberExp, '');

    if (formattedPhone == userPhone) {
      return true;
    } else {
      return false;
    }
  }

  static bool validatePhoneNumberForRegistration(String inputValue) {
    var formattedPhone = inputValue.replaceAll(clearPhoneNumberExp, '');

    if (formattedPhone.length > 10 && formattedPhone.length <= 15  && formattedPhone[0] == '+') {
      return true;
    } else {
      return false;
    }
  }

  static bool validateIsEmptyField(String value) {
    if(value?.length == 0 || value == null) {
      return false;
    } else {
      return true;
    }
  }

  static bool matchValidation(String inputValue, String realValue) {
    if(inputValue != '' && inputValue == realValue) {
      return true;
    } else {
      return false;
    }
  }

  static bool minLength(String value, int minLength) {
    if (value.length < minLength) {
      return false;
    } else {
      return true;
    }
  } 
}