import 'dart:io' show Platform;
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/purchase.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import '../locator.dart';
import 'interface/purchase_utils_interface.dart';

const String onlineSearch = 'online_search';
const String raiseProfile = 'raise_profile';
const String m3 = '3m';
const String w13dFree = '1w_3d_free';
const String m1 = '1m';
const String d3NoSub = '3d_nosub';

List<String> products = [onlineSearch, raiseProfile, m3, w13dFree, m1, d3NoSub];

class PurchaseUtils implements IPurchaseUtils {
  InAppPurchaseConnection iap = InAppPurchaseConnection.instance;

  buyProduct(ProductDetails product) {
    final PurchaseParam purchaseParam = PurchaseParam(productDetails: product);
    iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
  }

  hasPurchased(String productID, List<PurchaseDetails> purchases) {
    return purchases.firstWhere((purchase) => purchase.productID == productID,
        orElse: () => null);
  }

  getProducts(List<String> productIdList) async {
    Set<String> ids = Set.from(productIdList);
    ProductDetailsResponse response = await iap.queryProductDetails(ids);
    return response.productDetails;
  }

  getPastPurchases() async {
    QueryPurchaseDetailsResponse response = await iap.queryPastPurchases();
    for (PurchaseDetails purchase in response.pastPurchases) {
      if (Platform.isIOS) {
        iap.completePurchase(purchase);
      }
    }

    return response.pastPurchases;
  }

  getPurchaseStatus(String purchaseExpiredTs) {
    if (purchaseExpiredTs == null || purchaseExpiredTs == '') return false;
    DateTime date = DateTime.now();
    bool purchaseExpired = date.isAfter(DateTime.parse(purchaseExpiredTs));
    return purchaseExpired;
  }

  afterProductPurchase(Map purchaseData, [dynamic callback]) async {
    if (purchaseData != null && purchaseData['ctrl']['code'] == 200 ||
        purchaseData['ctrl']['code'] == 202) {
      userApi.getAccount((data) {
        ProfileUtils.updateUserProfile(data, callback);
      });
    }
  }

  onlineSearchPurchaseCallback(Map purchaseData, [dynamic callback]) async {
    if (purchaseData != null && purchaseData['ctrl']['code'] == 200 ||
        purchaseData['ctrl']['code'] == 202) {
      if (callback != null) callback();
      // userApi.getAccount((data) {
      //   ProfileUtils.updateUserProfile(data, callback);
      // });
    }
  }

  updateCurrentPurchase() async {
    print(['_____ UPDATE CURRENT PURCHASE ON SERVER _____']);
    UpdatePurchaseData purchaseData = await purchaseDao.getPurchaseData();
    if (purchaseData != null && purchaseData.token != null) {
      switch (purchaseData.product) {
        case w13dFree:
        case d3NoSub:
        case m1:
        case m3:
          print(['DEF PURCHASE']);
          purchaseApi.updatePurchase(purchaseData);
          break;
        case onlineSearch:
          purchaseApi.buyOnlineSearch(purchaseData.token);
          break;
        case raiseProfile:
          print(['RAISE PURCHASE']);
          purchaseApi.raiseProfile(purchaseData.token);
          break;
        default:
          return;
      }
    }
  }

  checkAndUpdateCurrentDenimBlackPurchase(
      UpdatePurchaseData purchaseData) async {
    // if (App.me.user.profile.gender == 1) return;
    UpdatePurchaseData purchase = await purchaseDao.getPurchaseData();
    print('purchase___________ $purchase');
    if (purchase == null) {
      await purchaseDao.setPurchaseData(purchaseData);
    } else {
      purchaseDao.updatePurchase(purchaseData);
    }

    if (App.me.user.subscription != null) {
      await purchaseDao.setTrialPurchaseStatus(
          TrialPurchaseStatus(owner: App.me.user.id, status: true));
    }
  }

  successPurchseDialog(context, [String title]) {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          body: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 10),
                Image.asset(
                  'assets/images/icons/section-subscription-symbol.jpg',
                  width: 64,
                ),
                SizedBox(height: 16),
                Text(
                  AppTranslations.of(context)
                      .text('denim_black_activated_title'),
                  style: DenimFonts.titleDefault(context),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 16),
                Text(
                  title == '' || title == null
                      ? AppTranslations.of(context)
                          .text('denim_black_activated_description')
                      : title,
                  style: DenimFonts.titleBody(context),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
        );
      },
    );
  }
}
