import 'dart:async';
import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/model/purchase.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/route_paths.dart' as routes;
import 'package:keYou/services/navigation_service.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/utils/verification.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:keYou/pages/denim_black/denim_black.dart' as products;

class AuthUtils {
  static Future<String> getToken() async {
    return await authDao.getToken();
  }

  static loginPhoneCallback(Map data) async {
    if (data != null && data['ctrl']['code'] == 200) {
      String token = data['data']['token'];
      String userId = data['data']['user'];
      if (data['data']['token'] != null) {
        await authDao.setToken(token);
        App.me.token = token;
      }
      if (userId != null) {
        await authDao.setCurrentUserId(userId);
        App.me.user.id = userId;
      }
    } else {
      await authDao.deleteToken();
      await authDao.deleteCurrentUserId();
    }
  }

  static loginTokenCallback(Map data) async {
    NavigationService _navigationService = locator<NavigationService>();
    ProfileData user = App.me.user;
    App.me.token = await authDao.getToken();
    if (data != null && data['ctrl']['code'] == 200) {
      userApi.getAccount(([Map data]) async {
        if (data != null &&
            data['ctrl']['code'] == 200 &&
            data['data']['registered'] != null &&
            !data['data']['blocked'] &&
            !data['data']['deleted']) {
          user.id = data['data']['id'];

          ProfileUtils.updateUserProfile(data, (ProfileData profile) async {
            if (profile != null) {
              Timer(Duration(milliseconds: 300), () async {
                _navigationService.navigateTo(routes.TabsRoute);
                await Future.delayed(Duration(microseconds: 200), () async {

                  List<PurchaseDetails> purchases = await purchaseUtils.getPastPurchases();
                  PurchaseDetails trial = purchaseUtils.hasPurchased(products.w13dFree, purchases);
                  TrialPurchaseStatus trialPurchaseStatus = await purchaseDao.getTrialPurchaseStatus();
                  // check trial purchase status
                  if(trial == null && trialPurchaseStatus == null && App.me.user.profile.gender == 1) {
                    _navigationService.navigateTo(routes.Trial);
                  }
                });
                VerificationUtils.checkVerificationCases(data);
              });
            }
            await ChatUtils.updateDialogs();
            purchaseUtils.updateCurrentPurchase();
          });
        } else if (data['data']['blocked']) {
          Timer(Duration(milliseconds: 500), () {
            _navigationService.navigateTo(
              routes.BlockingProfile,
              arguments:
                  'Ваша анкета \nзаблокирована по \nрешению администрации',
            );
          });
        } else if (!data['data']['blocked'] && data['data']['deleted']) {
          Timer(Duration(milliseconds: 500), () {
            _navigationService.navigateTo(
              routes.BlockingProfile,
              arguments: 'Ваша анкета \nбыла удалена, \nзарегистрируйте новую',
            );
          });
        } else {
          authApi.logout();
          return;
        }
      });
    } else {
      await Future.delayed(Duration(seconds: 1));
      _navigationService.navigateTo(routes.HomeRoute);
    }
  }

  static cleanLoginTokenCallback(Map data) async {
    if (data != null && data['ctrl']['code'] == 201) {
      print(['______ clean HANDSHAKE_____']);
      purchaseUtils.updateCurrentPurchase();
      ProfileUtils.updateUserProfile();
      await ChatUtils.updateDialogs();
    }
  }

  static cleanLogoutCallback([Map data]) async {
    if (data != null && data['ctrl']['code'] == 200) {
      await authDao.deleteCurrentUserId();
      await authDao.deleteToken();
      App.me.unreadMessageController.close();
      App.me.newViewsController.close();
    } else {
      return;
    }
  }
}
