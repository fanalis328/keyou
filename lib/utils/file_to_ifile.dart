import 'dart:io';
import 'package:keYou/services/image_picker/image_file_model.dart';

ImageFile fileToImageFile(File file) {
  return ImageFile(
    path: file.path,
  );
}
