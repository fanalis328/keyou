import 'dart:convert';
import 'dart:math';
import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/model/city.dart';
import 'package:keYou/model/phone_code.dart';
import 'package:flutter/services.dart';

class Repository {
  static final Repository _repo = Repository._internal();

  static Repository get() {
    return _repo;
  }

  Repository._internal();

  Future<String> getSmsCodeFromFakeApi(String phone) {
    String randomNum() {
      var random = new Random();
      return random.nextInt(9999).toString();
    }

    return Future.delayed(Duration(seconds: 1), () => randomNum());
  }

  Future<List<City>> getCity() async {
    List<City> cities = [];
    var response = await citiesDao.getCities();

    for (var city in response) {
      cities.add(City.fromJson(city));
    }

    return await new Future.delayed(new Duration(milliseconds: 500), () {
      return cities;
    });
  }

  Future<List<PhoneCode>> getPhoneCode() async {
    List<PhoneCode> countryPhoneCodeList = [];

    var codes = await rootBundle
        .loadString('assets/local_data/country_phone_codes_data.json');
    var parsed = jsonDecode(codes);

    for (var code in parsed) {
      countryPhoneCodeList.add(PhoneCode.fromJson(code));
    }

    return countryPhoneCodeList;
  }

  Future<List<Country>> getCountries() async {
    List<Country> countryList = [];
    await App.me
        .sendLegacy({'id': socket.requestId(), 'method': 'getCountries'});
    return countryList;
  }
}
