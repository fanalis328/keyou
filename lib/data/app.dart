import 'dart:async';
import 'dart:convert';
import 'package:device_id/device_id.dart';
import 'package:keYou/db/database.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/model/websocket/handshake.dart';
import 'package:keYou/model/websocket/views.dart';
import 'package:keYou/services/navigation_service.dart';
import 'package:keYou/utils/auth.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/utils/local_notications.dart';
import 'package:keYou/utils/verification.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:keYou/route_paths.dart' as routes;

class App {
  static final App _app = App._();
  static App get me => _app;

  BuildContext buildContext;
  ProfileData user = ProfileData();
  AppDatabase db = AppDatabase();
  String token;
  ProfileSettings settings = ProfileSettings();
  List<ShortUserProfile> searchUsersList = List<ShortUserProfile>();
  List<ShortViewUserProfile> viewUserList = List<ShortViewUserProfile>();
  App._();

  /// ----------------------------------------------------------
  /// APP LOCALE
  /// ----------------------------------------------------------
  final List<String> supportedLanguages = [
    "English",
    "Russian",
  ];

  final List<String> supportedLanguagesCodes = [
    "en",
    "ru",
  ];

  Iterable<Locale> supportedLocales() => supportedLanguagesCodes.map<Locale>(
        (language) => Locale(
          language,
          "",
        ),
      );

  LocaleChangeCallback onLocaleChanged;

  /// ----------------------------------------------------------
  /// APP WEBSOCKET
  /// ----------------------------------------------------------

  Map<int, dynamic> requests = {};
  final notifications = FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings settingsAndroid;
  IOSInitializationSettings settingsIOS;
  StreamController<bool> newViewsController;
  StreamController<bool> unreadMessageController;
  Stream<bool> newViewsStream;
  Stream<bool> unreadMessageStream;
  NavigationService _navigationService = locator<NavigationService>();


  initWebsocketConnection([bool handshake = true, String pushToken]) {
    close();
    newViewsController = StreamController();
    unreadMessageController = StreamController();
    newViewsStream = newViewsController.stream.asBroadcastStream();
    unreadMessageStream = unreadMessageController.stream.asBroadcastStream();

    socket.initCommunication();
    socket.addListener(_onMessageReceived);
    handShake(handshake, pushToken);

    // TODO: create class or function for this
    if (!kIsWeb) {
      settingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
      settingsIOS = IOSInitializationSettings(
          onDidReceiveLocalNotification: (
        id,
        title,
        body,
        payload,
      ) =>
              onSelectNotification(payload));

      notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification,
      );
    }

    socket.addListener(notificationListener);
  }

  close() {
    socket.removeListener(_onMessageReceived);
    socket.removeListener(notificationListener);
    socket.reset();
  }

  handShake(bool handshake, String pushToken) async {
    print(['___ handshake _____']);
    //TODO:: implement device id for web if needed
    String deviceid = await (kIsWeb ? Future.value('') : DeviceId.getID);
    String token = await authDao.getToken();
    String id = await authDao.getCurrentUserId();
    // String locale = await localeService.getLocale();
    String locale = 'en';
    await this.sendLegacy(
      HandShake(
        id: socket.requestId(),
        data: HandShakeData(
          dev: deviceid,
          push: pushToken,
          lang: locale.split('_').first,
        ),
      ).toJson(),
      (Map data) {
        if(data['ctrl']['code'] == 426) {
          _navigationService.navigateTo(routes.Updated);
        }
      }
    );

    if (handshake) {
      if (token != null || id != null) {
        await authApi.login(token, 'token', AuthUtils.loginTokenCallback);
      } else {
        return;
      }
    } else {
      print(['___ relogin on handshake _____']);
      await authApi.login(
        token,
        'token',
        AuthUtils.cleanLoginTokenCallback,
      );
    }
  }

  notificationListener(data) async {
    Map message = json.decode(data);
    if (message != null && message['notify'] != null) {
      switch (message['notify']['action']) {
        case 'newmsg':
          addTabIndicator(unreadMessageController, true);
          showOngoingNotification(notifications,
              title: 'У вас новое сообщение',
              body: '',
              payload:
                  "{\n\t\"data\" : \"${message['notify']['src']}\", \n\t\"event\": \"newmsg\"}");
          await ChatUtils.updateDialogs();
          break;
        case 'updacc':
        case 'subscription':
          VerificationUtils.verificationListener();
          break;
        case 'newview':
          addTabIndicator(newViewsController, true);
          showOngoingNotification(notifications,
              title: 'У вас новый просмотр',
              body: '',
              payload:
                  "{\n\t\"data\" : \"${message['notify']['src']}\", \n\t\"event\": \"newview\"}");
          break;
        default:
          return;
      }
    } else if (message != null && message['chat'] != null) {
      await ChatUtils.newMessageListener(message, false);
    } else {
      return;
    }
  }

  Future onSelectNotification(String payload) async {
    if (payload == null || payload.length == 0) return;
  }

  _onMessageReceived(serverMessage) {
    Map message;
    int id;
    if (serverMessage != null) {
      message = json.decode(serverMessage);
      print(['___ message _____', message]);
      id = message['ctrl'] == null ? null : message['ctrl']['id'];
      if (requests.containsKey(id) && id != null) {
        requests[id](message);
      }
    }
  }

  sendLegacy(Map data, [dynamic callback]) {
    int id = data['id'];
    String parsedData = jsonEncode(data);
    print(['___ parsedData _____', parsedData]);
    socket.send(parsedData);
    if (callback != null) {
      requests.putIfAbsent(id, () => callback);
    } else {
      return;
    }
  }

  Future<dynamic> send(Map data) {
    final completer = Completer<dynamic>();
    int id = data['id'];
    String parsedData = jsonEncode(data);
    print(['___ parsedData _____', parsedData]);
    socket.send(parsedData);
    requests.putIfAbsent(id, () => completer.complete());
    return completer.future;
  }

  addTabIndicator(StreamController streamController, bool param) {
    if (streamController.isClosed || streamController.sink == null) {
      return;
    } else {
      streamController.sink.add(param);
    }
  }
}

typedef void LocaleChangeCallback(Locale locale);

Future<String> _getDeviceId() {
  // TODO: add user agent
  if (kIsWeb) return Future.value('');
  return DeviceId.getID;
}
