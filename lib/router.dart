import 'package:keYou/pages/denim_black/denim_black_trial.dart';
import 'package:keYou/pages/login_page.dart';
import 'package:keYou/pages/moderation/universal_blocking_profile.dart';
import 'package:keYou/pages/moderation/verification_user_photo.dart';
import 'package:keYou/pages/registration_page.dart';
import 'package:keYou/pages/registration_steps/photo_registration.dart';
import 'package:keYou/pages/registration_steps/photo_verification_registration.dart';
import 'package:keYou/pages/splash_screen.dart';
import 'package:keYou/pages/start_page.dart';
import 'package:keYou/pages/tabs/chat/chat_dialog.dart';
import 'package:keYou/pages/tabs_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keYou/pages/updated_screen.dart';
import 'package:keYou/route_paths.dart' as routes;

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case routes.LoginRoute:
      return MaterialPageRoute(builder: (context) => LoginPage());
      break;
    case routes.HomeRoute:
      return MaterialPageRoute(builder: (context) => StartPage());
      break;
    case routes.RegistrationRoute:
      return MaterialPageRoute(builder: (context) => RegistrationPage());
      break;
    case routes.TabsRoute:
      return MaterialPageRoute(builder: (context) => TabsPage(initialIndex: settings.arguments == null ? 0 : settings.arguments));
      break;
    case routes.Account:
      return MaterialPageRoute(builder: (context) => TabsPage(verification: settings.arguments));
      break;
    case routes.ChatWithUserRoute:
      return MaterialPageRoute(builder: (context) => ChatDialogPage(topic: settings.arguments,));
      break;
    case routes.BlockingProfile:
      return MaterialPageRoute(builder: (context) => UniversalBlockingProfile(title: settings.arguments,));
      break;
    case routes.ModerationProfile:
      return MaterialPageRoute(builder: (context) => VerificationUserPhoto(data: settings.arguments));
      break;
    case routes.VerificationPhoto:
      return MaterialPageRoute(builder: (context) => PhotoVerificationStep(reVerification: settings.arguments,));
      break;
    case routes.LoadNewAvatar:
      return MaterialPageRoute(builder: (context) => PhotoRegistrationStep(reVerification: settings.arguments,));
      break;
    case routes.Trial:
      return MaterialPageRoute(builder: (context) => DenimBlackTrial());
      break;
    case routes.SplashScreenRoute: 
      return MaterialPageRoute(builder: (context) => SplashScreen());
      break;
    case routes.Updated: 
      return MaterialPageRoute(builder: (context) => UpdatedScreen());
      break;
    default:
      return MaterialPageRoute(builder: (context) => StartPage());
  }
}