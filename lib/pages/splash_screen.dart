import 'package:keYou/denim_icons.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: loft,
    );
  }

  Widget denim = Center(
    child: Container(
        padding: EdgeInsets.all(15),
        child: Icon(
          DenimIcon.section_settings_subscription,
          color: Colors.white,
          size: 150,
        )),
  );

  Widget loft = Center(
    child: Container(
      padding: EdgeInsets.all(15),
      child: FadeInImage(
        fadeInDuration: Duration(milliseconds: 250),
        placeholder: MemoryImage(kTransparentImage),
        image: AssetImage('assets/images/ky/keyou-logo.png'),
        fit: BoxFit.cover,
      ),
    ),
  );
}
