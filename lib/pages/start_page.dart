import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/pages/login_page.dart';
import 'package:keYou/pages/registration_page.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/locator.dart';
import 'package:flutter/material.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  @override
  void initState() {
    firebaseAnalytics.logAppOpen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          return;
        },
        child: keyYouScreen(context),
      ),
    );
  }

  Widget keyYouScreen(context) {
    return SafeArea(
      top: true,
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/ky/main_bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Image.asset(
              'assets/images/ky/keyou-logo.png',
              width: 240,
            ),
            SizedBox(
              height: 20,
            ),
            // Text('DeV 4.2.1 + 621', style: DenimFonts.titleXLarge(context, DenimColors.colorSpecialOnlineDefault),),
            Text(
              'Premium Gay Dating',
              style: DenimFonts.titleLarge(context, Colors.white),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 25),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 256,
                    child: DenimButton(
                        title: AppTranslations.of(context)
                            .text('btn_сreate_account_title'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => RegistrationPage(),
                            ),
                          );
                          firebaseAnalytics.sendAnalyticsEvent('registration');
                          yandexAnalytics.sendYandexAnalyticsEvent(
                              yandexAnalytics.eventRegistration);
                        }),
                  ),
                ],
              ),
            ),
            SizedBox(height: 11),
            Container(
              width: 256,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white.withOpacity(0.4),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: SizedBox(
                      height: 50,
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginPage(),
                            ),
                          );
                          yandexAnalytics.sendYandexAnalyticsEvent(
                              yandexAnalytics.eventLogin);
                        },
                        child: Text(
                          AppTranslations.of(context).text('btn_login_title'),
                          style: DenimFonts.titleBtnDefault(
                            context,
                            Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        color: DenimColors.colorBlackSecondary,
                        textColor: Colors.white,
                        highlightedBorderColor: DenimColors.colorPrimary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40)
          ],
        ),
      ),
    );
  }

  Widget denimScreen(context) {
    return SafeArea(
      top: true,
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/start-screen.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Image.asset(
              'assets/images/white-logo.png',
              width: 180,
            ),
            SizedBox(
              height: 8,
            ),
            // Text('DeV 4.2.1 + 621', style: DenimFonts.titleXLarge(context, DenimColors.colorSpecialOnlineDefault),),
            Text(
              AppTranslations.of(context).text('start_sceen_description'),
              style: DenimFonts.titleBody(context, Colors.white),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 70),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 216,
                    child: DenimButton(
                        title: AppTranslations.of(context)
                            .text('btn_сreate_account_title'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => RegistrationPage(),
                            ),
                          );
                          firebaseAnalytics.sendAnalyticsEvent('registration');
                          yandexAnalytics.sendYandexAnalyticsEvent(
                              yandexAnalytics.eventRegistration);
                        }),
                  ),
                ],
              ),
            ),
            SizedBox(height: 13),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              width: 276,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: SizedBox(
                      height: 50,
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginPage(),
                            ),
                          );
                          yandexAnalytics.sendYandexAnalyticsEvent(
                              yandexAnalytics.eventLogin);
                        },
                        child: Text(
                          AppTranslations.of(context).text('btn_login_title'),
                          style: DenimFonts.titleBtnDefault(
                            context,
                            Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        color: Colors.transparent,
                        textColor: Colors.white,
                        borderSide:
                            BorderSide(color: Colors.white.withOpacity(0.4)),
                        highlightedBorderColor: DenimColors.colorPrimary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40)
          ],
        ),
      ),
    );
  }

  Widget loftScreen(context) {
    return SafeArea(
      top: true,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            height: 400,
            child: Image.asset(
              'assets/images/loft-logo.jpg',
              width: 240,
            ),
          ),
          // Text('DeV 4.2.1 + 621', style: DenimFonts.titleXLarge(context, DenimColors.colorSpecialOnlineDefault),),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: DenimButton(
                          title: AppTranslations.of(context)
                              .text('btn_сreate_account_title'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => RegistrationPage(),
                              ),
                            );
                            firebaseAnalytics
                                .sendAnalyticsEvent('registration');
                            yandexAnalytics.sendYandexAnalyticsEvent(
                                yandexAnalytics.eventRegistration);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: SizedBox(
                          height: 50,
                          child: OutlineButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                                ),
                              );
                              yandexAnalytics.sendYandexAnalyticsEvent(
                                  yandexAnalytics.eventLogin);
                            },
                            child: Text(
                              AppTranslations.of(context)
                                  .text('btn_login_title'),
                              style: DenimFonts.titleBtnDefault(
                                context,
                                Colors.black,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            color: Colors.transparent,
                            textColor: Colors.white,
                            borderSide:
                                BorderSide(color: DenimColors.colorBlackLine),
                            highlightedBorderColor: Colors.black,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 60)
        ],
      ),
    );
  }
}
