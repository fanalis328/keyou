import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:keYou/widgets/circular_progress.dart';
import 'package:keYou/widgets/gallery/gallery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../../locator.dart';

class BaseProfileViewLayout extends StatefulWidget {
  final Icon floatBtnIcon;
  final bool showCurrentUser;
  final List<ProfileActionButton> profileActionButtonList;
  final List<UserPhoto> galleryItems;
  final bool showComplainButton;

  BaseProfileViewLayout({
    this.floatBtnIcon,
    this.showCurrentUser = false,
    this.profileActionButtonList,
    this.galleryItems,
    this.showComplainButton = false,
  });

  @override
  _BaseProfileViewLayoutState createState() => _BaseProfileViewLayoutState();
}

class _BaseProfileViewLayoutState extends State<BaseProfileViewLayout> {
  bool toggleText = true;
  ProfileData user = App.me.user;
  bool showGallery = false;
  String token;
  int moderatedUserStatus;

  @override
  void initState() {
    getToken();
    getAccountInfo();
    super.initState();
  }

  getToken() async {
    token = await authDao.getToken();
  }

  getAccountInfo() {
    userApi.getAccount((Map data) {
      if (data != null && data['ctrl']['code'] == 200) {
        if (!mounted) return;
        setState(() {
          moderatedUserStatus = data['data']['moderated'];
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder(
        // future: ProfileUtils.getCleanProfile(),
        future: userDao.getCurrentUser(user),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return CircularLoadingIndicator();
          } else {
            return Stack(
              children: <Widget>[
                ListView(
                  children: <Widget>[
                    FutureBuilder(
                      future: ProfileUtils.getUserImages(),
                      builder: (context, snapshot) {
                        if (snapshot.data == null) {
                          return Text('ERROR');
                        } else {
                          return Gallery(
                            galleryItems: snapshot.data,
                            token: token,
                            moderatedUser: moderatedUserStatus,
                          );
                        }
                      },
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${snapshot.data?.profile?.name?.trim()}, ${ProfileUtils.getAge(snapshot.data?.profile?.birthday)}',
                                      style: DenimFonts.titleLarge(context),
                                      textAlign: TextAlign.left,
                                    ),
                                    SizedBox(height: 3),
                                    Text(
                                      AppTranslations.of(context)
                                          .text('online'),
                                      style: DenimFonts.titleFootnote(
                                        context,
                                        DenimColors.colorBlackTertiary,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(15, 0, 15, 120),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  toggleText &&
                                          snapshot.data.profile.about.length >
                                              80
                                      ? snapshot.data.profile.about
                                              .substring(0, 80) +
                                          '...'
                                      : snapshot.data.profile.about,
                                  maxLines: 100,
                                  style: DenimFonts.titleBody(context),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: 4),
                                Visibility(
                                  visible:
                                      snapshot.data.profile.about.length <= 80
                                          ? false
                                          : true,
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          toggleText = !toggleText;
                                        });
                                      },
                                      child: Text(
                                        toggleText
                                            ? AppTranslations.of(context)
                                                .text('expand')
                                            : AppTranslations.of(context)
                                                .text('roll_up'),
                                        style: DenimFonts.titleFootnote(
                                          context,
                                          DenimColors.colorBlackTertiary,
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 16),
                                buildUserInformationRow(
                                  AppTranslations.of(context)
                                      .text('profile_country_title'),
                                  snapshot.data.profile.country.title
                                      .toString(),
                                ),
                                buildUserInformationRow(
                                  AppTranslations.of(context)
                                      .text('profile_location_title'),
                                  snapshot.data.profile.city.title.toString(),
                                ),
                                // buildUserInformationRow(
                                //   AppTranslations.of(context)
                                //       .text('profile_citizenship_title'),
                                //   snapshot.data.profile.citizenship.title
                                //       .toString(),
                                // ),
                                buildUserInformationRow(
                                  AppTranslations.of(context)
                                      .text('profile_appearance_title'),
                                  '${AppTranslations.of(context).text('profile_height_title')} ${snapshot.data?.profile?.height} inch, ${AppTranslations.of(context).text('profile_weight_title')} ${snapshot.data?.profile?.weight} lb',
                                ),
                                Visibility(
                                  visible: snapshot.data?.profile?.zodiac ==
                                              0 ||
                                          snapshot.data?.profile?.zodiac == null
                                      ? false
                                      : true,
                                  child: buildUserInformationRow(
                                    AppTranslations.of(context)
                                        .text('profile_zodiak_title'),
                                    AppTranslations.of(context).text(
                                      'zodiac_${snapshot.data?.profile?.zodiac}',
                                    ),
                                  ),
                                ),
                                SizedBox(height: 16),
                                Visibility(
                                  visible: widget.showComplainButton,
                                  child: BorderListItem(
                                    title: Text(
                                      AppTranslations.of(context)
                                          .text('сomplain'),
                                      style: DenimFonts.titleBtnDefault(
                                        context,
                                        DenimColors.colorBlackSecondary,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    onTap: () {},
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 25, 0, 20),
                    child: buidProfileActionButtonList(),
                  ),
                ),
                SafeArea(
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(10),
                        width: 40,
                        height: 40,
                        child: RawMaterialButton(
                          shape: CircleBorder(),
                          fillColor: DenimColors.colorTranslucentPrimary,
                          child: widget.floatBtnIcon,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  Widget buildUserInformationRow(String title, String subtitle, [callback]) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Card(
              elevation: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    callback == null ? title : callback(),
                    style: DenimFonts.titleDefault(context),
                  ),
                  SizedBox(height: 4),
                  Text(
                    subtitle,
                    style: DenimFonts.titleBody(
                      context,
                      DenimColors.colorBlackSecondary,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Row buidProfileActionButtonList() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: widget.profileActionButtonList
          .map(
            (button) => buildProfileActionButton(button.icon, button.onTap),
          )
          .toList(),
    );
  }

  Widget buildProfileActionButton(
    Icon icon,
    GestureTapCallback onTap,
  ) {
    return Container(
      width: 72,
      height: 72,
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(50),
        border: Border.all(width: 1, color: DenimColors.colorTranslucentLine),
        boxShadow: [
          BoxShadow(
            color: DenimColors.colorTranslucentLine,
            blurRadius: 10,
            offset: Offset(0, 6),
          ),
        ],
      ),
      child: RawMaterialButton(
        shape: CircleBorder(),
        child: icon,
        onPressed: onTap,
      ),
    );
  }
}

class ProfileActionButton {
  final Icon icon;
  final GestureTapCallback onTap;

  ProfileActionButton({this.icon, this.onTap});
}
