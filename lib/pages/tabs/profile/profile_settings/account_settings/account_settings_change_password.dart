import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/pages/password.dart';
import 'package:keYou/utils/validators.dart';
import 'package:flutter/material.dart';

enum ChangePasswordPageState {
  OLD_PASSWORD,
  NEW_PASSWORD,
  REPEAT_NEW_PASSWORD,
}

class AccountSettingsChangePasswordPage extends StatefulWidget {
  @override
  _AccountSettingsChangePasswordPageState createState() =>
      _AccountSettingsChangePasswordPageState();
}

class _AccountSettingsChangePasswordPageState
    extends State<AccountSettingsChangePasswordPage> {
  bool toggleObscurePassword = true;
  TextEditingController _oldPasswordController = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();
  TextEditingController _repeatNewPasswordController = TextEditingController();

  bool passwordFieldError = false;
  String passwordFieldErrorText;

  ChangePasswordPageState pageState = ChangePasswordPageState.OLD_PASSWORD;
  @override
  Widget build(BuildContext context) {
    return buildChangePasswordPage();
  }

  buildChangePasswordPage() {
    switch (pageState) {
      case ChangePasswordPageState.OLD_PASSWORD:
        return PasswordPage(
          title: AppTranslations.of(context).text('enter_old_password'),
          backArrowAction: () => Navigator.pop(context),
          controller: _oldPasswordController,
          passwordFieldError: passwordFieldError,
          passwordFieldErrorText: passwordFieldErrorText,
          submitButtonAction: () {
            if (!ValidatorUtils.matchValidation(
              _oldPasswordController.text,
              '1',
            )) {
              setState(() {
                passwordFieldError = true;
                passwordFieldErrorText =
                    AppTranslations.of(context).text('wrong_password');
              });
              return;
            }
            setState(() {
              pageState = ChangePasswordPageState.NEW_PASSWORD;
            });
            resetPasswordError();
          },
        );
        break;
      case ChangePasswordPageState.NEW_PASSWORD:
        return PasswordPage(
          title: AppTranslations.of(context).text('create_new_password'),
          backArrowAction: () {
            setState(() {
              pageState = ChangePasswordPageState.OLD_PASSWORD;
              resetPasswordError();
            });
          },
          controller: _newPasswordController,
          passwordFieldError: passwordFieldError,
          passwordFieldErrorText: passwordFieldErrorText,
          submitButtonAction: () {
            if (!ValidatorUtils.minLength(
              _newPasswordController.text,
              6,
            )) {
              setState(() {
                passwordFieldError = true;
                passwordFieldErrorText =
                    AppTranslations.of(context).text('error_6_characters_2');
              });
              return;
            }
            setState(() {
              pageState = ChangePasswordPageState.REPEAT_NEW_PASSWORD;
            });
            resetPasswordError();
          },
        );
        break;
      case ChangePasswordPageState.REPEAT_NEW_PASSWORD:
        return PasswordPage(
          title: AppTranslations.of(context).text('repeat_new_password'),
          backArrowAction: () {
            setState(() {
              pageState = ChangePasswordPageState.NEW_PASSWORD;
              resetPasswordError();
            });
          },
          controller: _repeatNewPasswordController,
          passwordFieldError: passwordFieldError,
          passwordFieldErrorText: passwordFieldErrorText,
          submitButtonTitle: AppTranslations.of(context).text('btn_save_title'),
          submitButtonAction: () {
            if (!ValidatorUtils.matchValidation(
              _newPasswordController.text,
              _repeatNewPasswordController.text,
            )) {
              setState(() {
                passwordFieldError = true;
                passwordFieldErrorText = 'Пароли не совпадают';
              });
              return;
            }
          },
        );
        break;
      default:
        setState(() {
          pageState = ChangePasswordPageState.NEW_PASSWORD;
        });
    }
  }

  resetPasswordError() {
    setState(() {
      passwordFieldError = false;
    });
  }
}
