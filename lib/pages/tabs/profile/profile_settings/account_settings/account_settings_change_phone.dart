import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/pages/registration_steps/initial_registration.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/validators.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/input.dart';
import 'package:flutter/material.dart';

class AccountSettingsChangePhonePage extends StatefulWidget {
  @override
  _AccountSettingsChangePhonePageState createState() =>
      _AccountSettingsChangePhonePageState();
}

class _AccountSettingsChangePhonePageState
    extends State<AccountSettingsChangePhonePage> {
  TextEditingController _passwordController = TextEditingController();
  bool toggleObscurePassword = true;

  @override
  void initState() {
    _passwordController = TextEditingController(text: 'q1w2e3');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
          ),
          SizedBox(height: 10),
          Expanded(
            child: Container(
              constraints: BoxConstraints.expand(),
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    AppTranslations.of(context).text('enter_password'),
                    style: DenimFonts.titleCallout(context),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 10),
                  DenimInput(
                    autofocus: true,
                    controller: _passwordController,
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          toggleObscurePassword = !toggleObscurePassword;
                        });
                      },
                      icon: toggleObscurePassword
                          ? Icon(
                              DenimIcon.section_login_password_hide,
                              color: Colors.black,
                            )
                          : Icon(
                              DenimIcon.section_login_password_show,
                              color: Colors.black,
                            ),
                    ),
                    obscureText: toggleObscurePassword,
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: AppTranslations.of(context).text('btn_сontinue_title'),
                    onPressed: () => navigateToChangePhoneScreen(),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  navigateToChangePhoneScreen() {
    var validatePassword = ValidatorUtils.matchValidation(
      _passwordController.text,
      'q1w2e3',
    );

    if (validatePassword) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => InitRegistrationStep(pageType: PageType.EDIT_PROFILE,)),
      );
    }
  }
}
