import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ProfileSettingsInformationPage extends StatefulWidget {
  @override
  _ProfileSettingsInformationPageState createState() =>
      _ProfileSettingsInformationPageState();
}

class _ProfileSettingsInformationPageState
    extends State<ProfileSettingsInformationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
            mainTitle:  AppTranslations.of(context).text('account_settings_information_title'),
            visibleActionTitle: false,
          ),
          // buildProfileSettingsItem(
          //   AppTranslations.of(context).text('account_information_about_title'),
          //   () {
          //     Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //         builder: (context) => WebViewInFlutter(
          //           appBarTitle: AppTranslations.of(context).text('account_information_about_title'),
          //           link: 'https://denimapp.ru/',
          //         ),
          //       ),
          //     );
          //   },
          // ),
          buildProfileSettingsItem(
            AppTranslations.of(context).text('account_information_terms_title'),
            () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WebViewInFlutter(
                    appBarTitle: AppTranslations.of(context).text('account_information_terms_title'),
                    link: 'https://keyou.one/terms-of-use',
                  ),
                ),
              );
            },
          ),
          buildProfileSettingsItem(
            AppTranslations.of(context).text('account_information_privacy_policy_title'),
            () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WebViewInFlutter(
                    appBarTitle: AppTranslations.of(context).text('account_information_privacy_policy_title'),
                    link: 'https://keyou.one/privacy-policy',
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget buildProfileSettingsItem(
    String title,
    GestureTapCallback onTap,
  ) {
    return ListTile(
      onTap: onTap,
      title: Text(
        title,
        style: DenimFonts.titleBody(context),
      ),
    );
  }
}

class WebViewInFlutter extends StatelessWidget {
  final String link;
  final String appBarTitle;

  WebViewInFlutter({
    this.link,
    this.appBarTitle,
  });

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: link,
      hidden: true,
      appBar: AppBar(title: Text(appBarTitle)),
    );
  }
}
