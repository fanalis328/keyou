import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/material.dart';

class MailingPage extends StatefulWidget {
  @override
  _MailingPageState createState() => _MailingPageState();
}

class _MailingPageState extends State<MailingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Visibility(
          visible: false,
          child: DefaultHeader(
            mainTitle: '${AppTranslations.of(context).text('soon')}...',
            visibleBackArrow: false,
            visibleActionTitle: false,
          ),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                '${AppTranslations.of(context).text('soon')}...',
                style: DenimFonts.titleXLarge(context),
              ),
              SizedBox(height: 20),
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: DenimColors.colorPrimary,
                    ),
                  ),
                  Positioned(
                    left: 17,
                    top: 22,
                    child: Icon(
                      DenimIcon.emails,
                      color: Colors.white,
                      size: 41,
                    ),
                  )
                ],
              ),
              SizedBox(height: 15),
              Text(
                AppTranslations.of(context).text('broadcast_main_title'),
                style: DenimFonts.titleXLarge(context),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: Text(
                  AppTranslations.of(context).text('broadcast_description'),
                  style: DenimFonts.titleBody(context),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ],
    ));
  }
}
