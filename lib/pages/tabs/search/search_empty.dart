import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/button.dart';
import 'package:flutter/material.dart';

class SearchEmptyState extends StatelessWidget {
  final GestureTapCallback callback;

  SearchEmptyState({this.callback});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          'assets/images/icons/section-search-empty.png',
          width: 60,
        ),
        SizedBox(height: 16),
        Text(
          AppTranslations.of(context).text('search_empty_state'),
          style: DenimFonts.titleBody(context),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 24),
        SizedBox(
          height: 36,
          child: DenimButton(
            onPressed: () => callback(),
            title: AppTranslations.of(context).text('change'),
          ),
        )
      ],
    );
  }
}
