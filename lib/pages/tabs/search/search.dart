import 'dart:async';
import 'dart:convert';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/purchase.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/model/websocket/search_filter.dart';
import 'package:keYou/pages/tabs/search/payment_states.dart/buy_online.dart';
import 'package:keYou/pages/tabs/search/search_empty.dart';
import 'package:keYou/pages/tabs/search/search_filter.dart';
import 'package:keYou/pages/tabs/search/search_header.dart';
import 'package:keYou/pages/tabs/search/search_item.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/purchase.dart';
import 'package:keYou/widgets/checkbox_button.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:keYou/widgets/up_to_top/up_to_top_dialog.dart';
import 'package:keYou/widgets/up_to_top/up_to_top_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../locator.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String token;
  bool onlinePurchaseState = false;
  List<ShortUserProfile> appSearchUsersList = App.me.searchUsersList;
  SearchFilters filters = SearchFilters();
  bool emptySearch = false;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  int position;
  bool _available = false;
  List<ProductDetails> products = [];
  List<PurchaseDetails> purchases = [];
  StreamSubscription _subscription;
  ProductDetails upToTopProduct;
  ProductDetails onlineProduct;

  getToken() async {
    token = await authDao.getToken();
  }

  @override
  void initState() {
    updateSearch();
    _initialize();
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  updatePosition() async {
    purchaseApi.getSearchPosition((data) {
      if (data['ctrl']['code'] == 200) {
        position = data['data']['pos'];
        if (!mounted) return;
        setState(() {});
      }
    });
  }

  _initialize() async {
    // TODO:  implement for web
    if (kIsWeb) return;

    _available = await purchaseUtils.iap.isAvailable();
    if (_available) {
      products = await purchaseUtils.getProducts([raiseProfile]);
      purchases = await purchaseUtils.getPastPurchases();
    }
    if (products.length > 0) {
      upToTopProduct =
          products.firstWhere((product) => product.id == raiseProfile);
      // onlineProduct =
      //     products.firstWhere((product) => product.id == onlineSearch);
    }

    _subscription =
        purchaseUtils.iap.purchaseUpdatedStream.listen((data) async {
      PurchaseDetails currentPurchase = data?.first;
      switch (currentPurchase.productID) {
        case onlineSearch:
          purchaseApi.buyOnlineSearch(
            currentPurchase.billingClientPurchase.purchaseToken,
            (purchaseResponse) {
              purchaseUtils.onlineSearchPurchaseCallback(purchaseResponse,
                  () async {
                purchaseUtils.successPurchseDialog(
                  context,
                  AppTranslations.of(context).text('product_online_success'),
                );
                await Future.delayed(Duration(seconds: 1));
                await updateSearch();
              });
            },
          );
          break;
        case raiseProfile:
          // set or update purchase to DB
          await purchaseUtils.checkAndUpdateCurrentDenimBlackPurchase(
            UpdatePurchaseData(
              owner: App.me.user.id,
              token: currentPurchase.billingClientPurchase.purchaseToken,
              product: currentPurchase.productID,
            ),
          );

          purchaseApi.raiseProfile(
            currentPurchase.billingClientPurchase.purchaseToken,
            (purchaseResponse) async {
              if (purchaseResponse != null &&
                      purchaseResponse['ctrl']['code'] == 200 ||
                  purchaseResponse != null &&
                      purchaseResponse['ctrl']['code'] == 202) {
                purchaseUtils.updateCurrentPurchase();
                purchaseUtils.successPurchseDialog(context,
                    AppTranslations.of(context).text('product_rase_success'));
                await Future.delayed(Duration(seconds: 1));
                updatePosition();
              }
            },
          );
          break;
        default:
          return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    getToken();
    return Scaffold(
      body: Column(
        children: <Widget>[
          SearchPageHeader(onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchPageFilter(),
              ),
            ).then((searchFilters) async {
              if (searchFilters == null) {
                return;
              } else {
                await updateSearch();
              }
            });
          }),
          Container(
            padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Row(
              children: <Widget>[
                Visibility(
                  visible: false,
                  child: CheckboxButton(
                    toggle: filters.newUsers,
                    title: AppTranslations.of(context).text('new'),
                    onPressed: () async {
                      filters.newUsers = !filters.newUsers;
                      await updateSearch();
                    },
                  ),
                ),
                CheckboxButton(
                  toggle: filters.online == null ? false : true,
                  title: AppTranslations.of(context).text('online'),
                  onPressed: () async {
                    filters.online = filters.online == null ? 1 : null;
                    await updateSearch();
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Visibility(
            visible: !onlinePurchaseState,
            child: Expanded(
              child: SmartRefresher(
                enablePullUp: true,
                enablePullDown: true,
                header: WaterDropHeader(
                  refresh: CircularProgressIndicator(),
                  completeDuration: Duration(seconds: 1),
                  waterDropColor: DenimColors.colorPrimary,
                ).complete,
                footer: CustomFooter(
                  loadStyle: LoadStyle.ShowWhenLoading,
                  builder: (BuildContext context, LoadStatus mode) {
                    Widget body;
                    body = CupertinoActivityIndicator();
                    return Container(
                      height: 55.0,
                      child: Center(child: body),
                    );
                  },
                ),
                onLoading: loadNextData,
                controller: _refreshController,
                onRefresh: updateSearch,
                child: emptySearch
                    ? SearchEmptyState(callback: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SearchPageFilter(),
                          ),
                        ).then((searchFilters) async {
                          if (searchFilters == null) {
                            return;
                          } else {
                            await updateSearch();
                          }
                        });
                      })
                    : CustomScrollView(
                        slivers: <Widget>[
                          SliverAppBar(
                            backgroundColor: Colors.white,
                            centerTitle: true,
                            titleSpacing: 0,
                            flexibleSpace: FlexibleSpaceBar(
                              centerTitle: true,
                              titlePadding: EdgeInsets.fromLTRB(16, 0, 16, 5),
                              title: ConstrainedBox(
                                constraints: BoxConstraints.expand(),
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          child: UpToTopSearch(
                                            position: position,
                                            onTap: () {
                                              if (App.me.user.profile.gender ==
                                                      2 &&
                                                  App.me.user.moderated != 1) {
                                                return showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return DefaultAlertDialog(
                                                      title: Text(
                                                        AppTranslations.of(
                                                          context,
                                                        ).text(
                                                          'profile_on_moderation_popup_2',
                                                        ),
                                                      ),
                                                      body: Container(
                                                        padding: EdgeInsets.symmetric(horizontal: 10),
                                                        child: Text(
                                                          AppTranslations.of(
                                                            context,
                                                          ).text(
                                                              'profile_on_moderation_raise_dialog'),
                                                        ),
                                                      ),
                                                      cancelBtnTitle:
                                                          AppTranslations.of(
                                                                  context)
                                                              .text('clear'),
                                                    );
                                                  },
                                                );
                                              } else {
                                                return showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return UpToTopDialog(
                                                      product: upToTopProduct,
                                                      available: _available,
                                                    );
                                                  },
                                                );
                                              }
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            pinned: false,
                            snap: false,
                            floating: true,
                            automaticallyImplyLeading: false,
                          ),
                          SliverPadding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            sliver: SliverGrid(
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                mainAxisSpacing: 20,
                                crossAxisSpacing: 12,
                                childAspectRatio: 0.75,
                                crossAxisCount: 2,
                              ),
                              delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                                  return SearchPageItem(
                                    shortUser: appSearchUsersList[index],
                                    token: token,
                                  );
                                },
                                childCount: appSearchUsersList.length,
                              ),
                            ),
                          ),
                        ],
                      ),
              ),
            ),
          ),
          Visibility(
            visible: onlinePurchaseState,
            child: Expanded(
              child: BuyOnline(
                product: onlineProduct,
                available: _available,
                updateSearch: updateSearch,
              ),
            ),
          )
        ],
      ),
    );
  }

  loadNextData() async {
    filters.offset = appSearchUsersList.length;
    searchApi.searchUsers(filters, getSearchUsersCallback);
  }

  Future<void> updateSearch() async {
    appSearchUsersList.clear();
    updatePosition();
    loadNextData();
  }

  getSearchUsersCallback(Map searchData) {
    if (searchData == null) return;

    switch (searchData['ctrl']['code']) {
      case 200:
        SearchUsersList searchUsersList = SearchUsersList.fromJson(
          json.encode(searchData['data']['users']),
        );
        updateSearhUsersView(searchUsersList.users);
        break;
      case 402:
        setState(() {
          onlinePurchaseState = true;
        });
        break;
      default:
        return showDialog(
          context: context,
          builder: (context) {
            return DefaultAlertDialog(
              title: Text(
                AppTranslations.of(context).text('blocking_error'),
                style: DenimFonts.titleDefault(context),
              ),
              body: Text(
                AppTranslations.of(context).text('blocking_error_connection'),
                style: DenimFonts.titleBody(context),
                textAlign: TextAlign.left,
              ),
              cancelBtnTitle: AppTranslations.of(context).text('clear'),
            );
          },
        );
    }

    _refreshController.loadComplete();
    _refreshController.refreshCompleted();
  }

  updateSearhUsersView(List<ShortUserProfile> usersData) async {
    if (!mounted) return [];
    List<ShortUserProfile> output = usersData
        .where((element) => !appSearchUsersList.contains(element))
        .toList();

    if (appSearchUsersList.length == 0) {
      if (!mounted) return [];
      emptySearch = true;
      onlinePurchaseState = false;
    }

    if (output.length > 0) {
      if (!mounted) return [];
      appSearchUsersList.addAll(output);

      final seen = Set<ShortUserProfile>();
      final unique = appSearchUsersList.where((str) => seen.add(str)).toList();
      appSearchUsersList.clear();
      appSearchUsersList.addAll(unique);
      emptySearch = false;
      onlinePurchaseState = false;
    }

    await Future.delayed(Duration(seconds: 1));
    if (!mounted) return;
    setState(() {});
    return appSearchUsersList;
  }
}
