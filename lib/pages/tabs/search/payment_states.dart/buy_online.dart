import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/pages/denim_black/denim_black.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:keYou/widgets/payment_radio_button_group.dart';
import 'package:flutter/material.dart';
import 'package:keYou/locator.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

class BuyOnline extends StatefulWidget {
  final ProductDetails product;
  final bool available;
  final Function updateSearch;

  BuyOnline({
    this.product,
    this.available = false,
    this.updateSearch,
  });

  @override
  _BuyOnlineState createState() => _BuyOnlineState();
}

class _BuyOnlineState extends State<BuyOnline> {
  List<RadioButton> _paymentRadioList = [];

  @override
  void initState() {
    addOnlineProduct();
    super.initState();
  }

  addOnlineProduct() {
    _paymentRadioList.clear();
    _paymentRadioList?.add(
      RadioButton(
        title: '1 час',
        price: widget.product?.price == null ? 'null' : widget.product?.price,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Container(
                    width: 144,
                    height: 144,
                    decoration: BoxDecoration(
                      color: DenimColors.colorSpecialOnlineDefault,
                      borderRadius: BorderRadius.circular(100),
                    ),
                  ),
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          AppTranslations.of(context)
                              .text('search_who_online_option_title'),
                          style: DenimFonts.titleXLarge(context),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          AppTranslations.of(context)
                              .text('search_who_online_option_description_1'),
                          style: DenimFonts.titleBody(context),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          AppTranslations.of(context)
                              .text('search_who_online_option_description_2'),
                          style: DenimFonts.titleBody(context),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Container(
              //   height: 60,
              //   child: PaymentRadioButtonGroup(
              //       paymentRadioList: _paymentRadioList),
              // ),
              // SizedBox(height: 16),
              Row(
                children: <Widget>[
                  Expanded(
                    child: DenimButton(
                      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DenimBlack())).then((val) {
                        return widget.updateSearch();
                      }),
                      title: AppTranslations.of(context)
                          .text('get_full_membership'),
                    ),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  buyOnline() {
    if (widget.product == null || !widget.available) {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(AppTranslations.of(context).text('blocking_error')),
            body: Text(
              'Сервис оплаты недоступен',
              style: DenimFonts.titleBody(context),
              textAlign: TextAlign.left,
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
          );
        },
      );
    } else {
      return purchaseUtils.buyProduct(widget.product);
    }
  }
}
