import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:flutter/material.dart';

class ExtendOnline extends StatefulWidget {
  @override
  _ExtendOnlineState createState() => _ExtendOnlineState();
}

class _ExtendOnlineState extends State<ExtendOnline> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(16, 7, 16, 7),
      color: DenimColors.colorBlackCell,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  AppTranslations.of(context).text('time_left'),
                  style: DenimFonts.titleCaption(
                    context,
                    DenimColors.colorBlackSecondary,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  '30 минут',
                  style: DenimFonts.titleBody(context),
                ),
              ],
            ),
          ),
          Container(
            width: 117,
            child: BorderListItem(
              onTap: () {},
              title: Text(
                AppTranslations.of(context).text('extend'),
                style: DenimFonts.titleBtnSmallSecondary(context),
              ),
              height: 36,
            ),
          )
        ],
      ),
    );
  }
}
