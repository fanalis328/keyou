import 'dart:convert';
import 'package:keYou/data/app.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/views.dart';
import 'package:keYou/pages/tabs/chat/chat_dialog.dart';
import 'package:keYou/pages/tabs/search/search_full_profile.dart';
import 'package:keYou/pages/tabs/views/views_item.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/utils/verification.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ViewsPage extends StatefulWidget {
  @override
  _ViewsPageState createState() => _ViewsPageState();
}

class _ViewsPageState extends State<ViewsPage> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  List<ShortViewUserProfile> _views = [];
  String token;

  Widget emptyViews() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          DenimIcon.section_tabbar_tab4,
          size: 64,
          color: DenimColors.colorBlackTertiary,
        ),
        SizedBox(height: 10),
        Text(
          AppTranslations.of(context).text('views_description_1'),
          style: DenimFonts.titleXLarge(
            context,
            DenimColors.colorBlackTertiary,
          ),
        ),
        Text(
          AppTranslations.of(context).text('views_description_2'),
          style: DenimFonts.titleXLarge(
            context,
            DenimColors.colorBlackTertiary,
          ),
        ),
      ],
    );
  }

  updateViews() {
    _views.clear();
    int offset = _views.length;
    viewsApi.getViews(offset, (Map data) {
      if (data != null &&
          data['ctrl']['code'] == 200 &&
          data['data']['users'].length > 0) {
        getViews(data['data']);
      }
    });
    if (!mounted) return;
    setState(() {});
    _refreshController.loadComplete();
    _refreshController.refreshCompleted();
  }

  loadMore() async {
    int offset = _views.length;
    await Future.delayed(Duration(milliseconds: 1000));
    viewsApi.getViews(offset, (Map data) {
      if (data != null && data['ctrl']['code'] == 200) {
        getViews(data['data']);
      }
    });
    if (!mounted) return;
    setState(() {});
    _refreshController.loadComplete();
    _refreshController.refreshCompleted();
  }

  getViews(Map usersData) {
    if (usersData['users'].length > 0) {
      ViewsUsersList viewUsersList = ViewsUsersList.fromJson(
        json.encode(usersData['users']),
      );
      updateViewsList(viewUsersList.users);
    } else {
      return;
    }
  }

  updateViewsList(List<ShortViewUserProfile> usersData) {
    if (!mounted) return [];
    usersData.sort(
      (a, b) => ChatUtils.getCleanMessageTsForDialogItem(
        b == null ? '1990-10-10 10:10' : b.time,
      ).compareTo(
        ChatUtils.getCleanMessageTsForDialogItem(
          a == null ? '1990-10-10 10:10' : a.time,
        ),
      ),
    );
    setState(() {
      _views.addAll(usersData);
    });
    return _views;
  }

  @override
  void initState() {
    App.me.addTabIndicator(App.me.newViewsController, false);
    getToken();
    updateViews();
    firebaseAnalytics.sendAnalyticsEvent('page_views');
    facebookAnalytics
        .sendFacebookAnalyticsEvent(facebookAnalytics.eventViewedContent);
    super.initState();
  }

  getToken() async {
    token = await authDao.getToken();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleBackArrow: false,
            visibleActionTitle: false,
            mainTitle: AppTranslations.of(context).text('views_main_title'),
          ),
          Expanded(
            child: _views?.length == 0
                ? emptyViews()
                : SmartRefresher(
                    enablePullUp: true,
                    enablePullDown: true,
                    header: WaterDropHeader(
                      refresh: CircularProgressIndicator(),
                      completeDuration: Duration(seconds: 1),
                      waterDropColor: DenimColors.colorPrimary,
                    ).complete,
                    footer: CustomFooter(
                      loadStyle: LoadStyle.ShowWhenLoading,
                      builder: (BuildContext context, LoadStatus mode) {
                        Widget body;
                        body = CupertinoActivityIndicator();
                        return Container(
                          height: 55.0,
                          child: Center(child: body),
                        );
                      },
                    ),
                    onLoading: loadMore,
                    controller: _refreshController,
                    onRefresh: updateViews,
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      itemCount: _views.length,
                      itemBuilder: (context, index) {
                        return ViewsItem(
                          onTapCallback: () =>
                              VerificationUtils.deleteOrBlockedUserTap(
                                  context,
                                  _views[index].deleted,
                                  _views[index].blocked, () {
                            firebaseAnalytics
                                .sendAnalyticsEvent('view_to_page');
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SearchFullProfileScreen(
                                    userId: _views[index].id),
                              ),
                            );
                          }),
                          name: _views[index].name,
                          age: _views[index].age,
                          city: _views[index].city,
                          country: _views[index].country,
                          time: _views[index].time,
                          token: token,
                          avatar: _views[index].avatar,
                          startChatCallback: () => goToChat(
                              _views[index].id,
                              context,
                              _views[index].deleted,
                              _views[index].blocked),
                          gender: _views[index].gender,
                        );
                      },
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  goToChat(String id, context, bool deleted, bool blocked) {
    VerificationUtils.deleteOrBlockedUserTap(context, deleted, blocked, () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatDialogPage(
            topic: id,
          ),
        ),
      );
      firebaseAnalytics.sendAnalyticsEvent('view_to_dialog');
    });
  }
}
