import 'dart:async';
import 'dart:convert';
import 'package:keYou/data/app.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/model/websocket/chat.dart';
import 'package:keYou/pages/tabs/chat/chat_dialog.dart';
import 'package:keYou/pages/tabs/chat/chat_items/chat_dialog_item.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController _messageController = TextEditingController();
  String token;
  ProfileData user = App.me.user;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  List<ChatDialogData> _chatsFromDbList = [];
  List<ChatDialogData> _chatsFromDbListForDisplay = [];
  // FocusNode focusNode;

  @override
  void initState() {
    super.initState();
    App.me.addTabIndicator(App.me.unreadMessageController, false);
    getToken();
    getChats();
    socket.addListener(notifyListener);
  }

  Future<void> updateChatOnNotification() async {
    await Future.delayed(Duration(milliseconds: 500));
    await getChats();
  } 

  // _dismissKeyboard() {
  //   FocusScope.of(context).requestFocus(FocusNode());
  // }

  notifyListener(data) async {
    Map message = json.decode(data);
    if (message != null && message['notify'] != null) {
      switch (message['notify']['action']) {
        case 'typing':
          typingMessage(message);
          break;
        case 'newmsg':
          updateChatOnNotification();
          break;
        default:
          return;
      }
    } else if (message != null &&
        message['chat'] != null &&
        message['chat']['content'] != null) {
      updateChatOnNotification();
    } else {
      return;
    }
  }

  typingMessage(Map message) async {
    String userTypingId = message['notify']['topic'];
    ChatDialogData chat = _chatsFromDbListForDisplay
        ?.firstWhere((item) => item.topic == userTypingId);

    if (chat != null) {
      if (!mounted) return;
      setState(() {
        chat.typing = true;
        chat.public.online = 1;
      });

      if (!mounted) return;
      Timer(Duration(seconds: 2), () async {
        setState(() {
          chat.typing = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // focusNode.unfocus();
        // FocusScopeNode currentFocus = FocusScopeNode();
        // if (!currentFocus.hasPrimaryFocus) {
        //   currentFocus.unfocus();
        // }
      },
      child: Scaffold(
        body: Column(
          children: <Widget>[
            DefaultHeader(
              visibleBackArrow: false,
              mainTitle: AppTranslations.of(context).text('chat_main_title'),
              visibleActionTitle: false,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
              height: 48,
              child: TextField(
                textCapitalization: TextCapitalization.sentences,
                // focusNode: focusNode,
                autofocus: false,
                controller: _messageController,
                onChanged: (val) {
                  searchDialog(val);
                },
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: DenimColors.colorBlackLine,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: DenimColors.colorBlackLine,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: DenimColors.colorBlackLine,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 2,
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  prefixIcon: Icon(
                    DenimIcon.universal_searchbar_search,
                    size: 16,
                    color: DenimColors.colorBlackSecondary,
                  ),
                  hintText: AppTranslations.of(context).text('search'),
                ),
              ),
            ),
            Expanded(
              child: _chatsFromDbListForDisplay != null &&
                      _chatsFromDbListForDisplay?.length == 0
                  ? emptyChats()
                  : SmartRefresher(
                      enablePullUp: false,
                      header: WaterDropHeader(
                        refresh: CircularProgressIndicator(),
                        completeDuration: Duration(seconds: 1),
                        waterDropColor: DenimColors.colorPrimary,
                      ).complete,
                      controller: _refreshController,
                      onRefresh: updateDialogs,
                      child: ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemCount: _chatsFromDbListForDisplay.length,
                        itemBuilder: (context, index) {
                          return ChatDialogItem(
                              gender:
                                  _chatsFromDbListForDisplay[index]
                                      .public
                                      .gender,
                              typing: _chatsFromDbListForDisplay[index]?.typing,
                              messageCounter: _chatsFromDbListForDisplay[index]
                                              ?.seq !=
                                          null &&
                                      _chatsFromDbListForDisplay[index].read !=
                                          null &&
                                      _chatsFromDbListForDisplay[index].seq >
                                          _chatsFromDbListForDisplay[index].read
                                  ? _chatsFromDbListForDisplay[index].seq -
                                      _chatsFromDbListForDisplay[index].read
                                  : null,
                              onTapCallback: () => goToChatPage(
                                  _chatsFromDbListForDisplay[index]?.topic),
                              name: _chatsFromDbListForDisplay[index]
                                  ?.public
                                  ?.name,
                              age: _chatsFromDbListForDisplay[index]
                                  ?.public
                                  ?.age,
                              avatar: _chatsFromDbListForDisplay[index]
                                  ?.public
                                  ?.avatar,
                              token: token,
                              isOnline: _chatsFromDbListForDisplay[index]
                                  ?.public
                                  ?.online,
                              lastMessage: _chatsFromDbListForDisplay[index]
                                  ?.lastMessage);
                        },
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget emptyChats() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            DenimIcon.section_tabbar_tab3,
            size: 64,
            color: DenimColors.colorBlackTertiary,
          ),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 45),
            child: Text(
              AppTranslations.of(context).text('chat_empty_dialogs_title'),
              textAlign: TextAlign.center,
              style: DenimFonts.titleXLarge(
                context,
                DenimColors.colorBlackTertiary,
              ),
            ),
          ),
        ],
      ),
    );
  }

  deleteChatDialog(String topic) {
    showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          title: Text(
            'Удалить чат?',
            style: DenimFonts.titleBody(context),
          ),
          actionButton: GestureDetector(
            onTap: () async {
              await chatDao.deleteChatDialog(topic);
              setState(() {});
              Navigator.pop(context);
            },
            child: Text(
              AppTranslations.of(context).text('delete'),
              style: DenimFonts.titleLink(
                context,
                DenimColors.colorPrimary,
              ),
            ),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('btn_сancel_title'),
        );
      },
    );
  }

  goToChatPage(String topic) async {
    // _dismissKeyboard();
    chatApi.subscribeChat(topic, (Map data) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatDialogPage(
            topic: topic,
          ),
        ),
      ).then((val) async {
        await ChatUtils.updateDialogs();
        socket.addListener(notifyListener);
        await Future.delayed(Duration(milliseconds: 500));
        await getChats();
      });
    });
  }

  getToken() async {
    token = await authDao.getToken();
    return token;
  }

  Future<List<ChatDialogData>> getChats() async {
    List<ChatDialogData> chats = await chatDao.getAllChatsSortedById();
    List<ChatDialogData> myChats = chats.length == 0 ? [] : getSortedDialogs(chats);
    _chatsFromDbList = myChats;
    _chatsFromDbListForDisplay = myChats;
    if (mounted) {
      setState(() {});
    }
    return _chatsFromDbListForDisplay;
  }

  Future<List<ChatDialogData>> searchDialog([String text = '']) async {
    if (text.length == 0) {
      setState(() {
        getSortedDialogs(_chatsFromDbList);
        _chatsFromDbListForDisplay = _chatsFromDbList;
      });
    }

    setState(() {
      _chatsFromDbListForDisplay = _chatsFromDbList.where((item) {
        var userName = item.public.name.toLowerCase();
        return userName.contains(text.toLowerCase());
      }).toList();
    });
    return _chatsFromDbListForDisplay;
  }

  getSortedDialogs(List<ChatDialogData> list) {
    List<ChatDialogData> myChats = list
        .where((item) => item.owner == user.id && item.lastMessage != null)
        .toList();
    if (myChats.length > 0) {
      myChats.sort(
        (a, b) => DateTime.parse(
                b.lastMessage == null ? '1990-10-10 10:10' : b.lastMessage.ts)
            .compareTo(
          DateTime.parse(
              a.lastMessage == null ? '1990-10-10 10:10' : a.lastMessage.ts),
        ),
      );
    } else {
      myChats = [];
    }
    return myChats;
  }

  Future updateDialogs() async {
    await ChatUtils.updateDialogs();
    if (!mounted) return;
    await Future.delayed(Duration(milliseconds: 400));
    await getChats();
    _refreshController.loadComplete();
    _refreshController.refreshCompleted();
  }

  @override
  void dispose() {
    super.dispose();
    _chatsFromDbList.clear();
    _chatsFromDbListForDisplay.clear();
    socket.removeListener(notifyListener);
  }
}
