import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/model/websocket/chat.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/utils/image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ChatDialogItem extends StatefulWidget {
  final GestureTapCallback longPressCallback;
  final GestureTapCallback onTapCallback;
  final String name;
  final int age;
  final UserPhoto avatar;
  final String token;
  final int isOnline;
  final LastMessage lastMessage;
  final int messageCounter;
  final bool typing;
  final int gender;

  ChatDialogItem({
    this.longPressCallback,
    this.onTapCallback,
    this.name,
    this.age,
    this.avatar,
    this.token,
    this.isOnline,
    this.lastMessage,
    this.messageCounter,
    this.typing = false,
    this.gender,
  });

  @override
  _ChatDialogItemState createState() => _ChatDialogItemState();
}

class _ChatDialogItemState extends State<ChatDialogItem> {
  bool prints = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTapCallback,
      onLongPress: widget.longPressCallback,
      child: Padding(
        padding: EdgeInsets.fromLTRB(12, 10, 12, 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 56,
              height: 56,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: AspectRatio(
                      aspectRatio: 1 / 1,
                      child: Container(
                        color: Colors.black,
                        child: ImageUtils.imageWidget(
                          widget.avatar,
                          widget.gender,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Visibility(
                      visible: widget.isOnline == 1 ? true : false,
                      child: Container(
                        width: 14,
                        height: 14,
                        decoration: BoxDecoration(
                          color: DenimColors.colorSpecialOnlineDefault,
                          borderRadius: BorderRadius.circular(100),
                          border: Border.all(
                            color: Colors.white,
                            width: 2,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(width: 12),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        '${widget.name}, ${widget.age}',
                        overflow: TextOverflow.ellipsis,
                        style: DenimFonts.titleBody(context),
                      ),
                      Text(
                        ChatUtils.getCleanMessageTsForDialogItem(widget.lastMessage.ts),
                        overflow: TextOverflow.ellipsis,
                        style: DenimFonts.titleCaption(
                          context,
                          DenimColors.colorBlackSecondary,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 36,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: widget.typing
                                ? Text(
                                    AppTranslations.of(context).text('prints'),
                                    style: DenimFonts.titleFootnote(
                                      context,
                                      DenimColors.colorBlackSecondary,
                                    ),
                                  )
                                : Row(
                                    children: <Widget>[
                                      Visibility(
                                        visible: widget.lastMessage.type == 0 || widget.lastMessage.type == null
                                            ? false
                                            : true,
                                        child: Icon(Icons.photo, size: 20),
                                      ),
                                      Expanded(
                                        child: Text(
                                          widget.lastMessage.type == 0 || widget.lastMessage.type == null
                                              ? widget.lastMessage.content
                                              : AppTranslations.of(context).text('picture'),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          style: DenimFonts.titleFootnote(
                                            context,
                                            DenimColors.colorBlackSecondary,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                          ),
                        ),
                        Visibility(
                          visible: widget.messageCounter == 0 ||
                                  widget.messageCounter == null
                              ? false
                              : true,
                          child: Container(
                            height: 24,
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            decoration: BoxDecoration(
                              color: DenimColors.colorPrimary,
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: Center(
                              child: Text(
                                widget.messageCounter.toString(),
                                style: DenimFonts.titleFootnote(
                                  context,
                                  Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: false,
                          child: Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                              color: DenimColors.colorPrimaryShade,
                              borderRadius: BorderRadius.circular(100),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
