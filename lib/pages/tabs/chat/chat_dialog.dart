import 'dart:async';
import 'dart:convert';
import 'package:keYou/config/config.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/model/websocket/chat.dart';
import 'package:keYou/pages/tabs/chat/chat_items/chat_message_item.dart';
import 'package:keYou/pages/tabs/search/search_full_profile.dart';
import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/utils/image.dart';
import 'package:keYou/utils/verification.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:throttling/throttling.dart';
import 'package:transparent_image/transparent_image.dart';

class ChatDialogPage extends StatefulWidget {
  final String topic;
  ChatDialogPage({this.topic});
  @override
  _ChatDialogPageState createState() => _ChatDialogPageState();
}

class _ChatDialogPageState extends State<ChatDialogPage> {
  TextEditingController _messageController = TextEditingController();
  String userId;
  ChatDialogData chatDialogData;
  List<ChatMessageItem> messageItems = [];
  ChatDialogData dialog;
  static int startSince;
  int defaultPrevSeq;
  int defaultBefor;
  int readMessages;
  bool typing = false;
  LastMessage lastMessage;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  Throttling eventTypingMessageTrotl =
      Throttling(duration: Duration(seconds: 2));
  int read;
  bool disable = true;
  ProfileData user = App.me.user;
  List<String> messageImagesLinks = [];

  @override
  void initState() {
    socket.addListener(addNewMessage);
    showLastChatMessages();
    delayed();
    super.initState();
  }

  @override
  void dispose() {
    socket.removeListener(addNewMessage);
    socket.addListener(App.me.notificationListener);
    messageItems.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    socket.removeListener(App.me.notificationListener);
    return WillPopScope(
      onWillPop: () => leaveChat(),
      child: Scaffold(
        body: Column(
          children: <Widget>[
            SafeArea(
              child: FutureBuilder(
                future: chatDialog(),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return SizedBox(height: 56);
                  } else {
                    return Container(
                      height: 56,
                      child: Row(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(
                              DenimIcon.universal_back,
                              size: 24,
                            ),
                            onPressed: () => leaveChat(),
                          ),
                          SizedBox(width: 14),
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: DenimColors.colorPrimary,
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: GestureDetector(
                                onTap: () =>
                                    VerificationUtils.deleteOrBlockedUserTap(
                                        context,
                                        snapshot.data.public.blocked,
                                        snapshot.data.public.deleted, () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          SearchFullProfileScreen(
                                              userId: dialog.topic),
                                    ),
                                  );
                                }),
                                child: ImageUtils.imageWidget(
                                    snapshot?.data?.public?.avatar,
                                    snapshot?.data?.public?.gender),
                              ),
                            ),
                          ),
                          SizedBox(width: 12),
                          Expanded(
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SearchFullProfileScreen(
                                      userId: dialog.topic),
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${snapshot?.data?.public?.name}, ${snapshot?.data?.public?.age}',
                                    style: DenimFonts.titleSmall(context),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    snapshot.data.public.online == 1
                                        ? AppTranslations.of(context)
                                            .text('online')
                                        : AppTranslations.of(context)
                                            .text('offline'),
                                    style: DenimFonts.titleFootnote(context,
                                        DenimColors.colorBlackSecondary),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // PopupMenuButton<String>(
                          //   onSelected: choiceAction,
                          //   itemBuilder: (BuildContext context) {
                          //     return DialogOptionConstants.options
                          //         .map((String option) {
                          //       return PopupMenuItem<String>(
                          //         value: option,
                          //         child: Text(option),
                          //       );
                          //     }).toList();
                          //   },
                          // ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
            Expanded(
              child: messageItems.length > 0
                  ? SmartRefresher(
                      enablePullDown: false,
                      enablePullUp: true,
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus mode) {
                          Widget body;
                          if (mode == LoadStatus.loading) {
                            body = CupertinoActivityIndicator();
                          }
                          return Container(
                            height: 70.0,
                            child: Center(child: body),
                          );
                        },
                      ),
                      controller: _refreshController,
                      onRefresh: loadMoreMessages,
                      onLoading: loadMoreMessages,
                      child: ListView.builder(
                        reverse: true,
                        padding: EdgeInsets.all(0),
                        itemCount: messageItems.length,
                        itemBuilder: (context, index) {
                          return messageItems[index];
                        },
                      ),
                    )
                  : emptyChat(),
            ),
            Container(
              padding: EdgeInsets.all(12),
              child: Stack(
                children: <Widget>[
                  TextField(
                    textCapitalization: TextCapitalization.sentences,
                    controller: _messageController,
                    maxLines: 5,
                    minLines: 1,
                    onChanged: (val) {
                      if (val.length > 0) {
                        setState(() {
                          disable = false;
                        });
                      } else {
                        setState(() {
                          disable = true;
                        });
                      }
                      eventTypingMessageTrotl.throttle(() {
                        chatApi.typingMessage(widget.topic);
                      });
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(48, 0, 48, 0),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: DenimColors.colorBlackLine,
                        ),
                        borderRadius: BorderRadius.circular(24),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: DenimColors.colorBlackLine,
                        ),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      hintText: AppTranslations.of(context)
                          .text('chat_dialog_input_hint'),
                    ),
                  ),
                  Positioned(
                    bottom: -1,
                    child: IconButton(
                      icon: Icon(Icons.photo_camera),
                      onPressed: () => getMessageImage(),
                    ),
                  ),
                  Positioned(
                    bottom: 6,
                    right: 5,
                    child: Container(
                      width: 36,
                      height: 36,
                      child: RawMaterialButton(
                        onPressed: () {
                          if (disable) return;
                          sendMessage();
                        },
                        fillColor: _messageController.text.length == 0
                            ? DenimColors.colorBlackSecondary
                            : DenimColors.colorPrimary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: Center(
                          child: Icon(
                            Icons.arrow_upward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  delayed() async {
    await Future.delayed(Duration(milliseconds: 460));
    setState(() {});
    return dialog;
  }

  addNewMessage(data) {
    Map message = json.decode(data);
    if (message['chat'] == null ||
        message['chat']['from'] != widget.topic &&
            message['chat']['from'] != App.me.user.id) return;

    ChatMessage newMessage = ChatMessage.fromJson(message['chat']);
    if (newMessage.head != null) {
      changeMessageState(newMessage);
    } else {
      addNewMessageToList(newMessage, false);
    }

    if (!mounted) return;
    dialog.seq = newMessage.seq;
    if (newMessage.from != widget.topic) {
      chatApi.readMessage(widget.topic, newMessage.seq);
      chatApi.receivedMessage(widget.topic, newMessage.seq);
    }

    updateChatSeqAndlastMessageInDb(messageItems?.first);
  }

  showLastChatMessages() async {
    ChatDialogData chat = await getChatDialogFromDb();
    chatApi.subscribeChat(widget.topic, (Map subscribedData) async {
      if (subscribedData == null || subscribedData['ctrl']['code'] != 200 && subscribedData['ctrl']['code'] != 304) {
        return showDialog(
          context: context,
          builder: (context) {
            return DefaultAlertDialog(
              title: Text(
                'Вы не можете написать этому пользователю.',
                style: DenimFonts.titleBody(context),
              ),
              cancelBtnTitle: AppTranslations.of(context).text('back'),
              cancelBtnCallBack: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
            );
          },
        );
      }
      if (chat != null && chat.owner == user.id) {
        lastMessage = chat.lastMessage;
      }

      chatApi.getChatInfo(widget.topic, (Map chatInfo) async {
        if (chatInfo != null &&
            chatInfo['ctrl'] != null &&
            chatInfo['data'] != null &&
            chatInfo['ctrl']['code'] == 200) {
          ChatDialogData currentDialogData =
              ChatDialogData.fromJson(chatInfo['data']);
          currentDialogData.lastMessage = lastMessage;
          currentDialogData.read = currentDialogData.seq;
          await chatDao.updateChatDialog(currentDialogData);

          startSince =
              currentDialogData.seq != null ? currentDialogData.seq - 50 : 0;
          defaultBefor = currentDialogData.seq;

          dialog = currentDialogData;
          if (currentDialogData.seq == 0 || currentDialogData.seq == null)
            return;
          getMessages(defaultBefor, startSince);
          chatApi.readMessage(widget.topic, currentDialogData.seq);
        }
      });
    });
    return;
  }

  getMessages([int before, int sinse]) {
    chatApi.getMessages(
        widget.topic, before < 0 ? 0 : before, sinse < 0 ? 0 : sinse,
        (Map data) {
      if (data['ctrl']['code'] == 200) {
        MessagesList messagesList = MessagesList.fromJson(data['data']);
        messagesList.messages
            .map((remoteMessage) => addNewMessageToList(remoteMessage, true))
            ?.toList();
        if (!mounted) return;
        setState(() {});
        _refreshController.loadComplete();
      }
    });
  }

  loadMoreMessages() {
    startSince = startSince - 10;
    defaultBefor = startSince + 9;
    getMessages(defaultBefor, startSince);
  }

  addNewMessageToList(ChatMessage message, bool oldMessage,
      [bool loading = false]) {
    switch (oldMessage) {
      case true:
        messageItems.add(ChatMessageItem(
          message: ChatMessageItemWidget(
              seq: message.seq,
              type: message.head == null || message.head.attachments.isEmpty
                  ? 0
                  : 1,
              read: false,
              incoming: message.from == widget.topic ? true : false,
              content: message.content == '' ? '' : message.content,
              ts: message.ts,
              topic: message.topic,
              from: message.from,
              imageUrl: message?.head == null
                  ? ''
                  : message.head.attachments.first.toString(),
              loading: loading,
              removeLoadingPhoto: removeLoadingPhoto),
        ));
        break;
      case false:
        messageItems.insert(
            0,
            ChatMessageItem(
              message: ChatMessageItemWidget(
                  seq: message.seq,
                  type: message.head == null || message.head.attachments.isEmpty
                      ? 0
                      : 1,
                  read: false,
                  incoming: message.from == widget.topic ? true : false,
                  content: message.content == '' ? '' : message.content,
                  ts: message.ts,
                  topic: message.topic,
                  from: message.from,
                  imageUrl: message?.head == null
                      ? ''
                      : message.head.attachments.first.toString(),
                  loading: loading,
                  removeLoadingPhoto: removeLoadingPhoto),
            ));
        break;
      default:
        return;
    }
  }

  updateChatSeqAndlastMessageInDb(ChatMessageItem messageItem) async {
    ChatDialogData chat = await getChatDialogFromDb();
    if (messageItem != null) {
      if (!mounted) return;
      setState(() {
        dialog.seq = messageItem.message.seq;
        dialog.read = messageItem.message.seq;
        dialog.lastMessage = LastMessage(
            from: messageItem.message.from,
            seq: messageItem.message.seq,
            content: messageItem.message.type == 0
                ? messageItem.message.content
                : 'Изображение',
            ts: messageItem.message.ts,
            type: messageItem.message.type);
      });

      if (chat == null || chat.owner != user.id) {
        await chatDao.insertChatDialog(dialog);
      } else {
        await chatDao.updateChatDialog(dialog);
      }
    }
  }

  sendMessage() async {
    ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
    if (currentUser.profile.gender == 2 && currentUser.moderated != 1 ||
        currentUser.profile.gender == 2 &&
            currentUser.profile.avatar.moderated != 1 ||
        currentUser.profile.gender == 1 && currentUser.subscription == null) {
      await VerificationUtils.verificationListener(context, () {
        sendMessageAfterChecks();
      });
    } else {
      sendMessageAfterChecks();
    }
  }

  sendMessageAfterChecks() {
    if (_messageController.text != '') messageImagesLinks.clear();
    if (_messageController.text != '' && widget.topic != null ||
        messageImagesLinks.length > 0) {
      chatApi.sendMessage(
          widget.topic, _messageController.text, messageImagesLinks,
          (Map data) {
        if (data != null) {
          checkMessageResponseCases(data['ctrl']['code']);
        }
      });
    }
  }

  checkMessageResponseCases(int code) async {
    switch (code) {
      case 200:
      case 202:
        setState(() {
          _messageController = TextEditingController(text: '');
        });
        if (user?.profile?.gender == 1) {
          firebaseAnalytics.sendAnalyticsEvent('message_M');
        } else {
          firebaseAnalytics.sendAnalyticsEvent('message_W');
        }
        break;
      case 402:
        setState(() {
          _messageController = TextEditingController(text: '');
        });
        if (messageItems[0].message.type == 1) {
          removeLoadingPhoto();
        }
        await VerificationUtils.verificationListener(context);
        break;
      default:
        return;
    }
  }

  Future<void> getMessageImage() async {
    ImageFile messageImage = await ImageUtils.getPhotoFromGallery();
    if (messageImage == null) {
      await Future.delayed(Duration(seconds: 1));
      chatApi.subscribeChat(widget.topic);
      return;
    }

    addNewMessageToList(
        ChatMessage(
            seq: messageItems.isEmpty ? 1 : messageItems[0].message.seq + 1,
            ts: DateTime.now().toIso8601String(),
            head: MessageItemHead(attachments: [messageImage.path])),
        false,
        true);
    if (!mounted) return;
    setState(() {});

    String imageId = await userApi.uploadImage(messageImage);
    String imagePath = appConfig.downloadImageEndpoint + imageId + '.jpeg';

    messageImagesLinks = [imagePath];
    chatApi.subscribeChat(widget.topic, (Map data) {
      if (data != null && data['ctrl']['code'] == 200 ||
          data['ctrl']['code'] == 304) {
        sendMessage();
      }
    });
  }

  changeMessageState(ChatMessage message) {
    messageItems.removeWhere((item) => item.message.seq == message.seq);
    addNewMessageToList(message, false);
    if (!mounted) return;
    setState(() {});
    return messageItems;
  }

  Future<ChatDialogData> chatDialog() async {
    ChatDialogData dialogFromDb = await getChatDialogFromDb();
    if (dialogFromDb == null) {
    } else {
      dialog = dialogFromDb;
      dialog.owner = App.me.user.id;
    }
    return dialog;
  }

  getUserId() async {
    userId = await authDao.getCurrentUserId();
    if (userId == null) {
      userId = App.me.user.id;
    }
    return userId;
  }

  Future<ChatDialogData> getChatDialogFromDb() async {
    ChatDialogData chat = await chatDao.getChatDialog(widget.topic);
    return chat;
  }

  leaveChat() async {
    Navigator.pop(context);
  }

  removeLoadingPhoto() {
    messageItems.removeAt(0);
    setState(() {});
    return;
  }

  Center emptyChat() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FadeInImage(
            placeholder: MemoryImage(kTransparentImage),
            image: AssetImage(
                'assets/images/icons/section-messages-chat-secure.jpg'),
            fit: BoxFit.cover,
            width: 64,
          ),
          SizedBox(height: 16),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50),
            child: Text(
              AppTranslations.of(context).text('chat_empty_dialog_description'),
              style:
                  DenimFonts.titleBody(context, DenimColors.colorBlackTertiary),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  void choiceAction(String options) {
    switch (options) {
      case DialogOptionConstants.Delete:
        print('DELETE DIALOG');
        break;
      default:
        print('dialog action');
    }
  }
}

class DialogOptionConstants {
  static const String Delete = 'Удалить';
  static const List<String> options = <String>[Delete];
}
