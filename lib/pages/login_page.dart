import 'package:keYou/blocs/login_bloc/bloc.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/login_page_sms.dart';
import 'package:keYou/pages/registration_steps/country_phone_codes.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/utils/validators.dart';
import 'package:keYou/utils/connections.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/fake_form_select.dart';
import 'package:keYou/widgets/input.dart';
import 'package:flutter/material.dart';

import '../locator.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _phoneNumberController = TextEditingController();
  bool errorSmsCode = false;
  bool errorPhoneNumber = false;
  LoginBloc loginBloc = LoginBloc();
  String errorPhoneNumberText;
  String _smsCode;
  ProfileData user = App.me.user;
  String _phoneCode = '+1';
  String inputLoginPhone;
  bool disableNextStepBtn = true;
  int minLengthForDisableBtn = 10;
  int maxLengthForDisableBtn = 10;

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DefaultHeader(backArrowTap: () {
              Navigator.pop(context);
            }),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                AppTranslations.of(context).text('login_main_title'),
                style: DenimFonts.titleCallout(context),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: 35),
            Expanded(
              flex: 1,
              child: defaultLoginPageState(),
            ),
          ],
        ),
      ),
    );
  }

  Widget defaultLoginPageState() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 80,
                child: FakeFormSelect(
                  value: _phoneCode,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CountryPhoneCodes(),
                      ),
                    ).then((code) {
                      if (code == null || code.length == 0) {
                        _phoneCode = '+1';
                        if (!mounted) return;
                        setState(() {
                          minLengthForDisableBtn = 10;
                          maxLengthForDisableBtn = 10;
                        });
                      } else {
                        if (!mounted) return;
                        switch (code) {
                          case '+7':
                            phoneNumberGuard(code, 10, 10);
                            break;
                          case '+373':
                            phoneNumberGuard(code, 8, 8);
                            break;
                          default:
                            phoneNumberGuard(code, 9, 12);
                        }
                      }
                    });
                  },
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                child: DenimInput(
                  autofocus: true,
                  errorText: errorPhoneNumber ? errorPhoneNumberText : null,
                  controller: _phoneNumberController,
                  keyboardType: TextInputType.phone,
                  hintText: '(000) 000-0000',
                  onChanged: (val) {
                    if (_phoneNumberController.text.length <
                            minLengthForDisableBtn ||
                        _phoneNumberController.text.length >
                            maxLengthForDisableBtn) {
                      setState(() {
                        disableNextStepBtn = true;
                      });
                    } else {
                      setState(() {
                        disableNextStepBtn = false;
                      });
                    }
                  },
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DenimButton(
                  disable: disableNextStepBtn,
                  onPressed: () => submitLoginForm(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  submitLoginForm() async {
    bool internetConnection =
        await ConnectionUtils.checkInternetConnectivity(context);
    if (!internetConnection) return;
    inputLoginPhone =
        ProfileUtils.getCleanPhone(_phoneCode + _phoneNumberController.text);

    var phoneValidation =
        ValidatorUtils.validatePhoneNumberForRegistration(inputLoginPhone);

    print('phone: $inputLoginPhone');
    if (phoneValidation) {
      authApi.validate(inputLoginPhone, validateLogin);
    } else {
      setState(() {
        errorPhoneNumber = true;
        errorPhoneNumberText = AppTranslations.of(context).text('initial_registration_step_error');
      });
    }
  }

  phoneNumberGuard(String code, int min, int max) {
    setState(() {
      _phoneCode = code;
      minLengthForDisableBtn = min;
      maxLengthForDisableBtn = max;
      _phoneNumberController.clear();
    });
  }

  validateLogin([Map validateData]) {
    if (validateData == null) return;

    switch (validateData['ctrl']['code']) {
      case 200:
        _smsCode = validateData['data']['code'] == null
            ? null
            : validateData['data']['code'];
        String status = validateData['data']['status'].toString();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPageSms(
              phone: inputLoginPhone,
              smsCode: _smsCode,
              status: status,
            ),
          ),
        );
        break;
      case 429:
        if (!mounted) return;
        setState(() {
          errorPhoneNumber = true;
          errorPhoneNumberText = AppTranslations.of(context).text('login_error_1');
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorPhoneNumber = true;
          errorPhoneNumberText = AppTranslations.of(context).text('login_error_2');
        });
        break;
      default:
        return;
    }
  }
}
