import 'package:flutter/material.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/button.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdatedScreen extends StatefulWidget {
  @override
  _UpdatedScreenState createState() => _UpdatedScreenState();
}

class _UpdatedScreenState extends State<UpdatedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: WillPopScope(
          onWillPop: () {
            return;
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(
                image: AssetImage('assets/images/ky/keyou-logo.png'),
                width: 140,
              ),
              SizedBox(height: 16),
              Text(
                AppTranslations.of(context).text('updated_title'),
                style: DenimFonts.titleLarge(context),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 12),
              Text(
                AppTranslations.of(context).text('updated_subtitle'),
                style: DenimFonts.titleBody(context),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 90),
              SizedBox(
                width: 240,
                child: DenimButton(
                  title: AppTranslations.of(context).text('updated_btn'),
                  onPressed: () => _launchURL(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _launchURL() async {
    const url = 'https://play.google.com/store/apps/details?id=one.keyou.android';
    await launch(url);
  }
}
