import 'package:keYou/data/app.dart';
import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/prev_step_registration.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';

class SearchGenderRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  SearchGenderRegistrationStep({this.bloc, this.pageType = PageType.REGISTRATION});
  @override
  _SearchGenderRegistrationStepState createState() =>
      _SearchGenderRegistrationStepState(bloc: bloc);
}

class _SearchGenderRegistrationStepState
    extends State<SearchGenderRegistrationStep> {
  final RegistrationBloc bloc;

  _SearchGenderRegistrationStepState({this.bloc});
  int genderPref;
  ProfileData user = App.me.user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(backArrowTap: () {
            widget.pageType == PageType.REGISTRATION
                ? RegistrationPrevStep.prevStep(
                    widget.bloc,
                    GetGenderStep(),
                  )
                : Navigator.pop(context);
          }),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      AppTranslations.of(context).text('gender_prefs_title'),
                      style: DenimFonts.titleCallout(context),
                    ),
                  ),
                  SizedBox(height: 34),
                  item(1, AppTranslations.of(context).text('gender_pref_1')),
                  item(2, AppTranslations.of(context).text('gender_pref_2')),
                  item(3, AppTranslations.of(context).text('gender_pref_3')),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  BorderListItem item(int searchGender, String title) {
    return BorderListItem(
      title: Text(
        title,
        textAlign: TextAlign.center,
        style: DenimFonts.titleBody(context),
      ),
      onTap: () {
        widget.pageType == PageType.REGISTRATION ? submitSearchGender(searchGender) : Navigator.pop(context, Preferences(gender: searchGender));
      },
    );
  }

  void submitSearchGender(int genderPref) async {
    user.preferences = Preferences(gender: genderPref);
    userApi.updateAccount(
      ProfileData(profile: user.profile, settings: user.settings, preferences: user.preferences),
      updateGenderStepCallback,
    );
  }

  updateGenderStepCallback(Map data) {
    if (data['ctrl']['code'] == 200) {
      bloc.dispatch(GetNameStep());
    } else {
      Snackbar.showSnackbar(context);
    }
  }
}
