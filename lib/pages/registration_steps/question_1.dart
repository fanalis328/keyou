import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/pages/registration_steps/question_registration.dart';
import 'package:flutter/cupertino.dart';

class Question1 extends StatefulWidget {
  final RegistrationBloc bloc;
  Question1({this.bloc});
  @override
  _Question1State createState() => _Question1State();
}

class _Question1State extends State<Question1> {
  @override
  Widget build(BuildContext context) {
    return QuestionTemplate(
      questionTitle: 'Вопрос ?',
      questions: ['вариант 1', 'вариант 2'],
      prevCallback: () {
        widget.bloc.dispatch(GetNameStep());
      },
      nextCallback: (int val) {
        widget.bloc.dispatch(GetBirthDayStep());
        print(['_______QUESTION ___:', ++val]);
      },
    );
  }
}
