import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/prev_step_registration.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/default_slider.dart';
import 'package:keYou/widgets/label_checkbox.dart';
import 'package:keYou/widgets/snackbar.dart';
import 'package:flutter/material.dart';

class WeightRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  WeightRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
  });
  @override
  _WeightRegistrationStepState createState() =>
      _WeightRegistrationStepState(bloc: bloc);
}

class _WeightRegistrationStepState extends State<WeightRegistrationStep> {
  final RegistrationBloc bloc;

  _WeightRegistrationStepState({this.bloc});
  double _sliderWeightValue = 120.0;
  bool _isSelected = true;
  bool changeUnits = false;
  ProfileData user = App.me.user;
  static const lb = 2.205;

  @override
  void initState() {
    initWeightSettings();
    super.initState();
  }

  initWeightSettings() async {
    var userFromDb = await userDao.getCurrentUser(user);

    setState(() {
      _isSelected = !userFromDb.settings.hideWeight;
      _sliderWeightValue = userFromDb.profile.weight == 0
          ? 120.0
          : userFromDb.profile.weight * 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? RegistrationPrevStep.prevStep(
                      bloc,
                      GetHeightStep(),
                    )
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text(
                      AppTranslations.of(context)
                          .text('weight_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 32),
                    Text(
                      _handleWeight().toString() +
                          ' lb',
                      style: TextStyle(
                        fontSize: 40,
                        fontFamily: 'RobotoBold',
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    // hidden
                    Visibility(
                      visible: false,
                      child: GestureDetector(
                        onTap: () => convertUnits(),
                        child: Text(
                          changeUnits
                              ? 'Показать в килограммах'
                              : 'Показать в фунтах',
                          style: DenimFonts.titleFootnote(
                            context,
                            DenimColors.colorBlackTertiary,
                          ),
                        ),
                      ),
                    ),
                    DefaultSlider(
                      slider: Slider(
                        value: _sliderWeightValue,
                        min: 85,
                        max: 450,
                        onChanged: (double value) {
                          setState(() {
                            _sliderWeightValue = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 30),
                    LabeledCheckbox(
                      label: AppTranslations.of(context)
                          .text('weight_registration_step_title_2'),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: widget.pageType == PageType.REGISTRATION
                        ? AppTranslations.of(context).text('btn_сontinue_title')
                        : AppTranslations.of(context).text('btn_save_title'),
                    onPressed: () {
                      switch (widget.pageType) {
                        case PageType.REGISTRATION:
                          user.profile.weight = _handleWeight();
                          user.settings.hideWeight = !_isSelected;
                          userApi.updateAccount(ProfileData(profile: user.profile, settings: user.settings),
                              (Map data) {
                            if (data['ctrl']['code'] != 200 &&
                                user.id == null) {
                              Snackbar.showSnackbar(context);
                              return;
                            } else {
                              userDao.updateUser(user);
                              settingsDao.setSettings(user.settings.toJson());
                              bloc.dispatch(GetUserProfileDescriptionStep());
                              if (user.profile.gender == 1) {
                                firebaseAnalytics.sendAnalyticsEvent(
                                    'reg_step_13_wheight_M');
                              } else {
                                firebaseAnalytics.sendAnalyticsEvent(
                                    'reg_step_13_wheight_W');
                              }
                            }
                          });
                          break;
                        case PageType.EDIT_PROFILE:
                          Navigator.pop(
                            context,
                            {
                              'weight': _handleWeight(),
                              'settings': _isSelected
                            },
                          );
                          break;
                        default:
                          bloc.dispatch(GetCitizenshipStep());
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  int _handleWeight() {
    var pound = _sliderWeightValue * lb;

    if (changeUnits) {
      // return pound.round().toString() + ' lb';
      return pound.round();
    } else {
      // return _sliderWeightValue.round().toString() + ' ${AppTranslations.of(context).text('weight_registration_step_unit_title')}';
      return _sliderWeightValue.round();
    }
  }

  void convertUnits() {
    setState(() {
      changeUnits = !changeUnits;
    });
  }
}
