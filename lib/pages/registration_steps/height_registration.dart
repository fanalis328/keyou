import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/prev_step_registration.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/default_slider.dart';
import 'package:keYou/widgets/label_checkbox.dart';
import 'package:keYou/widgets/snackbar.dart';
import 'package:flutter/material.dart';

import '../../locator.dart';

class HeightRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  HeightRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
  });
  @override
  _HeightRegistrationStepState createState() =>
      _HeightRegistrationStepState(bloc: bloc);
}

class _HeightRegistrationStepState extends State<HeightRegistrationStep> {
  final RegistrationBloc bloc;

  _HeightRegistrationStepState({this.bloc});

  ProfileData user = App.me.user;
  double _sliderHeightValue = 65.0;
  bool _isSelected = true;
  bool changeUnits = false;
  static const inch = 2.54;

  @override
  void initState() {
    initHeightSettings();
    super.initState();
  }

  initHeightSettings() async {
    var userFromDb = await userDao.getCurrentUser(user);
    print(userFromDb.toJson());
    setState(() {
      _isSelected = !userFromDb.settings.hideHeight;
      _sliderHeightValue = userFromDb?.profile?.height == 0
          ? 65.0 : userFromDb.profile.height * 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? RegistrationPrevStep.prevStep(
                      bloc,
                      GetCheckpointStep(),
                    )
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text(
                      AppTranslations.of(context)
                          .text('height_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 32),
                    Text(
                      _handleHeigth().toString() + ' inch',
                      style: TextStyle(
                        fontSize: 40,
                        fontFamily: 'RobotoBold',
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    // hidden
                    Visibility(
                      visible: false,
                      child: GestureDetector(
                        onTap: () => convertUnits(),
                        child: Text(
                          changeUnits
                              ? 'Показать в сантиметрах'
                              : 'Показать в дюймах',
                          style: DenimFonts.titleFootnote(
                            context,
                            DenimColors.colorBlackTertiary,
                          ),
                        ),
                      ),
                    ),
                    DefaultSlider(
                      slider: Slider(
                        value: _sliderHeightValue,
                        min: 60,
                        max: 100,
                        onChanged: (double value) {
                          setState(() {
                            _sliderHeightValue = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 30),
                    LabeledCheckbox(
                      label: AppTranslations.of(context)
                          .text('height_registration_step_title_2'),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: widget.pageType == PageType.REGISTRATION
                        ? AppTranslations.of(context).text('btn_сontinue_title')
                        : AppTranslations.of(context).text('btn_save_title'),
                    onPressed: () {
                      user.profile.height = _handleHeigth();
                      switch (widget.pageType) {
                        case PageType.REGISTRATION:
                          user.profile.height = _sliderHeightValue.round();
                          user.settings.hideHeight = !_isSelected;
                          userApi.updateAccount(ProfileData(profile: user.profile, settings: user.settings),
                              (Map data) {
                            if (data['ctrl']['code'] != 200 &&
                                user.id == null) {
                              Snackbar.showSnackbar(context);
                              return;
                            } else {
                              userDao.updateUser(user);
                              bloc.dispatch(GetWeightStep());
                              if (user.profile.gender == 1) {
                                firebaseAnalytics
                                    .sendAnalyticsEvent('reg_step_11_height_M');
                              } else {
                                firebaseAnalytics
                                    .sendAnalyticsEvent('reg_step_11_height_W');
                              }
                            }
                          });
                          break;
                        case PageType.EDIT_PROFILE:
                          Navigator.pop(
                            context,
                            {
                              'height': _handleHeigth(),
                              'settings': _isSelected
                            },
                          );
                          break;
                        default:
                          bloc.dispatch(GetWeightStep());
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  int _handleHeigth() {
    var inches = _sliderHeightValue / inch;

    if (changeUnits) {
      // return inches.round() + ' inch';
      return inches.round();
    } else {
      // return _sliderHeightValue.round().toString() + ' ${AppTranslations.of(context).text('height_registration_step_unit_title')}';
      return _sliderHeightValue.round();
    }
  }

  void convertUnits() {
    setState(() {
      changeUnits = !changeUnits;
    });
  }
}
