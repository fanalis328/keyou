import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/pages/password.dart';
import 'package:keYou/utils/validators.dart';
import 'package:flutter/material.dart';

class RepeatPasswordRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final String password;

  RepeatPasswordRegistrationStep({
    this.bloc,
    this.password,
  });

  @override
  _RepeatPasswordRegistrationStepState createState() =>
      _RepeatPasswordRegistrationStepState();
}

class _RepeatPasswordRegistrationStepState
    extends State<RepeatPasswordRegistrationStep> {
  TextEditingController _repeatPasswordController = TextEditingController();
  bool passwordFieldError = false;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return PasswordPage(
      title: 'Повторите пароль',
      showSubtitle: false,
      passwordFieldErrorText: 'Пароли не совпадают',
      controller: _repeatPasswordController,
      passwordFieldError: passwordFieldError,
      backArrowAction: () {},
      submitButtonAction: () => validatePassword(),
    );
  }

  validatePassword() {
    if(ValidatorUtils.matchValidation(_repeatPasswordController.text, widget.password) == true) {
      widget.bloc.dispatch(GetGenderStep());
    } else {
      setState(() {
       passwordFieldError = true; 
      });
      return;
    }
  }
}
