import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/blocs/registration_bloc/registration_bloc.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/photo_verification_end_registration.dart';
import 'package:keYou/services/image_picker/image_file_model.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/image.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/circular_progress.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/material.dart';
import 'package:keYou/widgets/border_list_item.dart';
import '../../locator.dart';

class PhotoVerificationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final bool reVerification;

  PhotoVerificationStep({this.bloc, this.reVerification = false});

  @override
  _PhotoVerificationStepState createState() =>
      _PhotoVerificationStepState(bloc: bloc);
}

class _PhotoVerificationStepState extends State<PhotoVerificationStep> {
  final RegistrationBloc bloc;
  _PhotoVerificationStepState({this.bloc});
  ProfileData user = App.me.user;
  int visible = 0;
  ImageFile image;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Visibility(
            visible: !loading,
            child: DefaultHeader(backArrowTap: () {
              if (widget.reVerification) {
                Navigator.pop(context);
              } else {
                bloc.dispatch(GetPhotoStep());
              }
            }),
          ),
          Expanded(
            child: loading
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularLoadingIndicator(),
                    ],
                  )
                : SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 10),
                          Text(
                            AppTranslations.of(context)
                                .text('verification_registration_step_title'),
                            style: DenimFonts.titleCallout(context),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 15),
                          Text(
                            AppTranslations.of(context).text(
                                'verification_registration_step_description'),
                            style: DenimFonts.titleBody(context),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                'assets/images/ky/male-verification.jpg',
                                width: 150,
                                height: 200,
                              ),
                            ],
                          ),
                          SizedBox(height: 46),
                          Text(
                            AppTranslations.of(context).text(
                                'verification_registration_step_description_2'),
                            style: DenimFonts.titleBody(context),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            AppTranslations.of(context).text(
                                'verification_registration_step_description_3'),
                            style: DenimFonts.titleBody(context),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 20),
                          
                          Center(
                            child: SizedBox(
                              // height: 36,
                              width: 150,
                              child: BorderListItem(
                                height: 40,
                                title: Text('Skip'),
                                onTap: () {
                                  bloc.dispatch(GetCheckpointStep());
                                },
                              )
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Visibility(
                    visible: true,
                    child: DenimButton(
                        color: loading
                            ? DenimColors.colorBlackLine
                            : DenimColors.colorPrimary,
                        title: AppTranslations.of(context)
                            .text('verification_registration_step_btn_title'),
                        onPressed: () async {
                          if (loading) return;

                          image = await ImageUtils.getPhotoFromCamera();
                          if (image == null) return;

                          if (widget.reVerification) {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PhotoVerificationEndStep(
                                  imageFile: image,
                                  reVerification: true,
                                ),
                              ),
                            );
                          } else {
                            bloc.dispatch(
                                GetPhotoVerificationEndStep(imageFile: image));
                            firebaseAnalytics
                                .sendAnalyticsEvent('reg_step_9_photoconf_W');
                          }

                          if (!mounted) return;
                          // setState(() {
                          loading = true;

                          // });
                        }),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
