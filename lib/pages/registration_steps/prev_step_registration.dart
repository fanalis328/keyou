import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/blocs/registration_bloc/registration_bloc.dart';

class RegistrationPrevStep {
  static prevStep(RegistrationBloc bloc,RegistrationEvent event) {
    bloc.dispatch(event);
  }
}