import 'package:keYou/data/app.dart';
import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/prev_step_registration.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:keYou/widgets/circular_progress.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/input.dart';
import 'package:keYou/widgets/snackbar.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';

class LocationRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;
  final UserLocation country;

  LocationRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
    this.country,
  });
  @override
  _LocationRegistrationStepState createState() =>
      _LocationRegistrationStepState(bloc: bloc);
}

class _LocationRegistrationStepState extends State<LocationRegistrationStep> {
  final RegistrationBloc bloc;

  _LocationRegistrationStepState({this.bloc});

  TextEditingController _locationController = TextEditingController();
  bool errorLocation = false;
  ProfileData user = App.me.user;
  List _cities = [];
  List _citiesForDisplay = [];
  UserLocation selectedCity;

  backArrowAction() {
    citiesDao.deleteCities();
    RegistrationPrevStep.prevStep(
      bloc,
      GetCountryStep(),
    );
  }

  @override
  void initState() {
    userApi.getAllCitiesByCountry(widget.country.id, getCitiescallback);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? backArrowAction()
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Visibility(
                    visible: true,
                    child: Text(
                      AppTranslations.of(context)
                          .text('location_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                    ),
                  ),
                  SizedBox(height: 10),
                  DenimInput(
                    autofocus: true,
                    controller: _locationController,
                    hintText: AppTranslations.of(context)
                        .text('location_registration_step_input_hint'),
                    prefixIcon: Icon(
                      DenimIcon.universal_searchbar_search,
                      color: DenimColors.colorBlackTertiary,
                      size: 20,
                    ),
                    onChanged: (val) => searchCountry(val),
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    flex: 1,
                    child: _citiesForDisplay == null
                        ? CircularLoadingIndicator()
                        : ListView.builder(
                            padding: EdgeInsets.all(0),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: _citiesForDisplay.length,
                            itemBuilder: (BuildContext context, int index) {
                              return BorderListItem(
                                title: Text(
                                  _citiesForDisplay[index]['title'],
                                  textAlign: TextAlign.center,
                                  style: DenimFonts.titleBody(context),
                                  maxLines: 1,
                                ),
                                onTap: () => submitLocationForm(
                                  _citiesForDisplay[index],
                                ),
                              );
                            },
                          ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void submitLocationForm(Map city) {
    selectedCity = UserLocation.fromJson(city);
    user.profile.city = selectedCity;
    switch (widget.pageType) {
      case PageType.REGISTRATION:
        userApi.updateAccount(
          ProfileData(profile: user.profile, settings: user.settings),
          (Map data) {
            if (data['ctrl']['code'] == 200) {
              citiesDao.setCities([city]);
              userDao.updateUser(user);
              bloc.dispatch(GetPhotoStep());

              if (user?.profile?.gender == 1) {
                yandexAnalytics
                    .sendYandexAnalyticsEvent(yandexAnalytics.eventRegStep4M);
                firebaseAnalytics.sendAnalyticsEvent('reg_step_7_loc_M');
              } else {
                yandexAnalytics
                    .sendYandexAnalyticsEvent(yandexAnalytics.eventRegStep4W);
                firebaseAnalytics.sendAnalyticsEvent('reg_step_7_loc_W');
              }
            } else {
              Snackbar.showSnackbar(context);
            }
          },
        );
        break;
      case PageType.EDIT_PROFILE:
        Navigator.pop(context, selectedCity);
        break;
      case PageType.FILTER:
        Navigator.pop(context, selectedCity);
        break;
      default:
        bloc.dispatch(GetPhotoStep());
    }
  }

  getCitiescallback([data]) {
    if(widget.pageType == PageType.FILTER) {
      _cities.add({'id': null, 'title': AppTranslations.of(context).text('all_cities')});
    }
    _cities.addAll(data['data']['cities']);
    _citiesForDisplay = _cities;
    setState(() {});
  }

  searchCountry([String text = '']) {
    setState(() {
      _citiesForDisplay = _cities.where((item) {
        var cityName = item['title'].toLowerCase();
        return cityName.contains(text.toLowerCase());
      }).toList();
    });
  }
}
