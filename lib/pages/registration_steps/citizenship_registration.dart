import 'package:keYou/data/app.dart';
import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/enum.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/prev_step_registration.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/input.dart';
import 'package:flutter/material.dart';

import '../../locator.dart';

class CitizenshipRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  CitizenshipRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
  });

  @override
  _CitizenshipRegistrationStepState createState() =>
      _CitizenshipRegistrationStepState(bloc: bloc);
}

class _CitizenshipRegistrationStepState
    extends State<CitizenshipRegistrationStep> {
  final RegistrationBloc bloc;

  _CitizenshipRegistrationStepState({this.bloc});

  TextEditingController _citizenController = TextEditingController();
  bool errorCitizen = false;
  // FocusNode fn = FocusNode();
  ProfileData user = App.me.user;
  List _countries = [];
  List _countriesForDisplay = [];

  // void afterBuild(_) {
  //   FocusScope.of(context).requestFocus(fn);
  // }

  @override
  void dispose() {
    // fn.dispose();
    super.dispose();
  }

  @override
  void initState() {
    getCountriesFromDb();
    super.initState();
  }

  getCountriesFromDb() async {
    var countries = await countriesListDao.getCountriesList();
    if (countries == null) {
      App.me.sendLegacy({'id': socket.requestId(), 'method': 'getCountries'},
          (Map data) async {
        if (data != null &&
            data['ctrl']['code'] == 200 &&
            data['data']['countries'].length > 0) {
          List filteredList = data['data']['countries'];
          filteredList.sort((a, b) => a['id'].compareTo(b['id']));
          getCountriescallback(filteredList);
          await countriesListDao.setCountriesList(filteredList);
        } else {
          return;
        }
      });
    } else {
      getCountriescallback(countries);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? RegistrationPrevStep.prevStep(
                      bloc,
                      GetCityStep(),
                    )
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Visibility(
                    visible:
                        widget.pageType == PageType.REGISTRATION ? true : false,
                    child: Text(
                      AppTranslations.of(context)
                          .text('citizenship_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                    ),
                  ),
                  SizedBox(height: 10),
                  DenimInput(
                    autofocus: true,
                    // focusNode: fn,
                    controller: _citizenController,
                    hintText: AppTranslations.of(context)
                        .text('location_registration_step_input_hint'),
                    prefixIcon: Icon(
                      DenimIcon.universal_searchbar_search,
                      color: DenimColors.colorBlackTertiary,
                      size: 20,
                    ),
                    onChanged: (val) {
                      searchCountry(val);
                    },
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    flex: 1,
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _countriesForDisplay.length,
                      itemBuilder: (BuildContext context, int index) {
                        return BorderListItem(
                          title: Text(
                            _countriesForDisplay[index]['title'],
                            textAlign: TextAlign.center,
                            style: DenimFonts.titleBody(context),
                            maxLines: 1,
                          ),
                          onTap: () {
                            submitCitizenshipForm(_countriesForDisplay[index]);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  getCountriescallback(countries) {
    _countries.addAll(countries);
    _countriesForDisplay = _countries;
    setState(() {});
  }

  searchCountry([String text = '']) {
    setState(() {
      _countriesForDisplay = _countries.where((item) {
        var countryName = item['title'].toLowerCase();
        return countryName.contains(text.toLowerCase());
      }).toList();
    });
  }

  void submitCitizenshipForm(Map country) {
    user.profile.citizenship = UserLocation.fromJson(country);
    switch (widget.pageType) {
      case PageType.REGISTRATION:
        userApi.updateAccount(ProfileData(profile: user.profile, settings: App.me.settings), (Map data) {
          if (data['ctrl']['code'] == 200) {
            countriesDao.setCountries([country]);
            userDao.updateUser(user);
            bloc.dispatch(GetPhotoStep());
            if (user.profile.gender == 1) {
              firebaseAnalytics.sendAnalyticsEvent('reg_step_7_citz_M');
            } else {
              firebaseAnalytics.sendAnalyticsEvent('reg_step_7_citz_W');
            }
          }
        });
        break;
      case PageType.EDIT_PROFILE:
        Navigator.pop(context, country);
        break;
      default:
        bloc.dispatch(GetPhotoStep());
    }
  }
}
