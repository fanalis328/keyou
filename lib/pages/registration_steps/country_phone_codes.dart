import 'package:keYou/data/repository.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/phone_code.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:flutter/material.dart';

class CountryPhoneCodes extends StatefulWidget {
  @override
  _CountryPhoneCodesState createState() => _CountryPhoneCodesState();
}

class _CountryPhoneCodesState extends State<CountryPhoneCodes> {
  TextEditingController _countryPhoneCodeController = TextEditingController();
  Future<List<PhoneCode>> phoneCodeData = Repository.get().getPhoneCode();
  List<PhoneCode> _phoneCodeList = [];
  List<PhoneCode> _phoneCodeListForDisplay = [];

  @override
  void initState() {
    phoneCodeData.then((data) {
      setState(() {
        _phoneCodeList.addAll(data);
        _phoneCodeListForDisplay = _phoneCodeList;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            mainTitle: AppTranslations.of(context).text('country'),
            visibleActionTitle: false,
            backArrowTap: () =>
                Navigator.pop(context),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    controller: _countryPhoneCodeController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(0),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: DenimColors.colorBlackLine,
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 2,
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      prefixIcon: Icon(
                        DenimIcon.universal_searchbar_search,
                        size: 16,
                        color: DenimColors.colorBlackSecondary,
                      ),
                      hintText: AppTranslations.of(context).text('phone_code'),
                    ),
                    onChanged: (text) {
                      search(text);
                    },
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    flex: 1,
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _phoneCodeListForDisplay.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          title: Text(
                            '${_phoneCodeListForDisplay[index].name} (${_phoneCodeListForDisplay[index].code})',
                            textAlign: TextAlign.left,
                            maxLines: 1,
                          ),
                          onTap: () {
                            Navigator.pop(
                              context,
                              _phoneCodeListForDisplay[index].code,
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  search([String text = '']) {
    setState(() {
      _phoneCodeListForDisplay = _phoneCodeList.where((item) {
        var countryName = item.name.toLowerCase();
        var countryCode = item.code;
        return countryName.contains(text.toLowerCase()) || countryCode.contains(text.toLowerCase());
      }).toList();
    });
  }
}
