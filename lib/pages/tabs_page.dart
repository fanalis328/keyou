import 'package:keYou/data/app.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/search_filter.dart';
import 'package:keYou/pages/tabs/chat/chat.dart';
import 'package:keYou/pages/tabs/profile/profile.dart';
import 'package:keYou/pages/tabs/search/search.dart';
import 'package:keYou/pages/tabs/views/views.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/verification.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:keYou/widgets/popups/profile_moderation.dart';
import 'package:flutter/material.dart';
import '../locator.dart';

class TabsPage extends StatefulWidget {
  final VerificationInfo verification;
  final int initialIndex;
  TabsPage({this.initialIndex = 0, this.verification});

  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  List<Widget> pages = [
    SearchPage(),
    // MailingPage(),
    ChatPage(),
    ViewsPage(),
    ProfilePage(),
  ];

  @override
  void initState() {
    super.initState();
    _showDialog();
    gender();
  }


  gender() {
    SearchFilters().gender = SearchFilters.searchUserByGenderPreferences(App.me.user.preferences?.gender);
  }
  _showDialog() async {
    if (widget.verification == null || !widget.verification.verification) return;
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return ProfileModerationDialog(moderated: true);
      },
    );
  }

  @override
  void dispose() {
    App.me.newViewsController.close();
    App.me.unreadMessageController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: pages.length,
      initialIndex: widget.verification == null
          ? widget.initialIndex
          : widget.verification.index,
      child: WillPopScope(
        onWillPop: () {
          FocusScope.of(context).unfocus();
          return showDialog<bool>(
            context: context,
            builder: (c) => DefaultAlertDialog(
              title: Text(AppTranslations.of(context).text('blocking_logout')),
              cancelBtnCallBack: () {
                authApi.logout();
              },
              cancelBtnTitle:
                  AppTranslations.of(context).text('account_logout'),
              actionButton: GestureDetector(
                child: Text(
                  AppTranslations.of(context).text('stay'),
                  style: DenimFonts.titleLink(
                    context,
                    DenimColors.colorPrimary,
                  ),
                ),
                onTap: () => Navigator.pop(c, false),
              ),
            ),
          );
        },
        child: Scaffold(
          body: TabBarView(
            children: pages,
            physics: NeverScrollableScrollPhysics(),
          ),
          bottomNavigationBar: Container(
            child: TabBar(
              onTap: (val) {
                // FocusScope.of(context).unfocus();
              },
              tabs: <Widget>[
                Tab(child: Icon(DenimIcon.section_tabbar_tab1, size: 32)),
                // Tab(child: Icon(DenimIcon.section_tabbar_tab2, size: 32)),
                Tab(
                  child: StreamBuilder(
                    stream: App.me.unreadMessageStream,
                    builder: (context, snapshot) {
                      if (snapshot.data == null) {
                        return Icon(DenimIcon.section_tabbar_tab3, size: 32);
                      } else if(snapshot.hasError) {
                        return Icon(DenimIcon.section_tabbar_tab3, size: 39);
                      } else {
                        return Stack(
                          children: <Widget>[
                            Icon(DenimIcon.section_tabbar_tab3, size: 32),
                            Positioned(
                              right: 0,
                              top: 2,
                              child: Visibility(
                                visible: snapshot.data == true ? true : false,
                                child: Container(
                                  width: 10,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: DenimColors.colorPrimary,
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    },
                  ),
                ),
                Tab(
                  child: StreamBuilder(
                    stream: App.me.newViewsStream,
                    builder: (context, snapshot) {
                      if (snapshot.data == null) {
                        return Icon(DenimIcon.section_tabbar_tab4, size: 32);
                      } else if(snapshot.hasError) {
                        return Icon(DenimIcon.section_tabbar_tab4, size: 39);
                      } else {
                        return Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child:
                                  Icon(DenimIcon.section_tabbar_tab4, size: 32),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Visibility(
                                visible: snapshot.data == true ? true : false,
                                child: Container(
                                  width: 7,
                                  height: 8,
                                  decoration: BoxDecoration(
                                    color: DenimColors.colorPrimary,
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    },
                  ),
                ),
                Tab(child: Icon(DenimIcon.section_tabbar_tab5, size: 32)),
              ],
              unselectedLabelColor: Colors.black,
              labelColor: DenimColors.colorPrimary,
              indicator: UnderlineTabIndicator(),
            ),
          ),
        ),
      ),
    );
  }
}
