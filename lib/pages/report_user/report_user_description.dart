import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/widgets/api/response_status_dialogs.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/textarea.dart';
import 'package:flutter/material.dart';

class ReportUserDescription extends StatefulWidget {
  final String reportReason;
  final String userId;

  ReportUserDescription({
    this.reportReason,
    this.userId,
  });

  @override
  _ReportUserDescriptionState createState() => _ReportUserDescriptionState();
}

class _ReportUserDescriptionState extends State<ReportUserDescription> {
  TextEditingController reportDescreeprionController = TextEditingController();
  bool disable = true;
  bool error = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            mainTitle: AppTranslations.of(context).text('report_main_title'),
            visibleActionTitle: false,
            backArrowTap: () => Navigator.pop(context),
          ),
          SizedBox(height: 10),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  Textarea(
                    controller: reportDescreeprionController,
                    minLines: 4,
                    autofocus: true,
                    maxLines: 6,
                    errorText: error
                        ? AppTranslations.of(context)
                            .text('user_description_registration_step_error')
                        : null,
                    hintText: AppTranslations.of(context).text('report_description_2'),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(children: <Widget>[
              Expanded(
                child: DenimButton(
                  title: AppTranslations.of(context).text('submit'),
                  onPressed: () => reportUser(),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }

  reportUser() {
    if (reportDescreeprionController.text.length < 1) {
      setState(() {
        error = true;
      });
      return;
    }
    userApi.reportUser(
        widget.userId, widget.reportReason, reportDescreeprionController.text,
        (data) {
      if (data['ctrl']['code'] == 200) {
        Navigator.pop(context);
        Navigator.pop(context);
        ResponseStatusDialogs().successWithImage(context, AppTranslations.of(context).text('clear'), 'assets/images/operation-complete.jpg', AppTranslations.of(context).text('report_sent'));
      } else {
        ResponseStatusDialogs().defaultError(context);
      }
    });
  }
}
