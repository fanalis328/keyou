import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/border_list_item.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/popups/new_support.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';

class UniversalBlockingProfile extends StatefulWidget {
  final String title;

  UniversalBlockingProfile({this.title = ''});
  @override
  _UniversalBlockingProfileState createState() =>
      _UniversalBlockingProfileState();
}

class _UniversalBlockingProfileState extends State<UniversalBlockingProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          return;
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/icons/section-blocking-attention.jpg',
                      width: 104,
                    ),
                    SizedBox(height: 16),
                    Text(
                      widget.title,
                      style: DenimFonts.titleDefault(context),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 24),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: SizedBox(
                            height: 48,
                            child: DenimButton(
                              onPressed: () => authApi.logout(),
                              title: AppTranslations.of(context).text('account_logout'),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 7),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: BorderListItem(
                            height: 48,
                            title: Text(
                              AppTranslations.of(context).text('default_header_action_title'),
                              style:
                                  DenimFonts.titleBtnSecondaryDefault(context),
                            ),
                            onTap: () {
                              return showDialog(
                                context: context,
                                builder: (context) {
                                  return SupportDialogs();
                                },
                              );
                            },
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
