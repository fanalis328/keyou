import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';

class LoftPremiumSlider extends StatefulWidget {
  @override
  _LoftPremiumSliderState createState() => _LoftPremiumSliderState();
}

class _LoftPremiumSliderState extends State<LoftPremiumSlider> {
  int _current = 0;
  List<Widget> slides;
  List<LoftPremiumSlide> slidesData;

  @override
  void initState() {
    super.initState();
  }

  initSlider() {
    slidesData = [
      LoftPremiumSlide(
        icon: DenimIcon.subscription_block_description_1,
        imageUrl: 'assets/images/ky/slide_1.jpg',
        title: AppTranslations.of(context).text('ky_premium_slide_1_title_1'),
        title2:
            AppTranslations.of(context).text('ky_premium_slide_1_title_2'),
      ),
      // LoftPremiumSlide(
      //   icon: DenimIcon.subscription_block_description_2,
      //   imageUrl: 'assets/images/ky/slide_2.jpg',
      //   title: AppTranslations.of(context).text('ky_premium_slide_2_title_1'),
      //   title2: AppTranslations.of(context).text('ky_premium_slide_2_title_2'),
      // ),
      LoftPremiumSlide(
        icon: DenimIcon.subscription_block_description_3,
        imageUrl: 'assets/images/ky/slide_3.jpg',
        title: AppTranslations.of(context).text('ky_premium_slide_3_title_1'),
        title2:
            AppTranslations.of(context).text('ky_premium_slide_3_title_2'),
      ),
      LoftPremiumSlide(
        icon: DenimIcon.subscription_block_description_4,
        imageUrl: 'assets/images/ky/slide_4.jpg',
        title: AppTranslations.of(context).text('ky_premium_slide_4_title_1'),
        title2:
            AppTranslations.of(context).text('ky_premium_slide_4_title_2'),
      ),
      LoftPremiumSlide(
        icon: DenimIcon.subscription_block_description_5,
        imageUrl: 'assets/images/ky/slide_5.jpg',
        title: AppTranslations.of(context).text('ky_premium_slide_5_title_1'),
        title2:
            AppTranslations.of(context).text('ky_premium_slide_5_title_2'),
      ),
    ];
    slides = slidesData.map((slide) => sliderItem(slide)).toList();
    return slides;
  }

  @override
  Widget build(BuildContext context) {
    initSlider();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CarouselSlider(
          items: slides,
          initialPage: 0,
          viewportFraction: 1.0,
          aspectRatio: 1 / 0.61,
          onPageChanged: (index) {
            setState(() {
              _current = index;
            });
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: slides
              .asMap()
              .map(
                (i, element) => MapEntry(
                  i,
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 10,
                        height: 10,
                        margin: EdgeInsets.symmetric(
                          vertical: 10.0,
                          horizontal: 2.0,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 2, color: Colors.black),
                          color: _current == i ? Colors.black : Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              )
              .values
              .toList(),
        ),
      ],
    );
  }

  Widget sliderItem(LoftPremiumSlide slideData) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Image(
                image: AssetImage(slideData.imageUrl),
                fit: BoxFit.cover,
                width: 144,
                height: 144,
              ),
              Positioned(
                right: -4,
                bottom: -4,
                child: Container(
                  width: 56,
                  height: 56,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.black,
                  ),
                  child: Center(
                    child: Icon(
                      slideData.icon,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: slideData.title,
                      style: TextStyle(
                        fontFamily: 'MontserratSemiBold',
                        fontWeight: FontWeight.w900,
                        fontSize: 22,
                        color: DenimColors.colorBlackSecondary,
                      ),
                    ),
                    TextSpan(
                      text: slideData.title2,
                      style: TextStyle(
                        fontFamily: 'MontserratSemiBold',
                        fontWeight: FontWeight.w900,
                        fontSize: 22,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class LoftPremiumSlide {
  String imageUrl;
  String title;
  String title2;
  IconData icon;

  LoftPremiumSlide({
    this.imageUrl,
    this.title,
    this.title2,
    this.icon,
  });
}
