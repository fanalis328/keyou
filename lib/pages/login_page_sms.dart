import 'package:keYou/blocs/registration_bloc/bloc.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/moderation/universal_blocking_profile.dart';
import 'package:keYou/pages/registration_page.dart';
import 'package:keYou/pages/tabs_page.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/auth.dart';
import 'package:keYou/utils/chat.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/utils/connections.dart';
import 'package:keYou/utils/verification.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/counter.dart';
import 'package:keYou/widgets/default_header.dart';
import 'package:keYou/widgets/input.dart';
import 'package:flutter/material.dart';

import '../locator.dart';

class LoginPageSms extends StatefulWidget {
  final String smsCode;
  final String phone;
  final String status;
  LoginPageSms({
    this.smsCode,
    this.phone,
    this.status,
  });

  @override
  _LoginPageSmsState createState() => _LoginPageSmsState();
}

class _LoginPageSmsState extends State<LoginPageSms> with TickerProviderStateMixin {
  TextEditingController _smsCodeController = TextEditingController();
  bool errorSmsCode = false;
  String errorSmsCodeText;
  ProfileData user = App.me.user;
  String _smsCode;
  static AnimationController _controller;
  static const int kStartValue = 60;
  bool resendSmsCode;
  bool disableNextStepBtn = true;
  final registrationBloc = RegistrationBloc();

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(minutes: 1),
    );
    _controller.forward(from: 0.0);
    _controller.addStatusListener(statusListener);
    setState(() {
      resendSmsCode = false;
      _smsCode = widget.smsCode;
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.removeStatusListener(statusListener);
    _controller.dispose();
    super.dispose();
  }

  statusListener(val) {
    setState(() {
      resendSmsCode = true;
      errorSmsCode = false;
      errorSmsCodeText = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DefaultHeader(backArrowTap: () {
            Navigator.pop(context);
          }),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              '${AppTranslations.of(context).text('login_enter_sms_code')} ${_smsCode == null ? '' : ':' + _smsCode}',
              style: DenimFonts.titleCallout(context),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      DenimInput(
                        autofocus: true,
                        controller: _smsCodeController,
                        keyboardType: TextInputType.number,
                        hintText: AppTranslations.of(context)
                            .text('login_sms_input_hint'),
                        errorText: errorSmsCode ? errorSmsCodeText : null,
                        onChanged: (val) {
                          if (_smsCodeController.text.length != 4) {
                            setState(() {
                              disableNextStepBtn = true;
                            });
                          } else {
                            setState(() {
                              disableNextStepBtn = false;
                            });
                          }
                        },
                      ),
                      SizedBox(height: 25),
                      SizedBox(
                        width: 195,
                        child: RawMaterialButton(
                            elevation: 0,
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            fillColor: !resendSmsCode
                                ? DenimColors.colorBlackLine
                                : Colors.white,
                            child: resendSmsCode
                                ? Text(
                                    AppTranslations.of(context).text(
                                        'sms_registration_step_repeat_btn_title'),
                                  )
                                : Countdown(
                                    style: DenimFonts.titleBtnXSmall(context),
                                    animation: StepTween(
                                      begin: kStartValue,
                                      end: 0,
                                    ).animate(_controller),
                                  ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: BorderSide(
                                width: 1,
                                color: DenimColors.colorBlackLine,
                              ),
                            ),
                            onPressed: () {
                              if (!resendSmsCode) return;
                              authApi.validate(widget.phone, resendSms);
                              if (mounted) {
                                setState(() {
                                  disableNextStepBtn = true;
                                });
                              }
                            }),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: DenimButton(
                          disable: disableNextStepBtn,
                          title: AppTranslations.of(context)
                              .text('btn_login_title'),
                          onPressed: () => submitSmsCode(),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  resendSms([Map validateData]) {
    if (validateData == null) return;

    switch (validateData['ctrl']['code']) {
      case 200:
        String code = validateData['data']['code'] == null
            ? null
            : validateData['data']['code'];
        _controller.forward(from: 0.0);
        setState(() {
          resendSmsCode = false;
          _smsCode = code;
          _smsCodeController = TextEditingController(text: '');
        });
        break;
      case 429:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = AppTranslations.of(context).text('login_error_1');
          disableNextStepBtn = true;
          _smsCodeController = TextEditingController(text: '');
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = AppTranslations.of(context).text('login_error_1');
          disableNextStepBtn = true;
          _smsCodeController = TextEditingController(text: '');
        });
        break;
      default:
        return;
    }
  }

  submitSmsCode() async {
    bool internetConnection =
        await ConnectionUtils.checkInternetConnectivity(context);
    if (!internetConnection) return;
    authApi.login(
        '${widget.phone}:${_smsCodeController.text}', 'phone', loginCallback);
  }

  loginCallback([Map data]) async {
    if (data == null) return;
    switch (data['ctrl']['code']) {
      case 200:
        await AuthUtils.loginPhoneCallback(data);
        await userApi.getAccount(checkUserRegistrated);
        break;
      case 400:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = AppTranslations.of(context).text('login_error_3');
          disableNextStepBtn = true;
        });
        break;
      case 409:
        authApi.logout(AuthUtils.cleanLogoutCallback);
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = AppTranslations.of(context).text('login_error_2');
          disableNextStepBtn = true;
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = AppTranslations.of(context).text('login_error_2');
          disableNextStepBtn = true;
        });
        break;
      default:
        return;
    }
  }

  checkUserRegistrated([Map data]) async {
    ProfileData user = App.me.user;
    user.id = await authDao.getCurrentUserId();
    if (data == null) return;

    if (data != null &&
        data['ctrl']['code'] == 200 &&
        data['data']['registered'] != null &&
        widget.status == '2' &&
        !data['data']['blocked'] &&
        !data['data']['deleted']) {
      ProfileUtils.updateUserInstanceFromServer(data,
          (ProfileData userData) async {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => TabsPage()));
        await ChatUtils.updateDialogs();
        VerificationUtils.checkVerificationCases(data, context);
        purchaseUtils.updateCurrentPurchase();
        firebaseAnalytics.sendAnalyticsEvent('login');
        firebaseAnalytics.logLogin();
      });
      return;
    } else if (data['ctrl']['code'] == 200 &&
        data['data']['registered'] == null) {
      ProfileUtils.updateUserInstanceFromServer(data,
          (ProfileData profileData) {
        setState(() {
          user = profileData;
        });
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RegistrationPage(getGenderStep: true)));
      });
      return;
    } else if (!data['data']['blocked'] && data['data']['deleted']) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UniversalBlockingProfile(
            title: AppTranslations.of(context).text('deleted_profile'),
          ),
        ),
      );
      return;
    } else if (data['data']['blocked']) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UniversalBlockingProfile(
            title: AppTranslations.of(context).text('blocked_profile'),
          ),
        ),
      );
      return;
    } else if (data['ctrl']['code'] != 200) {
      await authApi.logout(AuthUtils.cleanLogoutCallback());
      if (!mounted) return;
      setState(() {
        errorSmsCode = true;
        errorSmsCodeText = AppTranslations.of(context).text('login_error_2');
      });
      return;
    }
  }
}
