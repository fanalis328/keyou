// UI KIT TITLES
import 'package:flutter/material.dart';

class DenimFonts {
  static TextStyle titleXLarge(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 28, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleLarge(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 24, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleDefault(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 20, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleSmall(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 16, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleCallout(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 24, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleXSmall(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleSection(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, fontWeight: FontWeight.w600,  color: color, fontFamily: 'MontserratSemiBold');
  }

  static TextStyle titleBody(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 16, color: color, fontFamily: 'Roboto');
  }

  static TextStyle titleFootnote(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, color: color, fontFamily: 'Roboto');
  }

  static TextStyle titleCaption(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 12, color: color, fontFamily: 'Roboto');
  }

  static TextStyle titleBook(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 16, color: color, fontFamily: 'Roboto');
  }

  static TextStyle titleLink(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleBtnDefault(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 16, color: color, fontFamily: 'MontserratSemiBold');
  }

  static TextStyle titleBtnSecondaryDefault(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 16, fontWeight: FontWeight.w900, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleBtnSmallSecondary(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, color: color, fontFamily: 'MontserratNormal');
  }

  static TextStyle titleBtnSmall(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 14, color: color, fontFamily: 'MontserratSemiBold');
  }

  static TextStyle titleBtnXSmall(BuildContext context, [color]) {
    return Theme.of(context)
        .textTheme
        .title
        .copyWith(fontSize: 12, color: color, fontFamily: 'MontserratSemiBold');
  }
}
