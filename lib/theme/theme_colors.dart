// UI KIT COLORS
import 'package:flutter/material.dart';

class DenimColors {
  //______________________ PRIMARY _____________________
  // static const Color colorPrimary = Color.fromRGBO(255, 72, 112, 1);
  static const Color colorPrimary = Color.fromRGBO(175, 126, 255, 1);
  static const Color colorPrimaryTint = Color.fromRGBO(175, 126, 255, 0.2);
  static const Color colorPrimaryShade = Color.fromRGBO(255, 192, 206, 1);

  //______________________ BLACK _____________________
  static const Color colorBlackSecondary = Color.fromRGBO(111, 105, 112, 1);
  static const Color colorBlackTertiary = Color.fromRGBO(168, 159, 169, 1);
  static const Color colorBlackLine = Color.fromRGBO(228, 223, 229, 1);
  static const Color colorBlackCell = Color.fromRGBO(244, 240, 245, 1);

  //______________________ ACCESSORY ___________________
  static const Color colorAccessoryDefault = Color.fromRGBO(203, 51, 84, 1);
  static const Color colorAccessoryTint = Color.fromRGBO(238, 180, 180, 1);

  //______________________ SECONDARY ___________________
  static const Color colorSecondaryDefault = Color.fromRGBO(162, 124, 255, 1);

  //______________________ TERTIARY ___________________
  static const Color colorTertiaryDefault = Color.fromRGBO(0, 219, 163, 1);

  //_______________________ SPECIAL ______________________
  static const Color colorSpecialOnlineDefault = Color.fromRGBO(0, 219, 163, 1);
  static const Color colorSpecialPositive = Color.fromRGBO(55, 224, 102, 1);
  static const Color colorSpecialPositiveDark = Color.fromRGBO(20, 177, 102, 1);
  static const Color colorSpecialErrorDefault = Color.fromRGBO(255, 79, 79, 1);

  //____________________ Black Translucent ________________
  static const Color colorTranslucentSnackBar = Color.fromRGBO(0, 0, 0, 0.75);
  static const Color colorTranslucentPrimary = Color.fromRGBO(0, 0, 0, 0.64);
  static const Color colorTranslucentSecondary = Color.fromRGBO(0, 0, 0, 0.4);
  static const Color colorTranslucentTertiary = Color.fromRGBO(0, 0, 0, 0.3);
  static const Color colorTranslucentLine = Color.fromRGBO(0, 0, 0, 0.08);
  static const Color colorTranslucentSelection = Color.fromRGBO(0, 0, 0, 0.05);
  
  //____________________ White ________________
  static const Color colorWhiteTertiary = Color.fromRGBO(255, 255, 255, 0.4);


}
