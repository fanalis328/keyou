import 'package:keYou/push_notify/push_notification.dart';
import 'package:keYou/push_notify/push_notification_interface.dart';
import 'package:keYou/push_notify/push_notification_web.dart';
import 'package:keYou/services/image_editor/image_editor.dart';
import 'package:keYou/services/image_editor/image_editor_interface.dart';
import 'package:keYou/services/image_editor/image_editor_web.dart';
import 'package:keYou/services/navigation_service.dart';
import 'package:keYou/utils/interface/purchase_utils_interface.dart';
import 'package:keYou/utils/purchase.dart';
import 'package:keYou/utils/web/purchase_utils_web.dart';
import 'package:keYou/websocket/websocket_common.dart';
import 'package:keYou/websocket/websocket_stub.dart'
    if (dart.library.html) 'package:keYou/websocket/websocket_web.dart'
    if (dart.library.io) 'package:keYou/websocket/websocket.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'analytics/analytics.dart';
import 'api/auth_api.dart';
import 'api/chat_api.dart';
import 'api/purchase_api.dart';
import 'api/search_api.dart';
import 'api/user_api.dart';
import 'api/views_api.dart';
import 'db/auth_dao.dart';
import 'db/auth_dao_interface.dart';
import 'db/db.dart';
import 'locale/locale_service_stub.dart'
    if (dart.library.html) 'locale/locale_service_web.dart'
    if (dart.library.io) 'locale/locale_service.dart';
import 'locale/locale_service_interface.dart';
import 'services/image_picker/index.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  _createCommonServices();
  if (kIsWeb) {
    _createWebServices();
    return;
  }
  _createMobileServices();
}

void _createCommonServices() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton<IImagePicker>(() => ImagePicker());
}

void _createMobileServices() {
  // Analytics
  locator.registerLazySingleton<IYandexAnalytics>(() => YandexAnalytics());
  locator.registerLazySingleton<IFacebookAnalytics>(() => FacebookAnalytics());
  locator
      .registerLazySingleton<IFirebaseAnalytics>(() => FirebaseAnalyticsImpl());
  // Network
  locator.registerLazySingleton<WebSocketCommon>(getWebSocket);

  // Data access
  locator.registerLazySingleton<IAuthDao>(() => AuthDao());
  locator.registerLazySingleton<IChatDao>(() => ChatDao());
  locator.registerLazySingleton<ICitiesDao>(() => CitiesDao());
  locator.registerLazySingleton<ICountriesDao>(() => CountriesDao());
  locator.registerLazySingleton<ICountriesListDao>(() => CountriesListDao());
  locator.registerLazySingleton<ILangDao>(() => LangDao());
  locator.registerLazySingleton<IPurchaseDao>(() => PurchaseDao());
  locator.registerLazySingleton<ISettingsDao>(() => SettingsDao());
  locator.registerLazySingleton<IUserDao>(() => UserDao());
  locator.registerLazySingleton<IVerificationPhotoDao>(
      () => VerificationPhotoDao());
  locator.registerLazySingleton<IVoiceDao>(() => VoiceDao());
  locator.registerLazySingleton<ILocaleService>(getLocaleService);
  locator.registerLazySingleton<IPushNotification>(() => PushNotification());

  locator.registerLazySingleton<IPurchaseUtils>(() => PurchaseUtils());
  locator.registerLazySingleton<IImageEditor>(() => ImageEditor());
}

void _createWebServices() {
  // Analytics
  locator.registerLazySingleton<IYandexAnalytics>(() => YandexAnalyticsWeb());
  locator
      .registerLazySingleton<IFacebookAnalytics>(() => FacebookAnalyticsWeb());
  locator.registerLazySingleton<IFirebaseAnalytics>(
      () => FirebaseAnalyticsImplWeb());
  // Network
  locator.registerLazySingleton<WebSocketCommon>(getWebSocket);

  // Data access
  locator.registerLazySingleton<IAuthDao>(() => AuthDaoWeb());
  locator.registerLazySingleton<IChatDao>(() => ChatDaoWeb());
  locator.registerLazySingleton<ICitiesDao>(() => CitiesDaoWeb());
  locator.registerLazySingleton<ICountriesDao>(() => CountriesDaoWeb());
  locator.registerLazySingleton<ICountriesListDao>(() => CountriesListDaoWeb());
  locator.registerLazySingleton<ILangDao>(() => LangDaoWeb());
  locator.registerLazySingleton<IPurchaseDao>(() => PurchaseDaoWeb());
  locator.registerLazySingleton<ISettingsDao>(() => SettingsDaoWeb());
  locator.registerLazySingleton<IUserDao>(() => UserDaoWeb());
  locator.registerLazySingleton<IVerificationPhotoDao>(
      () => VerificationPhotoDaoWeb());
  locator.registerLazySingleton<IVoiceDao>(() => VoiceDaoWeb());

  // Locale
  locator.registerLazySingleton<ILocaleService>(getLocaleService);
  locator.registerLazySingleton<IPushNotification>(() => PushNotificationWeb());

  locator.registerLazySingleton<IPurchaseUtils>(() => PurchaseUtilsWeb());
  locator.registerLazySingleton<IImageEditor>(() => ImageEditorWeb());
}

// access shortcut

// Analytics
final yandexAnalytics = locator.get<IYandexAnalytics>();
final facebookAnalytics = locator.get<IFacebookAnalytics>();
final firebaseAnalytics = locator.get<IFirebaseAnalytics>();

// Network
final socket = locator.get<WebSocketCommon>();

// Data Access
final authDao = locator.get<IAuthDao>();
final chatDao = locator.get<IChatDao>();
final citiesDao = locator.get<ICitiesDao>();
final countriesDao = locator.get<ICountriesDao>();
final countriesListDao = locator.get<ICountriesListDao>();
final langDao = locator.get<ILangDao>();
final purchaseDao = locator.get<IPurchaseDao>();
final settingsDao = locator.get<ISettingsDao>();
final userDao = locator.get<IUserDao>();
final verificationPhotoDao = locator.get<IVerificationPhotoDao>();
final voiceDao = locator.get<IVoiceDao>();

// Api
final userApi = UserApi();
final authApi = AuthApi();
final chatApi = ChatApi();
final purchaseApi = PurchaseApi();
final searchApi = SearchApi();
final viewsApi = ViewsApi();

// Locale
final localeService = locator.get<ILocaleService>();

// Push
final pushNotificationService = locator.get<IPushNotification>();

// Utils
final purchaseUtils = locator.get<IPurchaseUtils>();

final imagePicker = locator.get<IImagePicker>();
