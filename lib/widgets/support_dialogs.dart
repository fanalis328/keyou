import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:keYou/widgets/input.dart';
import 'package:flutter/material.dart';

class SupportEmailDialog extends StatefulWidget {
  @override
  _SupportEmailDialogState createState() => _SupportEmailDialogState();
}

class _SupportEmailDialogState extends State<SupportEmailDialog> {
  bool emailState = false;
  TextEditingController _message = TextEditingController();
  TextEditingController _email = TextEditingController();
  bool buttonDisabled = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(top: 5),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      title: Visibility(
        visible: emailState ? true : false,
        child: Text(
          AppTranslations.of(context).text('popup_support_title_2'),
          style: DenimFonts.titleDefault(context),
          textAlign: TextAlign.left,
        ),
      ),
      content: Wrap(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Visibility(
              child: Text(
                emailState
                    ? ''
                    : AppTranslations.of(context).text('popup_support_title'),
                style: DenimFonts.titleBody(context),
                textAlign: TextAlign.left,
              ),
              visible: emailState ? false : true,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: setField(),
          ),
          SizedBox(height: 10),
          Container(
            height: 52,
            padding: EdgeInsets.only(right: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    if (emailState) {
                      checkButtonStatus(
                          emailState ? _message.text : _email.text);
                      setState(() {
                        emailState = false;
                      });
                    } else {
                      Navigator.pop(context);
                    }
                  },
                  child: Text(
                    emailState
                        ? AppTranslations.of(context).text('back')
                        : AppTranslations.of(context).text('btn_сancel_title'),
                    style: DenimFonts.titleBtnSmall(
                      context,
                      DenimColors.colorPrimary,
                    ),
                  ),
                ),
                SizedBox(width: 20),
                GestureDetector(
                  onTap: () {
                    if (buttonDisabled) {
                      return;
                    } else {
                      setButtonAction();
                      checkButtonStatus(
                          emailState ? _email.text : _message.text);
                    }
                  },
                  child: Text(
                    emailState
                        ? AppTranslations.of(context).text('submit')
                        : AppTranslations.of(context)
                            .text('btn_сontinue_title'),
                    style: DenimFonts.titleBtnSmall(
                      context,
                      buttonDisabled
                          ? DenimColors.colorBlackLine
                          : DenimColors.colorPrimary,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget setField() {
    if (emailState) {
      return DenimInput(
        controller: _email,
        autofocus: true,
        keyboardType: TextInputType.emailAddress,
        hintText: 'Email',
        onChanged: (val) {
          checkButtonStatus(val);
        },
      );
    }

    return DenimInput(
      controller: _message,
      autofocus: true,
      keyboardType: TextInputType.text,
      hintText: AppTranslations.of(context).text('popup_support_input_hint'),
      onChanged: (val) {
        checkButtonStatus(val);
      },
    );
  }

  checkButtonStatus([String val]) {
    if (val.toString().length > 0) {
      setState(() {
        buttonDisabled = false;
      });
    } else {
      setState(() {
        buttonDisabled = true;
      });
    }
  }

  setButtonAction() async {
    if (emailState) {
      await _sendSupportField();
      Navigator.pop(context);
      sendSuccessDialog();
    } else {
      setState(() {
        emailState = true;
        buttonDisabled = true;
      });
    }
  }

  Future _sendSupportField() {
    return Future.delayed(
      Duration(seconds: 1),
      () {},
    );
  }

  sendSuccessDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          cancelBtnTitle: AppTranslations.of(context).text('close'),
          title: Text(
            AppTranslations.of(context).text('popup_support_success_sent'),
            style: DenimFonts.titleDefault(context),
            textAlign: TextAlign.left,
          ),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  AppTranslations.of(context).text('popup_support_subtitle'),
                  textAlign: TextAlign.left,
                  style: DenimFonts.titleBody(
                    context,
                    Colors.black,
                  ),
                ),
                Text(
                  _email.text,
                  textAlign: TextAlign.left,
                  style: DenimFonts.titleBody(
                    context,
                    Colors.black,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
