import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ConnectionErrorDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      title: Text(
        'Вышла новая версия',
        style: DenimFonts.titleDefault(context),
      ),
      cancelBtnTitle: AppTranslations.of(context).text('update_app'),
      cancelBtnCallBack: () => _launchURL(),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text('update app'),
      ),
    );
  }

  _launchURL() async {
    const url = 'https://www.google.com';
    await launch(url);
  }
}
