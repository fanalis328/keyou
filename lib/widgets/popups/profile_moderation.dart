import 'package:keYou/data/app.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/verification.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/image.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/material.dart';

import '../../locator.dart';

class ProfileModerationDialog extends StatefulWidget {
  final bool moderated;
  ProfileModerationDialog({this.moderated = false});
  @override
  _ProfileModerationDialogState createState() =>
      _ProfileModerationDialogState();
}

class _ProfileModerationDialogState extends State<ProfileModerationDialog> {
  ProfileData user = App.me.user;
  String token;
  var imageWidget;
  UserPhoto avatar;

  @override
  void initState() {
    super.initState();
    delayed();
  }

  delayed() async {
    await Future.delayed(Duration(seconds: 1));
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      centerButtons: true,
      cancelBtnTitle: AppTranslations.of(context).text('btn_сontinue_title'),
      cancelBtnCallBack: () async {
        if (widget.moderated) {
          await verificationPhotoDao.setVerificationApproved(
              VerificationApproved(owner: App.me.user.id, approved: true));
          Navigator.pop(context);
        } else {
          Navigator.pop(context);
        }
      },
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 25),
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: 104,
                    height: 104,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: ImageUtils.imageWidget(App.me.user.profile.avatar),
                    ),
                  ),
                  Positioned(
                    right: -10,
                    bottom: -10,
                    child: Visibility(
                      visible: true,
                      child: Image.asset(
                        widget.moderated
                            ? 'assets/images/icons/section-moderation-accept.png'
                            : 'assets/images/icons/section-moderation-pending.png',
                        width: 48,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              widget.moderated
                  ? AppTranslations.of(context).text('profile_on_moderation_popup_1')
                  : AppTranslations.of(context).text('profile_on_moderation_popup_2'),
              style: DenimFonts.titleDefault(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 12),
            Text(
              widget.moderated
                  ? AppTranslations.of(context).text('profile_on_moderation_popup_3')
                  : AppTranslations.of(context).text('profile_on_moderation_popup_4'),
              style: DenimFonts.titleBody(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 32),
          ],
        ),
      ),
    );
  }
}
