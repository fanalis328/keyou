import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/widgets.dart';

class UserDescriptionRules extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      title: Text(
        AppTranslations.of(context).text('popup_user_description_rules_main_title'),
        style: DenimFonts.titleDefault(context),
      ),
      cancelBtnTitle: AppTranslations.of(context).text('clear'),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Wrap(
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('popup_user_description_rules'),
              style: DenimFonts.titleFootnote(context),
            ),
          ],
        ),
      ),
    );
  }
}
