import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/locator.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportDialogs extends StatelessWidget {
  final bool loft = true;

  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      title: Text(
        AppTranslations.of(context).text('popup_support_title'),
        style: DenimFonts.titleBody(context),
      ),
      actionButton: GestureDetector(
        onTap: () => whatsAppDialog(context),
        child: Text(
          AppTranslations.of(context).text('popup_support_btn_2'),
          style: DenimFonts.titleLink(
            context,
            DenimColors.colorPrimary,
          ),
        ),
      ),
      cancelBtnTitle: AppTranslations.of(context).text('popup_support_btn_1'),
      cancelBtnCallBack: () => emailDialogs(context),
    );
  }

  _launchURL() async {
    // const url =  'https://t.me/DenimApp_bot';
    // const url = 'https://t.me/LoftApp_bot';
    const url = 'https://t.me/KeYou_bot';
    firebaseAnalytics.sendAnalyticsEvent('complaint');
    // TODO: canlaunch dont't work in web
    // if (await canLaunch(url)) {
    await launch(url);
    // } else {
    // throw 'Could not launch $url';
    // }
  }

  _emailLaunchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    firebaseAnalytics.sendAnalyticsEvent('complaint');
    // TODO: canlaunch dont't work in web
    // if (await canLaunch(url)) {
    await launch(url);
    // } else {
    // throw 'Could not launch $url';
    // }
  }

  emailDialogs(context) {
    Navigator.of(context).pop();
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          title: Text(
            '${AppTranslations.of(context).text('popup_support_email_title')} ?',
            style: DenimFonts.titleBody(context),
          ),
          actionButton: GestureDetector(
            onTap: () => _emailLaunchURL(
              'support@keyou.one',
              // AppTranslations.of(context).text('popup_support_email_template_title_1'),
              // AppTranslations.of(context).text('popup_support_email_template_title_2'),
              AppTranslations.of(context).text('ky_popup_support_email_template_title_1'),
              AppTranslations.of(context).text('ky_popup_support_email_template_title_2'),
            ),
            child: Text(
              AppTranslations.of(context).text('write'),
              style: DenimFonts.titleLink(
                context,
                DenimColors.colorPrimary,
              ),
            ),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('back'),
          cancelBtnCallBack: () => Navigator.of(context).pop(),
        );
      },
    );
  }

  whatsAppDialog(context) {
    Navigator.of(context).pop();
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          title: Text(
            '${AppTranslations.of(context).text('popup_support_telegram_title')} ?',
            style: DenimFonts.titleBody(context),
          ),
          actionButton: GestureDetector(
            onTap: () => _launchURL(),
            child: Text(
              AppTranslations.of(context).text('btn_сontinue_title'),
              style: DenimFonts.titleLink(
                context,
                DenimColors.colorPrimary,
              ),
            ),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('back'),
          cancelBtnCallBack: () => Navigator.of(context).pop(),
        );
      },
    );
  }
}
