import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class DefaultAlertDialog extends StatefulWidget {
  final Widget title;
  final Widget body;
  final Widget actionButton;
  final String cancelBtnTitle;
  final GestureTapCallback cancelBtnCallBack;
  final bool centerButtons;
  final bool hiddenButtons;

  DefaultAlertDialog({
    this.title,
    this.body,
    this.actionButton,
    this.cancelBtnTitle,
    this.cancelBtnCallBack,
    this.centerButtons = false,
    this.hiddenButtons = false,
  });

  @override
  _DefaultAlertDialogState createState() => _DefaultAlertDialogState();
}

class _DefaultAlertDialogState extends State<DefaultAlertDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(top: 18),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      title: widget.title,
      content: Wrap(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Wrap(
              children: <Widget>[
                Visibility(
                  visible: widget.body == null ? false : true,
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 5,
                      maxHeight: 350,
                    ),
                    child: SingleChildScrollView(
                      child: widget.body,
                    ),
                  ),
                ),
                Visibility(
                  visible: !widget.hiddenButtons,
                  child: Row(
                    mainAxisAlignment: widget.centerButtons
                        ? MainAxisAlignment.center
                        : MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsetsDirectional.only(bottom: 8),
                        height: 36,
                        padding: EdgeInsets.symmetric(horizontal: 6),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                widget.cancelBtnCallBack == null
                                    ? Navigator.pop(context)
                                    : widget.cancelBtnCallBack();
                              },
                              child: Text(
                                widget.cancelBtnTitle == null
                                    ? AppTranslations.of(context)
                                        .text('btn_сancel_title')
                                    : widget.cancelBtnTitle,
                                style: DenimFonts.titleBtnSmallSecondary(
                                  context,
                                  DenimColors.colorPrimary,
                                ),
                              ),
                            ),
                            Visibility(
                              visible:
                                  widget.actionButton == null ? false : true,
                              child: Row(
                                children: <Widget>[
                                  SizedBox(width: 24),
                                  widget.actionButton == null
                                      ? Text('')
                                      : widget.actionButton
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
