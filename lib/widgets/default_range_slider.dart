import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/widgets/default_slider.dart';
import 'package:flutter/material.dart';

class DefaultRangeSlider extends StatefulWidget {
  final RangeSlider slider;
  final String title;

  DefaultRangeSlider({
    this.slider,
    this.title = 'Title',
  });

  @override
  _DefaultRangeSliderState createState() => _DefaultRangeSliderState();
}

class _DefaultRangeSliderState extends State<DefaultRangeSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 25),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            width: 95,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.title,
                  style: DenimFonts.titleCaption(context, DenimColors.colorBlackSecondary),
                ),
                Text(
                  '${widget.slider.values.start.round()} - ${widget.slider.values.end.round()}',
                  style: DenimFonts.titleBody(context),
                ),
              ],
            ),
          ),
          Expanded(
            child: DefaultSliderTheme(
              slider: widget.slider,
            ),
          )
        ],
      ),
    );
  }
}
