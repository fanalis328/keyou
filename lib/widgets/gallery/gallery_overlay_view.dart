import 'dart:io';
import 'package:keYou/config/config.dart';
import 'package:keYou/data/app.dart';
import 'package:keYou/denim_icons.dart';
import 'package:keYou/locale/app_translations.dart';
import 'package:keYou/model/websocket/account.dart';
import 'package:keYou/pages/registration_steps/photo_registration.dart';
import 'package:keYou/pages/tabs_page.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:keYou/utils/profile.dart';
import 'package:keYou/widgets/button.dart';
import 'package:keYou/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../../locator.dart';

class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    this.loadingChild,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex,
    this.editMod = false,
    @required this.galleryItems,
    this.token,
    this.search = false,
    this.showCounter = true,
  }) : pageController = PageController(initialPage: initialIndex);

  final Widget loadingChild;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final bool editMod;
  final PageController pageController;
  final List<UserPhoto> galleryItems;
  final String token;
  final bool search;
  final bool showCounter;

  @override
  State<StatefulWidget> createState() {
    return _GalleryPhotoViewWrapperState();
  }
}

class _GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  int currentIndex;
  double opacityLevel = 1.0;
  String avatarId;
  int gender = App.me.user.profile.gender;
  @override
  void initState() {
    currentIndex = widget.initialIndex;
    getUserAvatar();
    super.initState();
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  void toggleVisible(double opacity) {
    setState(() {
      opacityLevel = opacity == null ? 1.0 : opacity;
    });
  }

  getUserAvatar() async {
    ProfileData userFromDb = await userDao.getCurrentUser(App.me.user);
    avatarId = userFromDb?.profile?.avatar?.id;
    onPageChanged(currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            GestureDetector(
              onVerticalDragUpdate: (val) => Navigator.pop(
                context,
                currentIndex,
              ),
              child: SafeArea(
                top: true,
                bottom: true,
                child: PhotoViewGallery.builder(
                  scrollPhysics: const BouncingScrollPhysics(),
                  builder: _buildItem,
                  itemCount: widget.galleryItems.length,
                  loadingChild: widget.loadingChild,
                  backgroundDecoration: widget.backgroundDecoration,
                  pageController: widget.pageController,
                  onPageChanged: onPageChanged,
                  scaleStateChangedCallback: (val) {
                    if (val == PhotoViewScaleState.zoomedIn) {
                      toggleVisible(0);
                    } else {
                      toggleVisible(1);
                    }
                  },
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: IgnorePointer(
                ignoring: true,
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 100),
                  opacity: widget.galleryItems[currentIndex].moderated == 0
                      ? opacityLevel
                      : 0,
                  child: Container(
                    color: DenimColors.colorTranslucentSecondary,
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    constraints: BoxConstraints.expand(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          DenimIcon.section_profileedit_photomoderation,
                          color: Colors.white,
                          size: 48,
                        ),
                        SizedBox(height: 8),
                        Text(
                          AppTranslations.of(context)
                              .text('overlay_gallery_moderation_title'),
                          style: DenimFonts.titleXLarge(context, Colors.white),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 8),
                        Text(
                          AppTranslations.of(context)
                              .text('overlay_gallery_moderation_description'),
                          style: DenimFonts.titleBody(context, Colors.white),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          AppTranslations.of(context)
                              .text('overlay_gallery_moderation_description_2'),
                          style: DenimFonts.titleBody(context, Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: IgnorePointer(
                ignoring: true,
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 100),
                  opacity: widget.galleryItems[currentIndex].moderated == 2
                      ? opacityLevel
                      : 0,
                  child: Container(
                    color: DenimColors.colorTranslucentSecondary,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    constraints: BoxConstraints.expand(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/icons/section-moderation-reject.png',
                          width: 100,
                        ),
                        SizedBox(height: 8),
                        Text(
                          AppTranslations.of(context).text('view_gallery_photo_not_approve_title'),
                          style: DenimFonts.titleXLarge(context, Colors.white),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 8),
                        Text(
                          AppTranslations.of(context).text('view_gallery_photo_not_approve_description'),
                          style: DenimFonts.titleBody(context, Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: AnimatedOpacity(
                opacity: opacityLevel,
                duration: Duration(milliseconds: 220),
                child: SafeArea(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: 100,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: SizedBox(
                              width: 40,
                              height: 40,
                              child: RawMaterialButton(
                                shape: CircleBorder(),
                                fillColor:
                                    DenimColors.colorTranslucentSecondary,
                                child: Icon(
                                  DenimIcon.universal_close,
                                  color: Colors.white,
                                  size: 23,
                                ),
                                onPressed: () =>
                                    Navigator.pop(context, currentIndex),
                              ),
                            ),
                          ),
                        ),
                        AnimatedOpacity(
                          opacity:
                              widget.galleryItems[currentIndex].id == avatarId
                                  ? opacityLevel
                                  : 0,
                          duration: Duration(milliseconds: 100),
                          child: Container(
                            height: 32,
                            width: 110,
                            decoration: BoxDecoration(
                              color: DenimColors.colorTranslucentPrimary,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  DenimIcon.section_profileview_photomain,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  AppTranslations.of(context)
                                      .text('overlay_gallery_main_photo_title'),
                                  style: DenimFonts.titleXSmall(
                                    context,
                                    Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 100,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Visibility(
                              visible: widget.showCounter,
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                height: 32,
                                width: 90,
                                decoration: BoxDecoration(
                                  color: DenimColors.colorTranslucentPrimary,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Center(
                                  child: Text(
                                    '${currentIndex + 1} / ${widget.galleryItems.length}',
                                    style: DenimFonts.titleBody(
                                      context,
                                      Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Visibility(
                visible: !widget.search ? true : false,
                child: AnimatedOpacity(
                  opacity: opacityLevel,
                  duration: Duration(milliseconds: 220),
                  child: SafeArea(
                    bottom: true,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: widget.galleryItems[currentIndex].moderated == 2 &&
                              widget.galleryItems[currentIndex].id == avatarId
                          ? Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: DenimButton(
                                    title: AppTranslations.of(context).text('view_gallery_photo_not_approve_btn'),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              PhotoRegistrationStep(
                                            reVerification: true,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            )
                          : Row(
                              mainAxisAlignment:
                                  widget.galleryItems[currentIndex].id ==
                                              avatarId ||
                                          widget.galleryItems[currentIndex]
                                                  .moderated !=
                                              1
                                      ? MainAxisAlignment.center
                                      : MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Visibility(
                                  visible:
                                      widget.galleryItems[currentIndex].id ==
                                                  avatarId ||
                                              widget.galleryItems[currentIndex]
                                                      .moderated !=
                                                  1
                                          ? false
                                          : true,
                                  child: Expanded(
                                    child: Container(
                                      height: 50,
                                      width: 0,
                                      child: RawMaterialButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          side: BorderSide(
                                            width: 1,
                                            color:
                                                DenimColors.colorWhiteTertiary,
                                          ),
                                        ),
                                        fillColor: DenimColors
                                            .colorTranslucentSecondary,
                                        child: Text(
                                          widget.galleryItems[currentIndex]
                                                      .id ==
                                                  avatarId
                                              ? AppTranslations.of(context).text(
                                                  'overlay_gallery_btn_title')
                                              : AppTranslations.of(context).text(
                                                  'overlay_gallery_btn_title'),
                                          style: DenimFonts.titleBtnSmall(
                                            context,
                                            Colors.white,
                                          ),
                                        ),
                                        onPressed: () {
                                          if (widget.galleryItems[currentIndex]
                                                  .id ==
                                              avatarId) {
                                          } else {
                                            ProfileUtils.setAddedPhotoOnAvatar(
                                                widget
                                                    .galleryItems[currentIndex],
                                                () {
                                              showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return DefaultAlertDialog(
                                                    title: Text(
                                                      AppTranslations.of(
                                                              context)
                                                          .text(
                                                              'overlay_gallery_set_new_avatar'),
                                                      style:
                                                          DenimFonts.titleBody(
                                                              context),
                                                    ),
                                                    actionButton:
                                                        GestureDetector(
                                                      onTap: () =>
                                                          Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              TabsPage(
                                                            initialIndex: 3,
                                                          ),
                                                        ),
                                                      ),
                                                      child: Text(
                                                        AppTranslations.of(
                                                                context)
                                                            .text('look'),
                                                        style: DenimFonts
                                                            .titleLink(
                                                          context,
                                                          DenimColors
                                                              .colorPrimary,
                                                        ),
                                                      ),
                                                    ),
                                                    cancelBtnTitle:
                                                        AppTranslations.of(
                                                                context)
                                                            .text('clear'),
                                                  );
                                                },
                                              );
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible:
                                      widget.galleryItems[currentIndex].id ==
                                                  avatarId ||
                                              widget.galleryItems[currentIndex]
                                                      .moderated !=
                                                  1
                                          ? false
                                          : true,
                                  child: SizedBox(width: 15),
                                ),
                                Visibility(
                                  visible: widget.galleryItems[currentIndex]
                                              .moderated !=
                                          1
                                      ? false
                                      : true,
                                  child: buildRoundButton(
                                    () => ProfileUtils.editPhoto(
                                        widget.galleryItems[currentIndex],
                                        (UserPhoto modifiedImage) {
                                      setState(() {
                                        widget.galleryItems[currentIndex] =
                                            modifiedImage;
                                      });
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return DefaultAlertDialog(
                                            title: Text(
                                              AppTranslations.of(context).text(
                                                  'overlay_gallery_edit_photo_title'),
                                              style:
                                                  DenimFonts.titleBody(context),
                                            ),
                                            cancelBtnTitle:
                                                AppTranslations.of(context)
                                                    .text('clear'),
                                          );
                                        },
                                      );
                                    }),
                                    Icon(
                                      DenimIcon.photoview_editphoto,
                                      size: 25,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Visibility(
                                  child: SizedBox(width: 15),
                                  visible: widget.galleryItems[currentIndex]
                                              .moderated !=
                                          1
                                      ? false
                                      : true,
                                ),
                                buildRoundButton(
                                  () {
                                    if (widget.galleryItems.length <= 0) {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return DefaultAlertDialog(
                                            title: Text(
                                              AppTranslations.of(context).text(
                                                  'blocking_photo_delete'),
                                              style:
                                                  DenimFonts.titleBody(context),
                                            ),
                                            cancelBtnTitle:
                                                AppTranslations.of(context)
                                                    .text('clear'),
                                          );
                                        },
                                      );
                                    } else {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return DefaultAlertDialog(
                                            title: Text(
                                              '${AppTranslations.of(context).text('delete_photo')} ?',
                                              style:
                                                  DenimFonts.titleBody(context),
                                            ),
                                            actionButton: GestureDetector(
                                              onTap: () {
                                                ProfileUtils.deleteGalleryItem(
                                                  widget.galleryItems,
                                                  App.me.user,
                                                  userDao,
                                                  widget.galleryItems[
                                                      currentIndex],
                                                  () {
                                                    Navigator.pop(context);
                                                    showDialog(
                                                      context: context,
                                                      builder: (context) {
                                                        return DefaultAlertDialog(
                                                          title: Text(
                                                            AppTranslations.of(
                                                                    context)
                                                                .text(
                                                                    'overlay_gallery_delete_photo_title'),
                                                            style: DenimFonts
                                                                .titleBody(
                                                                    context),
                                                          ),
                                                          cancelBtnCallBack:
                                                              () async {
                                                            Navigator.pop(
                                                                context);
                                                            await Future.delayed(
                                                                Duration(
                                                                    milliseconds:
                                                                        150));
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          cancelBtnTitle:
                                                              AppTranslations.of(
                                                                      context)
                                                                  .text(
                                                                      'clear'),
                                                        );
                                                      },
                                                    );
                                                    return widget.galleryItems;
                                                  },
                                                  () {
                                                    print(
                                                        'ERROR VIEW GALL ON DELETE');
                                                  },
                                                );
                                              },
                                              child: Text(
                                                AppTranslations.of(context)
                                                    .text('delete'),
                                                style: DenimFonts.titleLink(
                                                  context,
                                                  DenimColors.colorPrimary,
                                                ),
                                              ),
                                            ),
                                            cancelBtnTitle:
                                                AppTranslations.of(context)
                                                    .text('btn_сancel_title'),
                                          );
                                        },
                                      );
                                    }
                                  },
                                  Icon(
                                    DenimIcon.photoview_deletephoto,
                                    size: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildRoundButton(
    GestureTapCallback onTap,
    Icon icon, {
    double width = 50,
    double height = 50,
  }) {
    return Container(
      width: width,
      height: height,
      child: RawMaterialButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100),
          side: BorderSide(width: 1, color: DenimColors.colorWhiteTertiary),
        ),
        fillColor: DenimColors.colorTranslucentSecondary,
        child: icon,
        onPressed: onTap,
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final UserPhoto item = widget.galleryItems[index];

    return PhotoViewGalleryPageOptions(
      imageProvider: widget.search
          ? NetworkImage(
              '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${item.original.path.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
              headers: {'Authorization': 'token ${App.me.token}'})
          : imageWidget(item),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * 1,
      maxScale: PhotoViewComputedScale.covered * 1.1,
    );
  }

  imageWidget(UserPhoto item) {
    if (item.original.path != null &&
        item.original.path.contains(appConfig.downloadImageEndpoint)) {
      return NetworkImage(
          '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${item.preview.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
          headers: {'Authorization': 'token ${App.me.token}'});
    } else {
      //FIXME:
      return FileImage(File(item.original.path));
    }
  }
}
