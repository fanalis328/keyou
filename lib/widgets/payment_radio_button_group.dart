import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class PaymentRadioButtonGroup extends StatefulWidget {
  final List<RadioButton> paymentRadioList;

  PaymentRadioButtonGroup({
    this.paymentRadioList,
  });

  @override
  _PaymentRadioButtonGroupState createState() =>
      _PaymentRadioButtonGroupState();
}

class _PaymentRadioButtonGroupState extends State<PaymentRadioButtonGroup> {
  int _selectedItem = 0;

  selectItem(index) {
    setState(() {
      _selectedItem = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.paymentRadioList.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return RadioButton(
          title: widget.paymentRadioList[index].title,
          index: index,
          price: widget.paymentRadioList[index].price,
          payment: true,
          content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                widget.paymentRadioList[index].title,
                style: DenimFonts.titleSmall(context),
              ),
              Text(
                widget.paymentRadioList[index].price,
                style: DenimFonts.titleFootnote(
                  context,
                  DenimColors.colorBlackSecondary,
                ),
              ),
            ],
          ),
          onChange: selectItem,
          isSelected: _selectedItem == index ? true : false,
        );
      },
    );
  }
}

class RadioButton extends StatefulWidget {
  final String title;
  final Widget content;
  final int index;
  final String price;
  final bool isSelected;
  final bool payment;
  final Function(int) onChange;
  final EdgeInsets padding;

  RadioButton({
    this.title,
    this.content,
    this.index,
    this.price,
    this.isSelected = false,
    this.payment = false,
    this.onChange,
    this.padding = const EdgeInsets.symmetric(horizontal: 0),
  });

  _RadioButtonState createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        RawMaterialButton(
          padding: widget.padding,
          elevation: 0,
          onPressed: () {
            widget.onChange(widget.index);
          },
          fillColor:
              widget.isSelected ? DenimColors.colorPrimaryTint : Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: widget.payment ? BorderRadius.circular(10) : BorderRadius.circular(30),
            side: BorderSide(
              width: 1,
              color: widget.isSelected
                  ? DenimColors.colorPrimary
                  : DenimColors.colorBlackLine,
            ),
          ),
          child: widget.content == null ? Text(widget.title, style: DenimFonts.titleXSmall(context)) : widget.content
        ),
        SizedBox(width: 10),
      ],
    );
  }
}
