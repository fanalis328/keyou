import 'package:keYou/widgets/payment_radio_button_group.dart';
import 'package:flutter/material.dart';

class DefaultRadioButtonGroup extends StatefulWidget {
  final List<RadioButton> items;
  final int selectedItem;
  final Function(int) onChange;

  DefaultRadioButtonGroup({this.items, this.selectedItem, this.onChange});
  @override
  _DefaultRadioButtonGroupState createState() => _DefaultRadioButtonGroupState();
}

class _DefaultRadioButtonGroupState extends State<DefaultRadioButtonGroup> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.items.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return RadioButton(
          padding: EdgeInsets.symmetric(horizontal: 12),
          title: widget.items[index].title,
          index: index,
          onChange: widget.onChange,
          isSelected: widget.selectedItem == index ? true : false,
        );
      },
    );
  }
}