import 'package:keYou/locale/app_translations.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:keYou/theme/theme_colors.dart';

class Snackbar {
  static showSnackbar(BuildContext context,
      [Widget content = const Text('Произошла ошибка'),
      int duration = 3,
      Color backgroundColor]) {
    final snackbar = SnackBar(
      content: content,
      duration: Duration(seconds: duration),
      backgroundColor: backgroundColor,
    );

    return Scaffold.of(context).showSnackBar(snackbar);
  }

  static showLoadPhotoFromSnackbar(BuildContext context, callback) {
    Snackbar.showSnackbar(
        context,
        Container(
          height: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  if (!kIsWeb)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                            color: Colors.white,
                            icon: Icon(
                              Icons.camera_alt,
                              size: 32,
                            ),
                            onPressed: () {
                              Scaffold.of(context).hideCurrentSnackBar();
                              callback('camera');
                            }),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).hideCurrentSnackBar();
                            callback('camera');
                          },
                          child:
                              Text(AppTranslations.of(context).text('camera')),
                        )
                      ],
                    ),
                  if (!kIsWeb) SizedBox(width: 70),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        color: Colors.white,
                        icon: Icon(
                          Icons.photo_library,
                          size: 32,
                        ),
                        onPressed: () {
                          Scaffold.of(context).hideCurrentSnackBar();
                          callback();
                        },
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).hideCurrentSnackBar();
                          callback();
                        },
                        child:
                            Text(AppTranslations.of(context).text('gallery')),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
        3,
        DenimColors.colorPrimary);
  }
}
