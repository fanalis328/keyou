import 'package:keYou/denim_icons.dart';
import 'package:keYou/theme/theme_colors.dart';
import 'package:keYou/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class CheckboxButton extends StatefulWidget {
  final GestureTapCallback onPressed;
  final String title;
  final bool toggle;

  CheckboxButton({this.onPressed, this.title, this.toggle = false});
  @override
  _CheckboxButtonState createState() => _CheckboxButtonState();
}

class _CheckboxButtonState extends State<CheckboxButton> {
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      elevation: 0,
      fillColor: widget.toggle ? DenimColors.colorPrimaryTint : Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 12),
      onPressed: () {
        widget.onPressed();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
        side: BorderSide(
          width: 1,
          color: widget.toggle ? DenimColors.colorPrimaryShade : DenimColors.colorBlackLine,
        ),
      ),
      child: Container(
        height: 32,
        child: Row(
          children: <Widget>[
            Visibility(
              visible: widget.toggle ? true : false,
              child: Icon(
                DenimIcon.universal_done,
                color: DenimColors.colorPrimary,
              ),
            ),
            Visibility(
              visible: widget.toggle ? true : false,
              child: SizedBox(width: 5),
            ),
            Text(
              widget.title,
              style: DenimFonts.titleXSmall(
                context,
                widget.toggle ? DenimColors.colorPrimary : null,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
