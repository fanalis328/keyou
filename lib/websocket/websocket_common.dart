import 'package:keYou/data/app.dart';
import 'package:flutter/foundation.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

abstract class WebSocketCommon {
  WebSocketChannel _channel;
  // Platform specific channel
  WebSocketChannel channelFactory();

  bool _isOn = false;
  ObserverList<Function> _listeners = ObserverList<Function>();
  int conter = 0;
  var requestIdList = [];

  initCommunication() async {
    try {
      _channel = channelFactory();
      _isOn = true;
      _channel.stream.listen(_onReceptionOfMessageFromServer, onDone: () {
        reset();
      }, onError: (error) {
        reset();
      });
    } catch (e) {
      print("________Create websocket channel error $e");
      _isOn = false;
    }
  }

  requestId() {
    var count = ++conter;
    requestIdList.add(count);
    return count;
  }

  /// ----------------------------------------------------------
  /// Closes the WebSocket communication
  /// ----------------------------------------------------------
  reset() {
    if (_channel != null) {
      if (_channel.sink != null) {
        _channel.sink.close();
        _isOn = false;
      }
    }
  }

  /// ---------------------------------------------------------
  /// Sends a message to the server
  /// ---------------------------------------------------------
  send(String message) async {
    if (_channel != null) {
      if (_channel.sink != null && _isOn) {
        _channel.sink.add(message);
      } else {
        await App.me.initWebsocketConnection(false);
      }
    }
  }

  /// ---------------------------------------------------------
  /// Adds a callback to be invoked in case of incoming
  /// notification
  /// ---------------------------------------------------------
  addListener(Function callback) {
    _listeners.add(callback);
  }

  removeListener(Function callback) {
    _listeners.remove(callback);
  }

  /// ----------------------------------------------------------
  /// Callback which is invoked each time that we are receiving
  /// a message from the server
  /// ----------------------------------------------------------
  _onReceptionOfMessageFromServer(message) {
    _isOn = true;
    _listeners.forEach((Function callback) {
      callback(message);
    });
  }
}
