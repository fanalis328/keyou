import 'package:keYou/config/config.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'websocket_common.dart';

class WebSocketMobile extends WebSocketCommon {
  @override
  WebSocketChannel channelFactory() {
    return IOWebSocketChannel.connect(appConfig.serverAdress);
  }
}

WebSocketCommon getWebSocket() {
  return WebSocketMobile();
}
